package com.act.user.Infrastructure.utils;

import com.act.user.application.enums.GenderEnums;
import com.act.user.domain.dto.UserOutputDto;
import com.act.user.domain.entity.User;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
@RequiredArgsConstructor
public class GenericMapper {

  private final ModelMapper modelMapper;

  // map cả null sang
  public <T, E> E mapToType(T source, Class<E> typeDestination) {
    if (source == null) {
      return null;
    }
    return modelMapper.map(source, typeDestination);
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (source == null || source.isEmpty()) {
      return null;
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }

  public void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  public String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  public UserOutputDto mapToUserOutputDto(User user) {
    UserOutputDto userOutputDto = mapToType(user, UserOutputDto.class);
    if (userOutputDto != null) {
      userOutputDto.setDob(UserUtils.sdf.format(user.getDob()));
      userOutputDto.setGender(GenderEnums.valueOf(user.getGender()).getValue());
      userOutputDto.setCreatedDate(UserUtils.sdf.format(user.getCreatedDate()));
    }
    return userOutputDto;
  }

  public List<UserOutputDto> mapToListUserOutputDto(List<User> users) {
    if (CollectionUtils.isEmpty(users)) {
      return new ArrayList<>();
    }
    return users.stream()
        .map(this::mapToUserOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}
