package com.act.user.Infrastructure.persistance.jpa;

import com.act.user.domain.entity.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserJpaRepository extends JpaRepository<User, Integer> {

  Optional<User> findByEmailAndState(String email, String state);

  Optional<User> findByIdAndState(Integer userId, String state);

  @Query(
      value =
          "SELECT DISTINCT(u) FROM User u "
              + "WHERE (:search IS NULL OR (u.name LIKE :search OR u.email LIKE :search OR u.phone LIKE :search)) "
              + "AND (:isTopStudent IS NULL OR u.isTopStudent = :isTopStudent) "
              + "AND u.state = :state")
  Page<User> findAllByCondition(
      @Param(value = "isTopStudent") Boolean isTopStudent,
      @Param(value = "search") String search,
      @Param(value = "state") String state,
      Pageable pageable);
}