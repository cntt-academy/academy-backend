package com.act.user.Infrastructure.utils;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.filter.UserInfo;
import com.act.user.application.enums.GenderEnums;
import com.act.user.application.enums.RedisEnums;
import com.act.user.application.enums.RoleEnums;
import com.act.user.domain.dto.UserOutputDto;
import java.text.SimpleDateFormat;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("UserUtils")
public class UserUtils {

  public static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

  /**
   * Chuẩn hóa parameter Query Like
   */
  public static String standardizedParameterQueryLike(String str) {
    return str == null || str.trim().isEmpty() ? null : "%" + str + "%";
  }

  /**
   * Validate và chuẩn hóa Gender
   */
  public String validateAndStandardizedGender(String gender) {
    try {
      GenderEnums genderEnums = GenderEnums.valueOf(StringUtils.capitalize(gender.toLowerCase()));
      return genderEnums.name();
    } catch (Exception ex) {
      throw new BadRequestException("Giới tính không hợp lệ");
    }
  }

  /**
   * Lấy ra thông tin Account đang login
   */
  public Map<String, String> getAccountLoginInfo() {
    log.info("<AdminUtils> => Function getAdminLoginInfo");

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserInfo userInfo = (UserInfo) authentication.getDetails();

    String email = userInfo.getEmail();
    log.info("Email: {}", email);
    String name = userInfo.getName();
    log.info("Name: {}", name);
    String role = userInfo.getRole();
    log.info("Role: {}", role);

    return Map.of("email", email, "name", name, "role", role);
  }

  /**
   * Kiểm tra quyền của tài khoản
   */
  public Boolean isOwnerOrManager(String role) {
    return role.equals(RoleEnums.Owner.name()) || role.equals(RoleEnums.Manager.name());
  }

  /**
   * Set thông tin Admin vào redis theo key
   */
  public void setRedisUserInfo(String email, UserOutputDto user) {
    RedisUtils.hset(email, RedisEnums.ID.name(), user.getId());
    RedisUtils.hset(email, RedisEnums.AVATAR.name(), user.getAvatar());
    RedisUtils.hset(email, RedisEnums.NAME.name(), user.getName());
    RedisUtils.hset(email, RedisEnums.GENDER.name(), user.getGender());
    RedisUtils.hset(email, RedisEnums.DOB.name(), user.getDob());
    RedisUtils.hset(email, RedisEnums.PHONE.name(), user.getPhone());
    RedisUtils.hset(email, RedisEnums.EMAIL.name(), user.getEmail());
    RedisUtils.hset(email, RedisEnums.ADDRESS.name(), user.getAddress());
    RedisUtils.hset(email, RedisEnums.JOB.name(), user.getJob());
    RedisUtils.hset(email, RedisEnums.WORK_PLACE.name(), user.getWorkplace());
    RedisUtils.hset(email, RedisEnums.CREATED_BY.name(), user.getCreatedBy());
    RedisUtils.hset(email, RedisEnums.CREATED_DATE.name(), user.getCreatedDate());
    RedisUtils.hset(email, RedisEnums.IS_TOP_STUDENT.name(), user.getIsTopStudent());
    RedisUtils.hset(email, RedisEnums.FEEDBACK.name(), user.getFeedback());
  }
}
