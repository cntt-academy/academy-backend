package com.act.user.Infrastructure.persistance.impl;

import com.act.user.Infrastructure.persistance.jpa.UserJpaRepository;
import com.act.user.application.enums.StateEnums;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("UserRepository")
public class UserRepositoryImpl implements UserRepository {

  private final UserJpaRepository userJpaRepository;

  @Override
  public Page<User> findAllByCondition(
      String search, Boolean isTopStudent, PageRequest pageRequest) {
    return userJpaRepository
        .findAllByCondition(isTopStudent, search, StateEnums.ACTIVE.name(), pageRequest);
  }

  @Override
  public User findByEmail(String email) {
    return userJpaRepository
        .findByEmailAndState(email, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public User findById(Integer userId) {
    return userJpaRepository
        .findByIdAndState(userId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public void save(User user) {
    userJpaRepository.save(user);
  }
}
