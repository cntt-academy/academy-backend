package com.act.user.Infrastructure.exception;

import com.act.user.application.response.ErrorResponseBase;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponseBase> handleException(Exception exception) {
    log.error("<Error processing> => {}", exception.getMessage());
    exception.printStackTrace();

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    response.setError(HttpStatus.INTERNAL_SERVER_ERROR);
    response.setMessage("Đã xảy ra lỗi không mong muốn. Xin hãy thử lại");
    return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<ErrorResponseBase> handleUnauthorizedException(
      UnauthorizedException exception) {
    log.error("<Error authorizing> => {}", exception.getMessage());

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.UNAUTHORIZED.value());
    response.setError(HttpStatus.UNAUTHORIZED);
    response.setMessage("Unauthorized");
    return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<ErrorResponseBase> handleBadRequestException(
      BadRequestException exception) {
    log.error("<Error validating> => {}", exception.getMessage());
    exception.printStackTrace();

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    response.setError(HttpStatus.BAD_REQUEST);
    response.setMessage(exception.getMessage());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  public ResponseEntity<ErrorResponseBase> handleMissingServletRequestParameterException(
      MissingServletRequestParameterException exception) {
    String name = exception.getParameterName();
    String type = exception.getParameterType();
    String errorMessage = "Tham số " + name + " kiểu dữ liệu " + type + " yêu cầu phải có";
    log.error("<Error validating> => {}", errorMessage);
    exception.printStackTrace();

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    response.setError(HttpStatus.BAD_REQUEST);
    response.setMessage(errorMessage);
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorResponseBase> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException exception) {
    String errorMessage = exception.getBindingResult().getFieldErrors().stream()
        .map(FieldError::getDefaultMessage)
        .collect(Collectors.joining(", ", "", " không hợp lệ"));
    log.error("<Error validating> => {}", errorMessage);
    exception.printStackTrace();

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    response.setError(HttpStatus.BAD_REQUEST);
    response.setMessage(errorMessage);
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ProcessException.class)
  public ResponseEntity<ErrorResponseBase> handleProcessException(ProcessException exception) {
    log.error("<Error processing> => {}", exception.getMessage());
    exception.printStackTrace();

    ErrorResponseBase response = new ErrorResponseBase();
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    response.setError(HttpStatus.INTERNAL_SERVER_ERROR);
    response.setMessage(exception.getMessage());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }
}
