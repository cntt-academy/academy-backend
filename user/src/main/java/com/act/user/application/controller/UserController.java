package com.act.user.application.controller;

import com.act.user.application.request.user.CreateUserRequest;
import com.act.user.application.request.user.GetListTopUserRequest;
import com.act.user.application.request.user.GetListUserRequest;
import com.act.user.application.request.user.UpdateUserRequest;
import com.act.user.domain.service.user.dispatcher.CheckExistsService;
import com.act.user.domain.service.user.dispatcher.CreateUserService;
import com.act.user.domain.service.user.dispatcher.DeleteUserService;
import com.act.user.domain.service.user.dispatcher.GetListTopUserService;
import com.act.user.domain.service.user.dispatcher.GetListUserService;
import com.act.user.domain.service.user.dispatcher.GetUserByIdService;
import com.act.user.domain.service.user.dispatcher.UpdateUserService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("UserController")
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

  private final GetListTopUserService getListTopUserService;
  private final CheckExistsService checkExistsService;
  private final GetUserByIdService getUserByIdService;
  private final GetListUserService getListUserService;
  private final CreateUserService createUserService;
  private final UpdateUserService updateUserService;
  private final DeleteUserService deleteUserService;

  @GetMapping(value = "/checkExists")
  public Object checkExists(@RequestParam String email) {
    return checkExistsService.execute(email);
  }

  @GetMapping(value = "/getById")
  public Object getById(@RequestParam String userId) {
    return getUserByIdService.execute(userId);
  }

  @PostMapping(value = "/getListTop")
  public Object getListTop(@Valid @RequestBody GetListTopUserRequest request) {
    return getListTopUserService.execute(request);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListUserRequest request) {
    return getListUserService.execute(request);
  }

  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateUserRequest request) {
    return createUserService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateUserRequest request) {
    return updateUserService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam String userId) {
    return deleteUserService.execute(userId);
  }
}
