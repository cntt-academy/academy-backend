package com.act.user.application.request;

import java.io.Serializable;
import lombok.Data;

@Data
public class PageRequest implements Serializable {

  private int page;
  private int size;
  private String sortBy;
  private boolean sortDesc;
}