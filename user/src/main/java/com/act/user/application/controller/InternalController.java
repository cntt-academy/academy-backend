package com.act.user.application.controller;

import com.act.user.domain.service.internal.dispatcher.GetUserByEmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("InternalController")
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalController {

  private final GetUserByEmailService getUserByEmailService;

  @GetMapping(value = "/user/getByEmail")
  public Object getByEmail(@RequestParam String email) {
    return getUserByEmailService.execute(email);
  }
}
