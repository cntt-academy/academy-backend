package com.act.user.application.request.user;

import com.act.user.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListTopUserRequest implements Serializable {

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
