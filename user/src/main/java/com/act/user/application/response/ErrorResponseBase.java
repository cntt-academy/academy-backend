package com.act.user.application.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class ErrorResponseBase implements Serializable {

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss")
  private String timestamp;

  private Integer status;

  private HttpStatus error;

  private String message;

  public ErrorResponseBase() {
    super();
    timestamp = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date());
  }

  public ErrorResponseBase(Integer status, HttpStatus error, String message) {
    super();
    timestamp = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date());
    this.status = status;
    this.error = error;
    this.message = message;
  }
}
