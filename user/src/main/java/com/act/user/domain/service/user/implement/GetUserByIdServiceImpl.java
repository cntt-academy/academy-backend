package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.utils.AESUtils;
import com.act.user.Infrastructure.utils.GenericMapper;
import com.act.user.Infrastructure.utils.UserUtils;
import com.act.user.application.enums.RoleEnums;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.dto.UserOutputDto;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.GetUserByIdService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetUserByIdService")
public class GetUserByIdServiceImpl implements GetUserByIdService {

  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final UserUtils userUtils;
  private final AESUtils aesUtils;
  private final Gson gson;
  private User user;

  @Override
  public ResponseEntity<?> execute(String userId) {
    log.info("======== API GET USER BY ID ========");
    log.info("User Id: {}", userId);
    return GetUserByIdService.super.execute(userId);
  }

  @Override
  public void validate(String userId) {
    user = userRepository.findById(Integer.valueOf(aesUtils.decrypt(userId)));
    log.info("<Query> => Result finding user by id: {}", user);
    if (user == null) {
      throw new BadRequestException("Không tìm thấy thông tin học viên");
    }

    Map<String, String> loggedAccount = userUtils.getAccountLoginInfo();
    if (!loggedAccount.get("email").equals(user.getEmail()) &&
        !userUtils.isOwnerOrManager(loggedAccount.get("role")) &&
        !loggedAccount.get("role").equals(RoleEnums.Collaborator.name())) {
      throw new BadRequestException("Bạn không có quyền lấy ra thông tin học viên");
    }
  }

  @Override
  public ResponseEntity<?> process(String userId) {
    UserOutputDto userOutputDto = genericMapper.mapToUserOutputDto(user);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin học viên thành công");
    response.setData(Map.of("user", userOutputDto));

    log.info("<Result API> => Getting user by id successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
