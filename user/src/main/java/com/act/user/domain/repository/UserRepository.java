package com.act.user.domain.repository;

import com.act.user.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface UserRepository {

  Page<User> findAllByCondition(String search, Boolean isTopStudent, PageRequest pageRequest);

  User findByEmail(String email);

  User findById(Integer userId);

  void save(User user);
}
