package com.act.user.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.user.domain.entity.User} entity
 */
@Data
public class UserOutputDto implements Serializable {

  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String dob;
  private String phone;
  private String email;
  private String address;
  private String job;
  private String workplace;
  private Boolean isTopStudent;
  private String feedback;
  private String emailAdminCreated;
  private String createdBy;
  private String createdDate;
}