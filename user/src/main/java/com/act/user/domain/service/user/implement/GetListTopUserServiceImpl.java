package com.act.user.domain.service.user.implement;

import com.act.user.application.request.user.GetListTopUserRequest;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.dto.UserOutputDto;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.GetListTopUserService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListTopUserService")
public class GetListTopUserServiceImpl implements GetListTopUserService {

  private final UserRepository userRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListTopUserRequest request) {
    log.info("======== API GET LIST TOP USER ========");
    log.info("Request get list top user: {}", gson.toJson(request));
    return GetListTopUserService.super.execute(request);
  }

  @Override
  public void validate(GetListTopUserRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListTopUserRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<User> users = userRepository.findAllByCondition(null, true, pageRequest);
    log.info("<Query> => Result getting top user: {}", gson.toJson(users));

    List<UserOutputDto> userOutputDtos = new ArrayList<>();
    users.forEach(item -> {
      UserOutputDto userOutputDto = new UserOutputDto();
      userOutputDto.setAvatar(item.getAvatar());
      userOutputDto.setName(item.getName());
      userOutputDto.setJob(item.getJob());
      userOutputDto.setWorkplace(item.getWorkplace());
      userOutputDto.setFeedback(item.getFeedback());
      userOutputDtos.add(userOutputDto);
    });

    Map<String, Object> data =
        Map.of("topUsers", userOutputDtos,
            "totalElements", users.getTotalElements(),
            "totalPages", users.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách top học viên thành công");
    response.setData(data);

    log.info("<Result API> => Getting list top user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
