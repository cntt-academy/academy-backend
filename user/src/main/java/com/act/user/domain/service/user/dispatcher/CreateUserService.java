package com.act.user.domain.service.user.dispatcher;

import com.act.user.application.request.user.CreateUserRequest;
import com.act.user.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateUserService
    extends BaseServiceRequestBody<CreateUserRequest, ResponseEntity<?>> {

}
