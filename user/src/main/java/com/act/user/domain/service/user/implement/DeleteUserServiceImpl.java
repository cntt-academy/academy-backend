package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.exception.ProcessException;
import com.act.user.Infrastructure.utils.AESUtils;
import com.act.user.Infrastructure.utils.UnirestUtils;
import com.act.user.Infrastructure.utils.UserUtils;
import com.act.user.application.enums.RoleEnums;
import com.act.user.application.enums.StateEnums;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.DeleteUserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteUserService")
public class DeleteUserServiceImpl implements DeleteUserService {

  private final UserRepository userRepository;
  private final EurekaClient eurekaClient;
  private final UserUtils userUtils;
  private final AESUtils aesUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  @Value("${path.deleteAllRegister}")
  private String pathDeleteAllByRegister;

  @Value("${path.deleteAccount}")
  private String pathDeleteAccount;
  private User user;

  @Override
  public ResponseEntity<?> execute(String userId) {
    log.info("======== API DELETE USER ========");
    log.info("User Id: {}", userId);
    return DeleteUserService.super.execute(userId);
  }

  @Override
  public void validate(String userId) {
    loggedAccount = userUtils.getAccountLoginInfo();
    if (!userUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa học viên");
    }

    user = userRepository.findById(Integer.valueOf(aesUtils.decrypt(userId)));
    log.info("<Query> => Find user by id result: {}", user);
    if (user == null) {
      throw new BadRequestException("Không tìm thấy học viên muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(String userId) {
    String urlDeleteAccount = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathDeleteAccount;

    Map<String, String> body =
        Map.of(
            "email", user.getEmail(),
            "role", RoleEnums.User.name(),
            "deletedBy", loggedAccount.get("name"));

    JsonObject resp = UnirestUtils.deleteWithoutHeader(urlDeleteAccount, gson.toJson(body));
    log.info("Url: {}", urlDeleteAccount);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    user.setState(StateEnums.DELETED.name());
    user.setUpdatedBy(loggedAccount.get("name"));
    user.setUpdatedDate(new Date());
    userRepository.save(user);

    CompletableFuture.runAsync(() -> {
      String urlDeleteAllByStudentId = eurekaClient
          .getNextServerFromEureka("course_and_class", false).getHomePageUrl()
          + "internal/register" + pathDeleteAllByRegister;

      Map<String, Object> bodyDeleteAllRegister =
          Map.of(
              "studentId", user.getId(),
              "deletedBy", loggedAccount.get("name"));

      JsonObject respDeleteAllRegister =
          UnirestUtils.deleteWithoutHeader(urlDeleteAllByStudentId, gson.toJson(bodyDeleteAllRegister));
      log.info("Url: {}", urlDeleteAllByStudentId);
      log.info("Body: {}", gson.toJson(bodyDeleteAllRegister));
      log.info("Result calling API: {}", respDeleteAllRegister);
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa học viên thành công");

    log.info("<Result API> => Deleting user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
