package com.act.user.domain.service.base;

public interface BaseServiceNoRequest<O> {

  O execute();
}
