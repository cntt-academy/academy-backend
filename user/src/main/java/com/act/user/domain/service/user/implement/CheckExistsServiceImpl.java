package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.ProcessException;
import com.act.user.Infrastructure.utils.AESUtils;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.CheckExistsService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CheckExistsService")
public class CheckExistsServiceImpl implements CheckExistsService {

  private final UserRepository userRepository;
  private final AESUtils aesUtils;

  @Override
  public ResponseEntity<?> execute(String email) {
    log.info("======== API CHECK USER EXISTS ========");
    log.info("Email user: {}", email);
    return CheckExistsService.super.execute(email);
  }

  @Override
  public void validate(String email) {
  }

  @Override
  public ResponseEntity<?> process(String email) {
    User user = userRepository.findByEmail(aesUtils.decrypt(email));
    log.info("<Query> => Result finding user by email: {}", user);
    if (user == null) {
      throw new ProcessException("Học viên chưa có tài khoản");
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Học viên đã có tài khoản");

    log.info("<Result API> => Check user exists successfully");

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
