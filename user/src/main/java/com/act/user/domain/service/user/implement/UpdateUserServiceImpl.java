package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.exception.ProcessException;
import com.act.user.Infrastructure.utils.GenericMapper;
import com.act.user.Infrastructure.utils.UnirestUtils;
import com.act.user.Infrastructure.utils.UserUtils;
import com.act.user.application.request.user.UpdateUserRequest;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.UpdateUserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateUserService")
public class UpdateUserServiceImpl implements UpdateUserService {

  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final UserUtils userUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  @Value("${path.updateAccount}")
  private String pathUpdateAccount;
  private User user;

  @Override
  public ResponseEntity<?> execute(UpdateUserRequest request) {
    log.info("======== API UPDATE USER ========");
    log.info("Request update user: {}", gson.toJson(request));
    return UpdateUserService.super.execute(request);
  }

  @Override
  public void validate(UpdateUserRequest request) {
    user = userRepository.findById(request.getId());
    log.info("<Query> => Result finding user by id: {}", user);
    if (user == null) {
      throw new BadRequestException("Không tìm thấy thông tin học viên muốn cập nhật");
    }

    loggedAccount = userUtils.getAccountLoginInfo();
    if (!loggedAccount.get("email").equals(user.getEmail())
        && !loggedAccount.get("email").equals(user.getEmailAdminCreated())
        && !userUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa thông tin học viên");
    }

    if (StringUtils.isNotBlank(request.getPhone())) {
      Pattern pattern = Pattern.compile("(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\\b");
      Matcher matcher = pattern.matcher(request.getPhone());
      if (!matcher.matches()) {
        throw new BadRequestException("Số điện thoại không hợp lệ");
      }
    }

    if (StringUtils.isNotBlank(request.getGender())) {
      user.setGender(userUtils.validateAndStandardizedGender(request.getGender()));
    }

    try {
      if (StringUtils.isNotBlank(request.getDob())) {
        user.setDob(UserUtils.sdf.parse(request.getDob()));
      }
    } catch (ParseException e) {
      throw new BadRequestException("Ngày sinh không hợp lệ");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateUserRequest request) {
    String urlUpdateName = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathUpdateAccount;

    Map<String, String> body =
        Map.of(
            "email", user.getEmail(),
            "name", request.getName(),
            "avatar", request.getAvatar(),
            "updatedBy", loggedAccount.get("name"));

    JsonObject resp = UnirestUtils.putWithoutHeader(urlUpdateName, gson.toJson(body));
    log.info("Url: {}", urlUpdateName);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    User dataUpdate = genericMapper.mapToType(request, User.class);
    dataUpdate.setDob(null);
    dataUpdate.setGender(null);
    genericMapper.copyNonNullProperties(dataUpdate, user);
    user.setUpdatedBy(loggedAccount.get("name"));
    user.setUpdatedDate(new Date());
    userRepository.save(user);

    if (loggedAccount.get("email").equalsIgnoreCase(user.getEmail())) {
      userUtils.setRedisUserInfo(user.getEmail(), genericMapper.mapToUserOutputDto(user));
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật thông tin học viên thành công");

    log.info("<Result API> => Updating user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
