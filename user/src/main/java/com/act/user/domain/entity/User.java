package com.act.user.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Lob
  @Column(name = "avatar", nullable = false)
  private String avatar;

  @Size(max = 50)
  @NotNull
  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Size(max = 10)
  @NotNull
  @Column(name = "gender", nullable = false, length = 10)
  private String gender;

  @NotNull
  @Column(name = "dob", nullable = false)
  private Date dob;

  @Size(max = 10)
  @NotNull
  @Column(name = "phone", nullable = false, length = 10)
  private String phone;

  @Size(max = 50)
  @NotNull
  @Column(name = "email", nullable = false, length = 50)
  private String email;

  @Lob
  @Column(name = "address")
  private String address;

  @Lob
  @Column(name = "job")
  private String job;

  @Lob
  @Column(name = "workplace")
  private String workplace;

  @NotNull
  @Column(name = "is_top_student", nullable = false)
  private Boolean isTopStudent = false;

  @Lob
  @Column(name = "feedback")
  private String feedback;

  @Column(name = "email_admin_created")
  private String emailAdminCreated;

  @Size(max = 10)
  @NotNull
  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Size(max = 50)
  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Size(max = 50)
  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @NotNull
  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", avatar='" + avatar + '\'' +
        ", name='" + name + '\'' +
        ", gender='" + gender + '\'' +
        ", dob=" + dob +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        ", address='" + address + '\'' +
        ", job='" + job + '\'' +
        ", workplace='" + workplace + '\'' +
        ", isTopStudent=" + isTopStudent +
        ", feedback='" + feedback + '\'' +
        ", emailAdminCreated=" + emailAdminCreated +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}