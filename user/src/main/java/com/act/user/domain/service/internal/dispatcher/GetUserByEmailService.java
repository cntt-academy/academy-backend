package com.act.user.domain.service.internal.dispatcher;

import com.act.user.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetUserByEmailService
    extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
