package com.act.user.domain.service.internal.implement;

import com.act.user.Infrastructure.exception.ProcessException;
import com.act.user.Infrastructure.utils.AESUtils;
import com.act.user.Infrastructure.utils.GenericMapper;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.dto.UserOutputDto;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.internal.dispatcher.GetUserByEmailService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetUserByEmailService")
public class GetUserByEmailServiceImpl implements GetUserByEmailService {

  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(String email) {
    log.info("======== API GET USER BY EMAIL ========");
    log.info("User email: {}", email);
    return GetUserByEmailService.super.execute(email);
  }

  @Override
  public void validate(String email) {

  }

  @Override
  public ResponseEntity<?> process(String email) {
    User user = userRepository.findByEmail(aesUtils.decrypt(email));
    log.info("<Query> => Result finding user by email: {}", user);
    if (user == null) {
      throw new ProcessException("Không tìm thấy thông tin học viên");
    }

    UserOutputDto userOutputDto = genericMapper.mapToUserOutputDto(user);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin học viên thành công");
    response.setData(Map.of("user", userOutputDto));

    log.info("<Result API> => Getting user by email successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
