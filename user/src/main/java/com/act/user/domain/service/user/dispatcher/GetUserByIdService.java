package com.act.user.domain.service.user.dispatcher;

import com.act.user.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetUserByIdService
    extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
