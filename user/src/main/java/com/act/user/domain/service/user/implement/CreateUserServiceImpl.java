package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.exception.ProcessException;
import com.act.user.Infrastructure.utils.AESUtils;
import com.act.user.Infrastructure.utils.GenericMapper;
import com.act.user.Infrastructure.utils.UnirestUtils;
import com.act.user.Infrastructure.utils.UserUtils;
import com.act.user.application.enums.GenderEnums;
import com.act.user.application.enums.RoleEnums;
import com.act.user.application.enums.StateEnums;
import com.act.user.application.request.user.CreateUserRequest;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.CreateUserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateUserService")
public class CreateUserServiceImpl implements CreateUserService {

  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final UserUtils userUtils;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Value("${path.sendEmail.userCreated}")
  private String pathSendEmailUserCreated;
  @Value("${path.getAdminByEmail}")
  private String pathGetAdminByEmail;
  @Value("${path.createAccount}")
  private String pathCreateAccount;
  @Value("${path.registerStudy}")
  private String pathRegisterStudy;
  private String emailCreateBy;
  private String nameCreateBy;
  private String gender;
  private Date dob;

  @Override
  public ResponseEntity<?> execute(CreateUserRequest request) {
    log.info("======== API CREATE USER ========");
    log.info("Request create user: {}", gson.toJson(request));
    return CreateUserService.super.execute(request);
  }

  @Override
  public void validate(CreateUserRequest request) {
    Pattern pattern;
    Matcher matcher;

    pattern = Pattern.compile("^[^\\s@]+@[^\\s@]+$");
    matcher = pattern.matcher(request.getEmail());
    if (!matcher.matches()) {
      throw new BadRequestException("Email không hợp lệ");
    }

    pattern = Pattern.compile("(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\\b");
    matcher = pattern.matcher(request.getPhone());
    if (!matcher.matches()) {
      throw new BadRequestException("Số điện thoại không hợp lệ");
    }

    gender = userUtils.validateAndStandardizedGender(request.getGender());

    try {
      dob = UserUtils.sdf.parse(request.getDob());
    } catch (ParseException e) {
      throw new BadRequestException("Ngày sinh không hợp lệ");
    }

    User user = userRepository.findByEmail(request.getEmail());
    log.info("<Query> => Result finding user by email: {}", user);
    if (user != null) {
      throw new BadRequestException("Email học viên đã được đăng ký");
    }

    if (StringUtils.isNotBlank(request.getCreatedBy())) {
      Map<String, Object> parameters = Map.of("email", aesUtils.encrypt(request.getCreatedBy()));

      String url = eurekaClient
          .getNextServerFromEureka("admin", false).getHomePageUrl()
          + "internal/admin" + pathGetAdminByEmail;
      JsonObject resp = UnirestUtils.getWithoutHeader(url, parameters);

      log.info("Url: {}", url);
      log.info("Email: {}", request.getCreatedBy());
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject admin = resp.get("data").getAsJsonObject().get("admin").getAsJsonObject();
        nameCreateBy = admin.get("name").getAsString();
        emailCreateBy = admin.get("email").getAsString();
      } else {
        throw new BadRequestException("Không tìm thấy quản trị viên tạo học viên");
      }
    } else {
      emailCreateBy = null;
      nameCreateBy = null;
    }
  }

  @Override
  public ResponseEntity<?> process(CreateUserRequest request) {
    String urlCreateAccount = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathCreateAccount;
    Map<String, String> bodyCreateAccount =
        new HashMap<>(Map.of(
            "avatar", request.getAvatar(),
            "name", request.getName(),
            "email", request.getEmail(),
            "role", RoleEnums.User.name()));
    if (StringUtils.isNotBlank(nameCreateBy)) {
      bodyCreateAccount.put("nameCreateBy", nameCreateBy);
    }
    JsonObject respCreateAccount =
        UnirestUtils.postByBodyWithoutHeader(urlCreateAccount, gson.toJson(bodyCreateAccount));
    log.info("Url: {}", urlCreateAccount);
    log.info("Body: {}", gson.toJson(bodyCreateAccount));
    log.info("Result calling API: {}", respCreateAccount);

    if (HttpStatus.OK.value() != respCreateAccount.get("status").getAsInt()) {
      throw new ProcessException(respCreateAccount.get("message").getAsString());
    }

    User user = genericMapper.mapToType(request, User.class);
    user.setEmailAdminCreated(emailCreateBy);
    user.setGender(gender);
    user.setDob(dob);
    user.setState(StateEnums.ACTIVE.name());
    user.setCreatedBy(nameCreateBy);
    user.setCreatedDate(new Date());
    user.setUpdatedBy(nameCreateBy);
    user.setUpdatedDate(new Date());
    userRepository.save(user);

    String message = "Thêm học viên thành công";

    if (request.getClassId() != null && request.getClassId() != 0) {
      String urlRegisterStudy = eurekaClient
          .getNextServerFromEureka("course_and_class", false).getHomePageUrl()
          + "register" + pathRegisterStudy;
      Map<String, Object> bodyRegisterStudy =
          Map.of("userEmail", user.getEmail(),
              "classId", request.getClassId(),
              "adminEmail", emailCreateBy);
      JsonObject respRegisterStudy =
          UnirestUtils.postByBodyWithoutHeader(urlRegisterStudy, gson.toJson(bodyRegisterStudy));
      log.info("Url: {}", urlCreateAccount);
      log.info("Body: {}", gson.toJson(bodyRegisterStudy));
      log.info("Result calling API: {}", respRegisterStudy);
      if (HttpStatus.OK.value() == respRegisterStudy.get("status").getAsInt()) {
        message = "Thêm học viên và đăng ký vào lớp học thành công";
      } else {
        message = "Thêm học viên thành công nhưng đăng ký vào lớp học thất bại";
      }
    }

    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailCreateAccount = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailUserCreated;

          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

          Map<String, String> body = new HashMap<>();
          body.put("name", user.getName());
          body.put("gender", GenderEnums.valueOf(user.getGender()).getValue());
          body.put("dob", UserUtils.sdf.format(user.getDob()));
          body.put("phone", user.getPhone());
          body.put("email", user.getEmail());
          body.put("collaborator", nameCreateBy);
          body.put("time", sdf.format(new Date()));

          UnirestUtils.postByBodyWithoutHeader(urlSendEmailCreateAccount, gson.toJson(body));
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);

    log.info("<Result API> => Creating user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
