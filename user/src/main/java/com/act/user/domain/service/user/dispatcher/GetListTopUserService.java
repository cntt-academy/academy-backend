package com.act.user.domain.service.user.dispatcher;

import com.act.user.application.request.user.GetListTopUserRequest;
import com.act.user.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListTopUserService
    extends BaseServiceRequestBody<GetListTopUserRequest, ResponseEntity<?>> {

}
