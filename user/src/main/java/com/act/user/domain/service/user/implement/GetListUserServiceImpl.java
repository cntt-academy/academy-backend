package com.act.user.domain.service.user.implement;

import com.act.user.Infrastructure.exception.BadRequestException;
import com.act.user.Infrastructure.utils.GenericMapper;
import com.act.user.Infrastructure.utils.UserUtils;
import com.act.user.application.enums.RoleEnums;
import com.act.user.application.request.user.GetListUserRequest;
import com.act.user.application.response.JsonResponseBase;
import com.act.user.domain.dto.UserOutputDto;
import com.act.user.domain.entity.User;
import com.act.user.domain.repository.UserRepository;
import com.act.user.domain.service.user.dispatcher.GetListUserService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListUserService")
public class GetListUserServiceImpl implements GetListUserService {

  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final UserUtils userUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListUserRequest request) {
    log.info("======== API GET LIST USER ========");
    log.info("Request get list user: {}", gson.toJson(request));
    return GetListUserService.super.execute(request);
  }

  @Override
  public void validate(GetListUserRequest request) {
    Map<String, String> loggedAccount = userUtils.getAccountLoginInfo();
    if (loggedAccount.get("role").equals(RoleEnums.User.name())) {
      throw new BadRequestException("Bạn không có quyền lấy ra danh sách tài khoản học viên");
    }
  }

  @Override
  public ResponseEntity<?> process(GetListUserRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<User> users = userRepository.findAllByCondition(
        UserUtils.standardizedParameterQueryLike(request.getSearch()),
        request.getIsTopStudent(),
        pageRequest);
    log.info("<Query> => Result getting list user by conditions: {}", gson.toJson(users));

    List<UserOutputDto> userOutputDtos = genericMapper.mapToListUserOutputDto(users.getContent());

    Map<String, Object> data =
        Map.of("users", userOutputDtos,
            "totalElements", users.getTotalElements(),
            "totalPages", users.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách học viên thành công");
    response.setData(data);

    log.info("<Result API> => Getting list user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
