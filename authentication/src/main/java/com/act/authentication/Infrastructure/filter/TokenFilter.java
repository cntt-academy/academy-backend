package com.act.authentication.Infrastructure.filter;

import com.act.authentication.Infrastructure.exception.UnauthorizedException;
import com.act.authentication.application.response.ErrorResponseBase;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.service.authentication.dispatcher.VerifyTokenService;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@RequiredArgsConstructor
public class TokenFilter extends OncePerRequestFilter {

  private final VerifyTokenService verifyTokenService;
  private final UserDetailsService userDetailsService;
  private final Gson gson;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws IOException {

    try {
      ResponseEntity<?> resp = verifyTokenService.execute(request);
      JsonResponseBase<?> jsonResp = (JsonResponseBase<?>) resp.getBody();
      if (jsonResp == null) {
        throw new UnauthorizedException("Verifying token fail");
      } else if (jsonResp.getStatus() != HttpStatus.OK.value()) {
        throw new UnauthorizedException(jsonResp.getMessage());
      }

      Map<String, String> data = (Map<String, String>) jsonResp.getData();
      String email = data.get("email");
      String name = data.get("name");
      String role = data.get("role");

      UserDetails userDetails = userDetailsService.loadUserByUsername(email);
      if (userDetails == null) {
        throw new UnauthorizedException("UserDetails get from token is null");
      }

      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
      authentication.setDetails(new UserInfo(email, name, role));
      SecurityContextHolder.getContext().setAuthentication(authentication);

      filterChain.doFilter(request, response);
    } catch (Exception e) {
      log.error("Filter token error: {}", e.getMessage());
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      response.setContentType("application/json");
      response.getWriter()
          .write(gson.toJson(new ErrorResponseBase(401, HttpStatus.UNAUTHORIZED, e.getMessage())));
    }
  }
}
