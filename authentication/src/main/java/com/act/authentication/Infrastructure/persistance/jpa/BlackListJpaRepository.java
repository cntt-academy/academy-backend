package com.act.authentication.Infrastructure.persistance.jpa;

import com.act.authentication.domain.entity.BlackList;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlackListJpaRepository extends JpaRepository<BlackList, Integer> {

  Optional<BlackList> findByToken(String token);
}