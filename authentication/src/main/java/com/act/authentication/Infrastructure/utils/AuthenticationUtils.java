package com.act.authentication.Infrastructure.utils;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.filter.UserInfo;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import java.text.SimpleDateFormat;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("AuthenticationUtils")
public class AuthenticationUtils {

  public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
  private final AccountRepository accountRepository;

  /**
   * Chuẩn hóa parameter Query Like
   */
  public static String standardizedParameterQueryLike(String str) {
    return str == null || str.trim().isEmpty() ? null : "%" + str + "%";
  }

  /**
   * Lấy ra thông tin Account đang login
   */
  public Map<String, String> getAccountLoginInfo() {
    log.info("<AdminUtils> => Function getAdminLoginInfo");

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserInfo userInfo = (UserInfo) authentication.getDetails();

    String email = userInfo.getEmail();
    log.info("Email: {}", email);
    String name = userInfo.getName();
    log.info("Name: {}", name);
    String role = userInfo.getRole();
    log.info("Role: {}", role);

    return Map.of("email", email, "name", name, "role", role);
  }

  /**
   * Lấy ra email của Giám đốc trung tâm
   */
  public String getOwnerEmail() {
    log.info("<AdminUtils> => Function getOwnerEmail");
    String ownerEmail = ManagerInfo.OWNER_EMAIL;
    log.info("Retrieve the academy director email stored in the system constant: {}", ownerEmail);
    if (StringUtils.isBlank(ownerEmail)) {
      log.info("Query searches the database and stores it in the system constant");
      Account owner = accountRepository.findByRole(RoleEnums.Owner.name());
      log.info("<Query> => Result finding account by role: {}", owner);
      if (owner != null) {
        log.info("Admin email: {}", owner.getEmail());
        ManagerInfo.OWNER_EMAIL = owner.getEmail();
        ownerEmail = owner.getEmail();
      }
    }
    log.info("<AdminUtils> => Result function getOwnerEmail: {}", ownerEmail);
    return ownerEmail;
  }

  /**
   * Lấy ra email của Quản lý trung tâm
   */
  public String getManagerEmail() {
    log.info("<AdminUtils> => getManagerEmail");
    String adminEmail = ManagerInfo.ADMIN_EMAIL;
    log.info("Retrieve the academy manager email stored in the system constant: {}", adminEmail);
    if (StringUtils.isBlank(adminEmail)) {
      log.info("Query searches the database and stores it in the system constant");
      Account admin = accountRepository.findByRole(RoleEnums.Manager.name());
      log.info("<Query> => Find account by role result: {}", admin);
      if (admin != null) {
        log.info("Admin email: {}", admin.getEmail());
        ManagerInfo.ADMIN_EMAIL = admin.getEmail();
        adminEmail = admin.getEmail();
      }
    }
    log.info("<AdminUtils> => Result getManagerEmail: {}", adminEmail);
    return adminEmail;
  }

  /**
   * Kiểm tra quyền của tài khoản
   */
  public Boolean isOwnerOrManager(String email) {
    return StringUtils.isNotBlank(email)
        && (email.equals(getOwnerEmail()) || email.equals(getManagerEmail()));
  }

  /**
   * Validate và chuẩn hóa Role
   */
  public String validateAndStandardizedRole(String role) {
    try {
      RoleEnums roleEnum = RoleEnums.valueOf(StringUtils.capitalize(role.toLowerCase()));
      return roleEnum.name();
    } catch (IllegalArgumentException ex) {
      throw new BadRequestException("Role không hợp lệ");
    }
  }
}
