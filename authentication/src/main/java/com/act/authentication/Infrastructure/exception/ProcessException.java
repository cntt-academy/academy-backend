package com.act.authentication.Infrastructure.exception;

public class ProcessException extends RuntimeException {

  public ProcessException(String message) {
    super(message);
  }
}
