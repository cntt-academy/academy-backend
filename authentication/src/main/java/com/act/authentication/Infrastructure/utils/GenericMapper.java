package com.act.authentication.Infrastructure.utils;

import com.act.authentication.domain.dto.AccountOutputDto;
import com.act.authentication.domain.entity.Account;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component("GenericMapper")
public class GenericMapper {

  private final ModelMapper modelMapper;

  public GenericMapper(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public <T, E> E mapToType(T source, Class<E> typeDestination) {
    if (source == null) {
      return null;
    }
    return modelMapper.map(source, typeDestination);
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (source == null || source.isEmpty()) {
      return null;
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }

  public void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  public String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  public AccountOutputDto mapToAccountOutputDto(Account account) {
    AccountOutputDto accountOutputDto = mapToType(account, AccountOutputDto.class);
    accountOutputDto.setAccountId(account.getId());
    accountOutputDto.setId(null);
    return accountOutputDto;
  }

  public List<AccountOutputDto> mapToListAccountOutputDto(List<Account> accounts) {
    if (CollectionUtils.isEmpty(accounts)) {
      return new ArrayList<>();
    }
    return accounts.stream()
        .map(this::mapToAccountOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}
