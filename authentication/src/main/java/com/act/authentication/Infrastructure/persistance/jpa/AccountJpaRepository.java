package com.act.authentication.Infrastructure.persistance.jpa;

import com.act.authentication.domain.entity.Account;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountJpaRepository extends JpaRepository<Account, Integer> {

  Optional<Account> findByEmailAndRoleAndState(String email, String role, String state);

  Optional<Account> findByIdAndState(Integer accountId, String state);

  Optional<Account> findByEmailAndState(String email, String state);

  Optional<Account> findByRoleAndState(String role, String state);

}