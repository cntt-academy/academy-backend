package com.act.authentication.Infrastructure.persistance.impl;

import com.act.authentication.Infrastructure.persistance.jpa.AccountJpaRepository;
import com.act.authentication.Infrastructure.utils.GenericMapper;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.enums.StateEnums;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@RequiredArgsConstructor
@Repository("AccountRepository")
public class AccountRepositoryImpl implements AccountRepository {

  private final AccountJpaRepository accountJpaRepository;
  private final GenericMapper genericMapper;

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public Account findByRole(String role) {
    return accountJpaRepository
        .findByRoleAndState(role, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public Account findByEmail(String email) {
    return accountJpaRepository
        .findByEmailAndState(email, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public Account findById(Integer accountId) {
    return accountJpaRepository
        .findByIdAndState(accountId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public List<Account> findAllByKey(String key, String loggedEmail) {
    String sql =
        "SELECT DISTINCT(a) FROM Account a "
            + "WHERE (:key IS NULL OR (a.name LIKE :key OR a.email LIKE :key)) "
            + "AND a.email <> :loggedEmail "
            + "AND a.state <> :stateNot "
            + "AND a.role <> :roleNot "
            + "ORDER BY a.createdDate ";

    Query query = entityManager.createQuery(sql);
    query.setParameter("stateNot", StateEnums.DELETED.name());
    query.setParameter("roleNot", RoleEnums.Owner.name());
    query.setParameter("loggedEmail", loggedEmail);
    query.setParameter("key", key);
    query.setMaxResults(10);

    List<?> resultList = query.getResultList();
    return genericMapper.mapToListOfType(resultList, Account.class);
  }

  @Override
  public Account findByEmailAndRole(String email, String role) {
    return accountJpaRepository
        .findByEmailAndRoleAndState(email, role, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public void save(Account account) {
    accountJpaRepository.save(account);
  }

  @Override
  public void saveAll(List<Account> accounts) {
    accountJpaRepository.saveAll(accounts);
  }
}
