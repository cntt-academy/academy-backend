package com.act.authentication.Infrastructure.utils;

import com.act.authentication.Infrastructure.exception.ProcessException;
import com.act.authentication.Infrastructure.filter.CustomUserDetails;
import com.act.authentication.application.enums.RoleEnums;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("TokenUtils")
public class TokenUtils {

  @Value("${secret.key}")
  private String secretKey;
  @Value("${admin.expire.time}")
  private String adminExpireTime;
  @Value("${user.expire.time}")
  private String userExpireTime;

  public Map<?, ?> generateToken(CustomUserDetails userDetails) {
    log.info("<TokenUtils> => GenerateToken");
    log.info("=> Set the token expiration time by the role of the account");
    String role = userDetails.getAccount().getRole();
    log.info("Role: {}", role);
    Date now = new Date();
    log.info("Time: {}", now);

    int expiry;
    Date expiryDate;
    if (role.equals(RoleEnums.User.name())) {
      expiry = Integer.parseInt(userExpireTime);
      expiryDate = new Date(now.getTime() + Long.parseLong(userExpireTime));
    } else {
      expiry = Integer.parseInt(adminExpireTime);
      expiryDate = new Date(now.getTime() + Long.parseLong(adminExpireTime));
    }
    log.info("ExpiryDate: {}", expiryDate);

    log.info("=> Create JWT from user's email");
    String token = Jwts.builder()
        .setSubject(userDetails.getAccount().getEmail())
        .claim("accountId", userDetails.getAccount().getId())
        .claim("name", userDetails.getAccount().getName())
        .claim("role", role)
        .setIssuedAt(now)
        .setExpiration(expiryDate)
        .signWith(SignatureAlgorithm.HS512, secretKey)
        .compact();
    Map<?, ?> result = Map.of("role", role, "expiry", expiry, "token", token);

    log.info("<TokenUtils> => Generating Token successfully");
    return result;
  }

  public Jws<Claims> verifyToken(String token) {
    try {
      return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
    } catch (MalformedJwtException ex) {
      throw new ProcessException("Token không hợp lệ");
    } catch (ExpiredJwtException ex) {
      throw new ProcessException("Token đã hết hạn");
    } catch (Exception ex) {
      throw new ProcessException("Verify token thất bại");
    }
  }
}
