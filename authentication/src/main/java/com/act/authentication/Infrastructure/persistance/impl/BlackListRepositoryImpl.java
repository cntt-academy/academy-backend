package com.act.authentication.Infrastructure.persistance.impl;

import com.act.authentication.Infrastructure.persistance.jpa.BlackListJpaRepository;
import com.act.authentication.domain.entity.BlackList;
import com.act.authentication.domain.repository.BlackListRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@RequiredArgsConstructor
@Repository("BlackListRepository")
public class BlackListRepositoryImpl implements BlackListRepository {

  private final BlackListJpaRepository blackListJpaRepository;

  @Override
  public BlackList findByToken(String token) {
    return blackListJpaRepository.findByToken(token).orElse(null);
  }

  @Override
  public void save(BlackList blackList) {
    blackListJpaRepository.save(blackList);
  }
}
