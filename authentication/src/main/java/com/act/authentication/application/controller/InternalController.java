package com.act.authentication.application.controller;

import com.act.authentication.application.request.account.ChangeManagerRequest;
import com.act.authentication.application.request.account.CreateAccountRequest;
import com.act.authentication.application.request.account.DeleteAccountRequest;
import com.act.authentication.application.request.account.UpdateAccountRequest;
import com.act.authentication.domain.service.internal.dispatcher.ChangeManagerService;
import com.act.authentication.domain.service.internal.dispatcher.CreateAccountService;
import com.act.authentication.domain.service.internal.dispatcher.DeleteAccountService;
import com.act.authentication.domain.service.internal.dispatcher.GetAccountByIdService;
import com.act.authentication.domain.service.internal.dispatcher.UpdateAccountService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("InternalController")
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalController {

  private final GetAccountByIdService getAccountByIdService;
  private final CreateAccountService createAccountService;
  private final UpdateAccountService updateAccountService;
  private final DeleteAccountService deleteAccountService;
  private final ChangeManagerService changeManagerService;

  @GetMapping(value = "/account/getById")
  public Object getAccountById(@RequestParam String accountId) {
    return getAccountByIdService.execute(accountId);
  }

  @PostMapping(value = "/account/create")
  public Object createAccount(@Valid @RequestBody CreateAccountRequest request) {
    return createAccountService.execute(request);
  }

  @PutMapping(value = "/account/update")
  public Object updateAccount(@Valid @RequestBody UpdateAccountRequest request) {
    return updateAccountService.execute(request);
  }

  @DeleteMapping("/account/delete")
  public Object deleteAccount(@Valid @RequestBody DeleteAccountRequest request) {
    return deleteAccountService.execute(request);
  }

  @PutMapping(value = "/authentication/changeManager")
  public Object changeManager(@Valid @RequestBody ChangeManagerRequest request) {
    return changeManagerService.execute(request);
  }
}
