package com.act.authentication.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateEnums {
  ACTIVE("Hoạt động"),
  DELETED("Xóa");
  private final String value;
}
