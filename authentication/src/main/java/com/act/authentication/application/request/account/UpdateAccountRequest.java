package com.act.authentication.application.request.account;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UpdateAccountRequest implements Serializable {

  @NotBlank(message = "Email")
  private String email;

  private String avatar;
  
  private String name;

  @NotBlank(message = "Người chỉnh sửa")
  private String updatedBy;
}
