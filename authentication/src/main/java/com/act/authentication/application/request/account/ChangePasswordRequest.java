package com.act.authentication.application.request.account;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ChangePasswordRequest implements Serializable {

  @NotBlank(message = "Mật khẩu cũ")
  private String oldPassword;

  @NotBlank(message = "Mật khẩu mới")
  private String newPassword;
}
