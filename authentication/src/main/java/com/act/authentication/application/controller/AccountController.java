package com.act.authentication.application.controller;

import com.act.authentication.application.request.account.ChangePasswordRequest;
import com.act.authentication.domain.service.account.dispatcher.ChangePasswordService;
import com.act.authentication.domain.service.account.dispatcher.FindAccountByKeyService;
import com.act.authentication.domain.service.account.dispatcher.ForgetPasswordService;
import com.act.authentication.domain.service.account.dispatcher.GetLoggedAccountInfoService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("AccountController")
@RequestMapping(value = "/account", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

  private final GetLoggedAccountInfoService getLoggedAccountInfoService;
  private final FindAccountByKeyService findAccountByKeyService;
  private final ForgetPasswordService forgetPasswordService;
  private final ChangePasswordService changePasswordService;

  @GetMapping(value = "/getLoggedInfo")
  public Object getLoggedAccountInfo() {
    return getLoggedAccountInfoService.execute();
  }

  @GetMapping(value = "/findByKey")
  public Object findAccountByKey(@RequestParam String key) {
    return findAccountByKeyService.execute(key);
  }

  @GetMapping(value = "/forgetPassword")
  public Object forgetPassword(@RequestParam String email) {
    return forgetPasswordService.execute(email);
  }

  @PutMapping(value = "/changePassword")
  public Object changePassword(@Valid @RequestBody ChangePasswordRequest request) {
    return changePasswordService.execute(request);
  }
}
