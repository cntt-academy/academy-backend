package com.act.authentication.application.request.account;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ChangeManagerRequest implements Serializable {

  @NotBlank(message = "Email quản lý mới")
  private String email;

  @NotBlank(message = "Người chỉnh sửa")
  private String updatedBy;
}
