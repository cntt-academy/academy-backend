package com.act.authentication.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RedisEnums {
  ID,
  ACCOUNT_ID,
  AVATAR,
  NAME,
  GENDER,
  DOB,
  PHONE,
  EMAIL,
  ROLE,
  ADDRESS,
  JOB,
  WORK_PLACE,
  CREATED_BY,
  CREATED_DATE,
  IS_TOP_STUDENT,
  FEEDBACK,
}
