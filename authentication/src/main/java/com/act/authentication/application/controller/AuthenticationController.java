package com.act.authentication.application.controller;

import com.act.authentication.application.request.authentication.LoginRequest;
import com.act.authentication.domain.service.authentication.dispatcher.LoginService;
import com.act.authentication.domain.service.authentication.dispatcher.LogoutService;
import com.act.authentication.domain.service.authentication.dispatcher.VerifyTokenService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("AuthenticationController")
@RequestMapping(value = "/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

  private final VerifyTokenService verifyTokenService;
  private final LogoutService logoutService;
  private final LoginService loginService;

  @GetMapping(value = "/verifyToken")
  public Object verifyToken(HttpServletRequest request) {
    return verifyTokenService.execute(request);
  }

  @GetMapping(value = "/logout")
  public Object logout(HttpServletRequest request) {
    return logoutService.execute(request);
  }

  @PostMapping(value = "/login")
  public Object login(@Valid @RequestBody LoginRequest request) {
    return loginService.execute(request);
  }
}
