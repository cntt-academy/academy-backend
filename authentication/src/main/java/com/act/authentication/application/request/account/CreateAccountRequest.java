package com.act.authentication.application.request.account;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CreateAccountRequest implements Serializable {

  @NotBlank(message = "Ảnh đại diện")
  private String avatar;
  
  @NotBlank(message = "Họ và tên")
  private String name;

  @NotBlank(message = "Email")
  private String email;

  @NotBlank(message = "Role")
  private String role;

  private String emailCreateBy;
  private String nameCreateBy;
}
