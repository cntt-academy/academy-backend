package com.act.authentication.application.request.account;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DeleteAccountRequest implements Serializable {

  @NotBlank(message = "Email")
  private String email;

  @NotBlank(message = "Role")
  private String role;

  @NotBlank(message = "Người xóa")
  private String deletedBy;
}
