package com.act.authentication.application.request.authentication;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class LoginRequest implements Serializable {

  @NotBlank(message = "Username")
  private String username;

  @NotBlank(message = "Password")
  private String password;
}
