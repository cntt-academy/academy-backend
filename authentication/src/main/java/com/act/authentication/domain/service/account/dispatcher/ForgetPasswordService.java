package com.act.authentication.domain.service.account.dispatcher;

import com.act.authentication.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface ForgetPasswordService extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
