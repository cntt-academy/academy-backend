package com.act.authentication.domain.service.authentication.dispatcher;

import com.act.authentication.application.request.authentication.LoginRequest;
import com.act.authentication.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface LoginService extends BaseServiceRequestBody<LoginRequest, ResponseEntity<?>> {

}
