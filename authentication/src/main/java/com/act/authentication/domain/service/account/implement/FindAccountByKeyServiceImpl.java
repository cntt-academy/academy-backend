package com.act.authentication.domain.service.account.implement;

import com.act.authentication.Infrastructure.utils.AuthenticationUtils;
import com.act.authentication.Infrastructure.utils.GenericMapper;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.dto.AccountOutputDto;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.account.dispatcher.FindAccountByKeyService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("FindAccountByKeyService")
public class FindAccountByKeyServiceImpl implements FindAccountByKeyService {

  private final AuthenticationUtils authenticationUtils;
  private final AccountRepository accountRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(String keySearch) {
    log.info("======== API GET LIST ACCOUNT ========");
    log.info("Key search: {}", gson.toJson(keySearch));
    return FindAccountByKeyService.super.execute(keySearch);
  }

  @Override
  public void validate(String keySearch) {
  }

  @Override
  public ResponseEntity<?> process(String keySearch) {
    Map<String, Object> data = Map.of("accounts", new ArrayList<>(), "totalElements", 0);

    if (StringUtils.isNotBlank(keySearch)) {
      Map<String, String> loggedAccount = authenticationUtils.getAccountLoginInfo();

      List<Account> accounts =
          accountRepository.findAllByKey(
              AuthenticationUtils.standardizedParameterQueryLike(keySearch),
              loggedAccount.get("email"));
      log.info("<Query> => Result find all account by key: {}", gson.toJson(accounts));

      List<AccountOutputDto> accountOutputDtos = genericMapper.mapToListAccountOutputDto(accounts);

      data = Map.of("accounts", accountOutputDtos, "totalElements", accountOutputDtos.size());
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách tài khoản thành công");
    response.setData(data);

    log.info("<Result API> => Getting list account successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
