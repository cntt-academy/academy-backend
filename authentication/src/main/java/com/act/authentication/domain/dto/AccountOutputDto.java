package com.act.authentication.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

/**
 * A DTO for the {@link com.act.authentication.domain.entity.Account} entity
 */
@Data
@JsonInclude(Include.NON_NULL)
public class AccountOutputDto {

  private Integer id;
  private Integer accountId;
  private String avatar;
  private String name;
  private String gender;
  private String dob;
  private String phone;
  private String email;
  private String role;
  private String address;
  private String job;
  private String workplace;
  private Boolean isTopStudent;
  private String feedback;
  private String emailAdminCreated;
  private String createdBy;
  private String createdDate;
}
