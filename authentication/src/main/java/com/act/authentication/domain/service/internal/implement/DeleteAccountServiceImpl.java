package com.act.authentication.domain.service.internal.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.utils.AuthenticationUtils;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.enums.StateEnums;
import com.act.authentication.application.request.account.DeleteAccountRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.internal.dispatcher.DeleteAccountService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteAccountService")
public class DeleteAccountServiceImpl implements DeleteAccountService {

  private final AuthenticationUtils authenticationUtils;
  private final AccountRepository accountRepository;
  private final Gson gson;

  private Account account;

  @Override
  public ResponseEntity<?> execute(DeleteAccountRequest request) {
    log.info("======== API DELETE ACCOUNT ========");
    log.info("Request delete account: {}", gson.toJson(request));
    return DeleteAccountService.super.execute(request);
  }

  @Override
  public void validate(DeleteAccountRequest request) {
    String email = request.getEmail();
    String role = authenticationUtils.validateAndStandardizedRole(request.getRole());

    account = accountRepository.findByEmailAndRole(email, role);
    log.info("<Query> => Result finding account by email and role: {}", account);
    if (account == null) {
      throw new BadRequestException("Không tìm thấy thông tin tài khoản theo email và role");
    }

    if (account.getRole().equals(RoleEnums.Manager.name())
        || account.getRole().equals(RoleEnums.Owner.name())) {
      throw new BadRequestException("Không được phép xóa tài khoản quản lý hoặc giám đốc trung tâm");
    }
  }

  @Override
  public ResponseEntity<?> process(DeleteAccountRequest request) {
    account.setState(StateEnums.DELETED.name());
    account.setUpdatedBy(request.getDeletedBy());
    account.setUpdatedDate(new Date());
    accountRepository.save(account);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa tài khoản thành công");

    log.info("<Result API> => Account deleted successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
