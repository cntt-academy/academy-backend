package com.act.authentication.domain.service.account.dispatcher;

import com.act.authentication.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface FindAccountByKeyService extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
