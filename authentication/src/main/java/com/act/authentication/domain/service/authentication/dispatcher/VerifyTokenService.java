package com.act.authentication.domain.service.authentication.dispatcher;

import com.act.authentication.domain.service.base.BaseServiceRequestParam;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;

public interface VerifyTokenService extends
    BaseServiceRequestParam<HttpServletRequest, ResponseEntity<?>> {

}
