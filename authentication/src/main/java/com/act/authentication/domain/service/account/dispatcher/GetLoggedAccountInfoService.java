package com.act.authentication.domain.service.account.dispatcher;

import com.act.authentication.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetLoggedAccountInfoService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
