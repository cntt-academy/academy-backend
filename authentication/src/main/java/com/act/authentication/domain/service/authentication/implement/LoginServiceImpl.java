package com.act.authentication.domain.service.authentication.implement;


import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.filter.CustomUserDetails;
import com.act.authentication.Infrastructure.utils.TokenUtils;
import com.act.authentication.application.request.authentication.LoginRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.authentication.dispatcher.LoginService;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("LoginService")
public class LoginServiceImpl implements LoginService {

  private final AuthenticationManager authenticationManager;
  private final AccountRepository accountRepository;
  private final PasswordEncoder passwordEncoder;
  private final TokenUtils tokenUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(LoginRequest request) {
    log.info("======== API LOGIN ========");
    log.info("Username want to login: {}", request.getUsername());
    return LoginService.super.execute(request);
  }

  @Override
  public void validate(LoginRequest request) {
    Account account = accountRepository.findByEmail(request.getUsername());
    log.info("<Query> => Result finding account by email: {}", account);
    if (account == null) {
      throw new BadRequestException("Không tìm thấy tài khoản theo tên đăng nhập");
    }

    if (!passwordEncoder.matches(request.getPassword(), account.getPassword())) {
      throw new BadRequestException("Mật khẩu đăng nhập không đúng");
    }
  }

  @Override
  public ResponseEntity<?> process(LoginRequest request) {
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

    Map<?, ?> resultGenerate =
        tokenUtils.generateToken((CustomUserDetails) authentication.getPrincipal());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đăng nhập thành công");
    Map<String, Object> data = new HashMap<>();
    data.put("username", request.getUsername());
    data.put("role", resultGenerate.get("role"));
    data.put("expiry", resultGenerate.get("expiry"));
    data.put("token", resultGenerate.get("token"));
    response.setData(data);

    log.info("<Result API> => Login successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
