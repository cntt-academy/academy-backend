package com.act.authentication.domain.service.account.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.exception.ProcessException;
import com.act.authentication.Infrastructure.utils.AESUtils;
import com.act.authentication.Infrastructure.utils.AuthenticationUtils;
import com.act.authentication.Infrastructure.utils.UnirestUtils;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.account.dispatcher.ForgetPasswordService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ForgetPasswordService")
public class ForgetPasswordServiceImpl implements ForgetPasswordService {

  private final AuthenticationUtils authenticationUtils;
  private final AccountRepository accountRepository;
  private final PasswordEncoder passwordEncoder;
  private final EurekaClient eurekaClient;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Value("${path.sendEmail.forgetPassword}")
  private String pathSendEmailForgetPassword;
  private Account account;

  @Override
  public ResponseEntity<?> execute(String email) {
    log.info("======== API FORGET PASSWORD ========");
    log.info("Email forgotten password: {}", email);
    return ForgetPasswordService.super.execute(email);
  }

  @Override
  public void validate(String email) {
    account = accountRepository.findByEmail(aesUtils.decrypt(email));
    log.info("<Query> => Result finding account by email: {}", account);
    if (account == null) {
      throw new BadRequestException("Không tìm thấy thông tin tài khoản theo email");
    }
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(String email) {
    String time = AuthenticationUtils.sdf.format(new Date());

    String specialCharacters = "!@#$%^&*()-_=+{}[]|:;<>,.?~";
    String specialCharacter =
        RandomStringUtils.random(2, specialCharacters.replace("/", "").replace("\\", ""));
    String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
    String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
    String numbers = RandomStringUtils.randomNumeric(2);
    String password =
        upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(specialCharacter);

    account.setPassword(passwordEncoder.encode(password));
    account.setUpdatedBy(null);
    account.setUpdatedDate(new Date());
    accountRepository.save(account);

    // Nếu account có role là User => Gửi email về người dùng
    // Nếu account có role là Collaborator hoặc Admin => Gửi email về Quản lý trung tâm
    CompletableFuture<String> sendEmail = CompletableFuture.supplyAsync(
        () -> {
          try {
            String urlSendEmailForgetPassword = eurekaClient
                .getNextServerFromEureka("message", false).getHomePageUrl()
                + "email" + pathSendEmailForgetPassword;

            String sendTo = account.getEmail();
            if (account.getRole().equals(RoleEnums.Collaborator.name())) {
              sendTo = authenticationUtils.getManagerEmail();
            }

            Map<String, String> body = new HashMap<>();
            body.put("email", sendTo);
            body.put("role", account.getRole());
            body.put("newPassword", aesUtils.encrypt(password));
            body.put("time", time);

            JsonObject resp =
                UnirestUtils.postByBodyWithoutHeader(urlSendEmailForgetPassword, gson.toJson(body));
            if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
              return "success";
            }
          } catch (Exception ignored) {
          }
          return "fail";
        });

    if (sendEmail.get().equals("fail")) {
      throw new ProcessException("Đã xảy ra lỗi không mong muốn. Xin hãy thử lại");
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    if (account.getRole().equals(RoleEnums.Collaborator.name())) {
      response.setMessage("Xin hãy liên hệ với Quản lý để được nhận kết quả xử lý");
    } else {
      response.setMessage("Xin hãy kiểm tra hòm thư để nhận kết quả xử lý");
    }

    log.info("<Result API> => Email contained new password has been sent");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
