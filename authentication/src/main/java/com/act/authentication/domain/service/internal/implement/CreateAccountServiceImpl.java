package com.act.authentication.domain.service.internal.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.utils.AuthenticationUtils;
import com.act.authentication.Infrastructure.utils.UnirestUtils;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.enums.StateEnums;
import com.act.authentication.application.request.account.CreateAccountRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.internal.dispatcher.CreateAccountService;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateAccountService")
public class CreateAccountServiceImpl implements CreateAccountService {

  private final AuthenticationUtils authenticationUtils;
  private final AccountRepository accountRepository;
  private final PasswordEncoder passwordEncoder;
  private final EurekaClient eurekaClient;
  private final Gson gson;

  @Value("${path.sendEmail.accountCreated}")
  private String pathSendEmailAccountCreated;
  private String role;

  @Override
  public ResponseEntity<?> execute(CreateAccountRequest request) {
    log.info("======== API CREATE ACCOUNT ========");
    log.info("Create account request: {}", gson.toJson(request));
    return CreateAccountService.super.execute(request);
  }

  @Override
  public void validate(CreateAccountRequest request) {
    Pattern pattern = Pattern.compile("^[^\\s@]+@[^\\s@]+$");
    Matcher matcher = pattern.matcher(request.getEmail());
    if (!matcher.matches()) {
      throw new BadRequestException("Email tài khoản không hợp lệ");
    }

    role = authenticationUtils.validateAndStandardizedRole(request.getRole());

    if (request.getRole().equals(RoleEnums.Owner.name())
        || request.getRole().equals(RoleEnums.Manager.name())) {
      throw new BadRequestException("Không được phép tạo tài khoản quản lý");
    }

    if (request.getRole().equals(RoleEnums.Collaborator.name())
        && !authenticationUtils.isOwnerOrManager(request.getEmailCreateBy())) {
      throw new BadRequestException("Bạn không có quyền tạo tài khoản cộng tác viên");
    }

    Account account = accountRepository.findByEmail(request.getEmail());
    log.info("<Query> => Result finding account by email: {}", account);
    if (account != null) {
      throw new BadRequestException("Email đã được đăng ký tài khoản");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateAccountRequest request) {
    Account account = new Account();
    account.setAvatar(request.getAvatar());
    account.setName(request.getName());
    account.setEmail(request.getEmail());
    account.setPassword(passwordEncoder.encode("Abc@1234"));
    account.setRole(role);
    account.setState(StateEnums.ACTIVE.name());
    account.setCreatedBy(request.getNameCreateBy());
    account.setCreatedDate(new Date());
    account.setUpdatedBy(request.getNameCreateBy());
    account.setUpdatedDate(new Date());
    accountRepository.save(account);

    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailCreateAccount = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailAccountCreated;

          Map<String, String> body = new HashMap<>();
          body.put("email", account.getEmail());
          body.put("role", account.getRole());
          body.put("time", AuthenticationUtils.sdf.format(new Date()));

          UnirestUtils.postByBodyWithoutHeader(urlSendEmailCreateAccount, gson.toJson(body));
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Tạo tài khoản thành công");

    log.info("<Result API> => Creating account successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
