package com.act.authentication.domain.service.account.dispatcher;

import com.act.authentication.application.request.account.ChangePasswordRequest;
import com.act.authentication.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface ChangePasswordService extends
    BaseServiceRequestBody<ChangePasswordRequest, ResponseEntity<?>> {

}
