package com.act.authentication.domain.service.base;

public interface BaseServiceNoRequest<O> {

  O execute();
}
