package com.act.authentication.domain.service.authentication.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.utils.TokenUtils;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.BlackList;
import com.act.authentication.domain.repository.BlackListRepository;
import com.act.authentication.domain.service.authentication.dispatcher.VerifyTokenService;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("VerifyTokenService")
public class VerifyTokenServiceImpl implements VerifyTokenService {

  private final BlackListRepository blackListRepository;
  private final TokenUtils tokenUtils;
  private final Gson gson;
  private String token;

  @Override
  public ResponseEntity<?> execute(HttpServletRequest request) {
    log.info("======== API VALIDATE TOKEN ========");
    return VerifyTokenService.super.execute(request);
  }

  @Override
  public void validate(HttpServletRequest request) {
    token = request.getHeader("Authorization").replace("Bearer", "").trim();
    log.info("Verify token: {}", token);

    if (StringUtils.isBlank(token)) {
      throw new BadRequestException("Token không hợp lệ");
    }

    BlackList blockToken = blackListRepository.findByToken(token);
    log.info("<Query> => Result finding blockToken: {}", blockToken);
    if (blockToken != null) {
      throw new BadRequestException("Token đã hết hiệu lực");
    }
  }

  @Override
  public ResponseEntity<?> process(HttpServletRequest request) {
    Jws<Claims> claimsJws = tokenUtils.verifyToken(token);

    Claims claims = claimsJws.getBody();
    String email = claims.getSubject();
    Integer accountId = (Integer) claims.get("accountId");
    String name = (String) claims.get("name");
    String role = (String) claims.get("role");

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xác thực token thành công");
    response.setData(
        Map.of("accountId", accountId, "email", email, "name", name, "role", role));

    log.info("<Result API> => Verify token successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
