package com.act.authentication.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "black_list")
public class BlackList {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Column(name = "token", nullable = false)
  private String token;

  @NotNull
  @Column(name = "block_date", nullable = false)
  private Date blockDate;

  @Override
  public String toString() {
    return "BlackList{" +
        "id=" + id +
        ", token='" + token + '\'' +
        ", blockDate=" + blockDate +
        '}';
  }
}