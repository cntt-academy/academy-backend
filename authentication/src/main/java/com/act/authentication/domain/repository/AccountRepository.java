package com.act.authentication.domain.repository;

import com.act.authentication.domain.entity.Account;
import java.util.List;

public interface AccountRepository {

  Account findByRole(String role);

  Account findByEmail(String email);

  Account findById(Integer accountId);

  List<Account> findAllByKey(String key, String loggedEmail);

  Account findByEmailAndRole(String email, String role);

  void save(Account account);

  void saveAll(List<Account> accounts);
}
