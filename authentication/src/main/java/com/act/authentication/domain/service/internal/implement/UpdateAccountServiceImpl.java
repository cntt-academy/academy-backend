package com.act.authentication.domain.service.internal.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.application.request.account.UpdateAccountRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.internal.dispatcher.UpdateAccountService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("UpdateAccountService")
public class UpdateAccountServiceImpl implements UpdateAccountService {

  private final AccountRepository accountRepository;
  private final Gson gson;
  private Account account;

  @Override
  public ResponseEntity<?> execute(UpdateAccountRequest request) {
    log.info("======== API UPDATE ACCOUNT NAME ========");
    log.info("Request update name: {}", gson.toJson(request));
    return UpdateAccountService.super.execute(request);
  }

  @Override
  public void validate(UpdateAccountRequest request) {
    account = accountRepository.findByEmail(request.getEmail());
    log.info("<Query> => Result finding account by email: {}", account);
    if (account == null) {
      throw new BadRequestException("Không tìm thấy thông tin tài khoản");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateAccountRequest request) {
    if (StringUtils.isNotBlank(request.getAvatar())) {
      account.setAvatar(request.getAvatar());
    }
    if (StringUtils.isNotBlank(request.getName())) {
      account.setName(request.getName());
    }
    account.setUpdatedBy(request.getUpdatedBy());
    account.setUpdatedDate(new Date());
    accountRepository.save(account);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật tên tài khoản thành công");

    log.info("<Result API> => Name account updated successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
