package com.act.authentication.domain.service.internal.dispatcher;

import com.act.authentication.application.request.account.ChangeManagerRequest;
import com.act.authentication.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface ChangeManagerService extends
    BaseServiceRequestBody<ChangeManagerRequest, ResponseEntity<?>> {

}
