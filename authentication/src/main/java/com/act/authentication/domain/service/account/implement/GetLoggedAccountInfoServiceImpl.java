package com.act.authentication.domain.service.account.implement;

import com.act.authentication.Infrastructure.exception.ProcessException;
import com.act.authentication.Infrastructure.utils.AESUtils;
import com.act.authentication.Infrastructure.utils.RedisUtils;
import com.act.authentication.Infrastructure.utils.UnirestUtils;
import com.act.authentication.application.enums.RedisEnums;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.dto.AccountOutputDto;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.account.dispatcher.GetLoggedAccountInfoService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetLoggedAccountInfoService")
public class GetLoggedAccountInfoServiceImpl implements GetLoggedAccountInfoService {

  private final AccountRepository accountRepository;
  private final EurekaClient eurekaClient;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Value("${path.getUserByEmail}")
  private String pathGetUserByEmail;

  @Value("${path.getAdminByEmail}")
  private String pathGetAdminByEmail;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET LOGGED ACCOUNT INFO ========");

    // Lấy ra authentication đang login
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    // Lấy ra email của account
    String email = authentication.getName();
    log.info("Email: {}", email);

    if (StringUtils.isBlank(email)) {
      log.error("<Error processing> => Can't get email of user");
      throw new ProcessException("Không lấy ra được email tài khoản đang đăng nhập");
    }

    // Tìm thông tin account theo email
    Account account = accountRepository.findByEmail(email);
    log.info("<Query> => Result finding account by email: {}", account);
    if (account == null) {
      log.error("<Error processing> => Logged account not found");
      throw new ProcessException("Không tìm thấy tài khoản đang đăng nhập");
    }

    AccountOutputDto loggedAccount = getAccountInfoByEmail(email, account);
    log.info("<Mapping> => Logged account info: {}", loggedAccount);
    if (loggedAccount == null) {
      log.error("<Error processing> => Logged account information not found");
      throw new ProcessException("Không tìm thấy thông tin tài khoản đang đăng nhập");
    }

    // Set redis thông tin user đang đăng nhập
    setRedisAccountInfo(email, loggedAccount);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin tài khoản đang đăng nhập thành công");
    response.setData(Map.of("loggedAccount", loggedAccount));

    log.info("<Result API> => Getting logged account info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  /**
   * Set thông tin tài khoản vào redis theo key
   */
  public void setRedisAccountInfo(String email, AccountOutputDto account) {
    RedisUtils.hset(email, RedisEnums.ID.name(), account.getId());
    RedisUtils.hset(email, RedisEnums.ACCOUNT_ID.name(), account.getAccountId());
    RedisUtils.hset(email, RedisEnums.AVATAR.name(), account.getAvatar());
    RedisUtils.hset(email, RedisEnums.NAME.name(), account.getName());
    RedisUtils.hset(email, RedisEnums.GENDER.name(), account.getGender());
    RedisUtils.hset(email, RedisEnums.DOB.name(), account.getDob());
    RedisUtils.hset(email, RedisEnums.PHONE.name(), account.getPhone());
    RedisUtils.hset(email, RedisEnums.EMAIL.name(), account.getEmail());
    RedisUtils.hset(email, RedisEnums.ROLE.name(), account.getRole());
    RedisUtils.hset(email, RedisEnums.ADDRESS.name(), account.getAddress());
    RedisUtils.hset(email, RedisEnums.JOB.name(), account.getJob());
    RedisUtils.hset(email, RedisEnums.WORK_PLACE.name(), account.getWorkplace());
    RedisUtils.hset(email, RedisEnums.CREATED_BY.name(), account.getCreatedBy());
    RedisUtils.hset(email, RedisEnums.CREATED_DATE.name(), account.getCreatedDate());

    if (account.getRole().equals(RoleEnums.User.name())) {
      RedisUtils.hset(email, RedisEnums.IS_TOP_STUDENT.name(), account.getIsTopStudent());
      RedisUtils.hset(email, RedisEnums.FEEDBACK.name(), account.getFeedback());
    }
  }

  /**
   * Lấy ra thông tin tài khoản theo email
   */
  private AccountOutputDto getAccountInfoByEmail(String email, Account account) {
    Map<String, Object> parameters = Map.of("email", aesUtils.encrypt(email));

    String url;
    if (RoleEnums.User.name().equals(account.getRole())) {
      url = eurekaClient
          .getNextServerFromEureka("user", false).getHomePageUrl()
          + "internal/user" + pathGetUserByEmail;
    } else {
      url = eurekaClient
          .getNextServerFromEureka("ADMIN", false).getHomePageUrl()
          + "internal/admin" + pathGetAdminByEmail;
    }

    JsonObject resp = UnirestUtils.getWithoutHeader(url, parameters);

    log.info("Url: {}", url);
    log.info("Email: {}", email);
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
      AccountOutputDto accountOutputDto;
      if (RoleEnums.User.name().equals(account.getRole())) {
        accountOutputDto = gson.fromJson(
            resp.get("data").getAsJsonObject().get("user").getAsJsonObject(),
            AccountOutputDto.class);
        accountOutputDto.setAccountId(account.getId());
        accountOutputDto.setRole(RoleEnums.User.name());
      } else {
        accountOutputDto = gson.fromJson(
            resp.get("data").getAsJsonObject().get("admin").getAsJsonObject(),
            AccountOutputDto.class);
        accountOutputDto.setAccountId(account.getId());
      }
      return accountOutputDto;
    }
    return null;
  }
}
