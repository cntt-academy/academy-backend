package com.act.authentication.domain.service.account.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.utils.AuthenticationUtils;
import com.act.authentication.application.request.account.ChangePasswordRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.account.dispatcher.ChangePasswordService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangePasswordService")
public class ChangePasswordServiceImpl implements ChangePasswordService {

  private final AuthenticationUtils authenticationUtils;
  private final AccountRepository accountRepository;
  private final PasswordEncoder passwordEncoder;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private Account account;

  @Override
  public ResponseEntity<?> execute(ChangePasswordRequest request) {
    log.info("======== API CHANGE PASSWORD ========");
    return ChangePasswordService.super.execute(request);
  }

  @Override
  public void validate(ChangePasswordRequest request) {
    loggedAccount = authenticationUtils.getAccountLoginInfo();
    account = accountRepository.findByEmail(loggedAccount.get("email"));
    log.info("<Query> => Result finding account by email: {}", account);

    if (!passwordEncoder.matches(request.getOldPassword(), account.getPassword())) {
      throw new BadRequestException("Mật khẩu cũ không đúng");
    }

    if (request.getNewPassword().equals(request.getOldPassword())) {
      throw new BadRequestException("Mật khẩu mới trùng với mật khẩu cũ");
    }
  }

  @Override
  public ResponseEntity<?> process(ChangePasswordRequest request) {
    account.setPassword(passwordEncoder.encode(request.getNewPassword()));
    account.setUpdatedBy(loggedAccount.get("name"));
    account.setUpdatedDate(new Date());
    accountRepository.save(account);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đổi mật khẩu thành công");

    log.info("<Result API> => Password updated successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
