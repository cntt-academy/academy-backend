package com.act.authentication.domain.service.internal.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.Infrastructure.utils.ManagerInfo;
import com.act.authentication.application.enums.RoleEnums;
import com.act.authentication.application.request.account.ChangeManagerRequest;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.internal.dispatcher.ChangeManagerService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangeManagerService")
public class ChangeManagerServiceImpl implements ChangeManagerService {

  private final AccountRepository accountRepository;
  private final Gson gson;

  private Account oldManager;
  private Account newManager;

  @Override
  public ResponseEntity<?> execute(ChangeManagerRequest request) {
    log.info("======== API CHANGE MANAGER ========");
    log.info("Email of new manager: {}", request.getEmail());
    return ChangeManagerService.super.execute(request);
  }

  @Override
  public void validate(ChangeManagerRequest request) {
    oldManager = accountRepository.findByRole(RoleEnums.Manager.name());
    log.info("<Query> => Result finding account by role: {}", oldManager);

    newManager =
        accountRepository.findByEmailAndRole(request.getEmail(), RoleEnums.Collaborator.name());
    log.info("<Query> => Result finding account by email and role: {}", newManager);
    if (newManager == null) {
      throw new BadRequestException("Không tìm thấy tài khoản cộng tác viên");
    }
  }

  @Override
  public ResponseEntity<?> process(ChangeManagerRequest request) {
    // Set quyền Manager cho tài khoản Cộng tác viên
    newManager.setRole(RoleEnums.Manager.name());
    newManager.setUpdatedBy(request.getUpdatedBy());
    newManager.setUpdatedDate(new Date());

    // Set quyền Collaborator cho tài khoản Quản lý hiện tại
    oldManager.setRole(RoleEnums.Collaborator.name());
    oldManager.setUpdatedBy(request.getUpdatedBy());
    oldManager.setUpdatedDate(new Date());

    List<Account> accounts = List.of(newManager, oldManager);
    accountRepository.saveAll(accounts);

    ManagerInfo.ADMIN_EMAIL = newManager.getEmail();

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đổi quản lý trung tâm thành công");

    log.info("<Result API> => Changing manager successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
