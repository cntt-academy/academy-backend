package com.act.authentication.domain.service.internal.dispatcher;

import com.act.authentication.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetAccountByIdService extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
