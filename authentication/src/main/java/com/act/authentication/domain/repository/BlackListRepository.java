package com.act.authentication.domain.repository;

import com.act.authentication.domain.entity.BlackList;

public interface BlackListRepository {

  BlackList findByToken(String token);

  void save(BlackList blackList);
}
