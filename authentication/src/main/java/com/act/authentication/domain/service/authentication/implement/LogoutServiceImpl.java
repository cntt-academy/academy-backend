package com.act.authentication.domain.service.authentication.implement;

import com.act.authentication.Infrastructure.exception.BadRequestException;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.entity.BlackList;
import com.act.authentication.domain.repository.BlackListRepository;
import com.act.authentication.domain.service.authentication.dispatcher.LogoutService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("LogoutService")
public class LogoutServiceImpl implements LogoutService {

  private final BlackListRepository blackListRepository;
  private final Gson gson;
  private String token;

  @Override
  public ResponseEntity<?> execute(HttpServletRequest request) {
    log.info("======== API LOGOUT ========");
    return LogoutService.super.execute(request);
  }

  @Override
  public void validate(HttpServletRequest request) {
    token = request.getHeader("Authorization").replace("Bearer", "").trim();
    log.info("Verify token: {}", token);

    if (StringUtils.isBlank(token)) {
      throw new BadRequestException("Token không hợp lệ");
    }

    BlackList blockToken = blackListRepository.findByToken(token);
    log.info("<Query> => Result finding blockToken: {}", blockToken);
    if (blockToken != null) {
      throw new BadRequestException("Token đã hết hiệu lực");
    }
  }

  @Override
  public ResponseEntity<?> process(HttpServletRequest request) {
    BlackList blackList = new BlackList();
    blackList.setToken(token);
    blackList.setBlockDate(new Date());
    blackListRepository.save(blackList);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Logout thành công");

    log.info("<Result API> => Logout successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
