package com.act.authentication.domain.service.internal.implement;

import com.act.authentication.Infrastructure.exception.ProcessException;
import com.act.authentication.Infrastructure.utils.AESUtils;
import com.act.authentication.Infrastructure.utils.GenericMapper;
import com.act.authentication.application.response.JsonResponseBase;
import com.act.authentication.domain.dto.AccountOutputDto;
import com.act.authentication.domain.entity.Account;
import com.act.authentication.domain.repository.AccountRepository;
import com.act.authentication.domain.service.internal.dispatcher.GetAccountByIdService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAccountByIdService")
public class GetAccountByIdServiceImpl implements GetAccountByIdService {

  private final AccountRepository accountRepository;
  private final GenericMapper genericMapper;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(String accountId) {
    log.info("======== API GET ACCOUNT BY ID ========");
    log.info("Account Id: {}", accountId);
    return GetAccountByIdService.super.execute(accountId);
  }

  @Override
  public void validate(String accountId) {

  }

  @Override
  public ResponseEntity<?> process(String accountId) {
    Account account = accountRepository.findById(Integer.valueOf(aesUtils.decrypt(accountId)));
    log.info("<Query> => Result finding account by id: {}", account);
    if (account == null) {
      throw new ProcessException("Không tìm thấy thông tin tài khoản");
    }

    AccountOutputDto accountOutputDto = genericMapper.mapToAccountOutputDto(account);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin tài khoản thành công");
    response.setData(Map.of("account", accountOutputDto));

    log.info("<Result API> => Getting account by id successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
