package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetManagerInfoService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
