package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.request.news.CreateNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.news.dispatcher.CreateNewsService;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateNewsService")
public class CreateNewsServiceImpl implements CreateNewsService {

  private final TypeNewsRepository typeNewsRepository;
  private final AdminRepository adminRepository;
  private final NewsRepository newsRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  @Value("${path.sendEmail.newsCreated}")
  private String pathSendEmailNewsCreated;

  @Override
  public ResponseEntity<?> execute(CreateNewsRequest request) {
    log.info("======== API CREATE NEWS ========");
    log.info("Request create news: {}", gson.toJson(request));
    return CreateNewsService.super.execute(request);
  }

  @Override
  public void validate(CreateNewsRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (RoleEnums.User.name().equals(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền tạo tin tức");
    }

    TypeNews typeNews = typeNewsRepository.findById(request.getTypeId());
    log.info("<Query> => Result finding type news by id: {}", typeNews);
    if (typeNews == null) {
      throw new BadRequestException("Không tìm thấy loại tin tức");
    }

    News news = newsRepository.findByName(request.getName());
    log.info("<Query> => Result finding news by name: {}", news);
    if (news != null) {
      throw new BadRequestException("Đã tồn tại tin tức có tên giống với yêu cầu");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateNewsRequest request) {
    Admin admin = adminRepository.findByEmail(loggedAccount.get("email"));

    News news = genericMapper.mapToType(request, News.class);
    news.setPosterId(admin.getId());
    news.setCreatedBy(admin.getName());
    news.setCreatedDate(new Date());
    news.setUpdatedBy(admin.getName());
    news.setUpdatedDate(new Date());
    if (adminUtils.isOwnerOrManager(admin.getRole())) {
      news.setState(NewsStateEnums.ACTIVE.name());
    } else {
      news.setState(NewsStateEnums.WAIT.name());
    }
    newsRepository.save(news);

    CompletableFuture.runAsync(
        () -> {
          if (!adminUtils.isOwnerOrManager(admin.getRole())) {
            String urlSendEmailNewsCreated = eurekaClient
                .getNextServerFromEureka("message", false).getHomePageUrl()
                + "email" + pathSendEmailNewsCreated;

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Admin collaborator = adminRepository.findById(news.getPosterId());

            Map<String, Object> bodySendEmail = new HashMap<>();
            bodySendEmail.put("time", sdf.format(new Date()));
            bodySendEmail.put("collaborator", collaborator.getName());
            bodySendEmail.put("name", news.getName());

            UnirestUtils
                .postByBodyWithoutHeader(urlSendEmailNewsCreated, gson.toJson(bodySendEmail));
          }
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm tin tức mới thành công");

    log.info("<Result API> => Creating news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
