package com.act.admin.domain.service.typeNews.dispatcher;

import com.act.admin.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetAllTypeNewsService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
