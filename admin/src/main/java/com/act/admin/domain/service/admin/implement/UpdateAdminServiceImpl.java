package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.request.admin.UpdateAdminRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetAllAdminService;
import com.act.admin.domain.service.admin.dispatcher.UpdateAdminService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateAdminService")
public class UpdateAdminServiceImpl implements UpdateAdminService {

  private final GetAllAdminService getAllAdminService;
  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  @Value("${path.updateAccount}")
  private String pathUpdateAccount;
  private Admin admin;

  @Override
  public ResponseEntity<?> execute(UpdateAdminRequest request) {
    log.info("======== API UPDATE ADMIN ========");
    log.info("Request update admin: {}", gson.toJson(request));
    return UpdateAdminService.super.execute(request);
  }

  @Override
  public void validate(UpdateAdminRequest request) {
    admin = adminRepository.findById(request.getId());
    log.info("<Query> => Result finding admin by id: {}", admin);
    if (admin == null) {
      throw new BadRequestException("Không tìm thấy thông tin quản trị viên muốn cập nhật");
    }

    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!loggedAccount.get("email").equals(admin.getEmail()) &&
        Boolean.TRUE.equals(!adminUtils.isOwnerOrManager(loggedAccount.get("role")))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa quản trị viên");
    }

    if (StringUtils.isNotBlank(request.getPhone())) {
      Pattern pattern = Pattern.compile("(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\\b");
      Matcher matcher = pattern.matcher(request.getPhone());
      if (!matcher.matches()) {
        throw new BadRequestException("Số điện thoại không hợp lệ");
      }
    }

    if (StringUtils.isNotBlank(request.getGender())) {
      admin.setGender(adminUtils.validateAndStandardizedGender(request.getGender()));
    }

    try {
      if (StringUtils.isNotBlank(request.getDob())) {
        admin.setDob(AdminUtils.sdf.parse(request.getDob()));
      }
    } catch (ParseException e) {
      throw new BadRequestException("Ngày sinh không hợp lệ");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateAdminRequest request) {
    String urlUpdateName = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathUpdateAccount;

    Map<String, String> body =
        Map.of(
            "email", admin.getEmail(),
            "name", request.getName(),
            "avatar", request.getAvatar(),
            "updatedBy", loggedAccount.get("name"));

    JsonObject resp = UnirestUtils.putWithoutHeader(urlUpdateName, gson.toJson(body));
    log.info("Url: {}", urlUpdateName);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    Admin dataUpdate = genericMapper.mapToType(request, Admin.class);
    dataUpdate.setDob(null);
    dataUpdate.setGender(null);

    genericMapper.copyNonNullProperties(dataUpdate, admin);
    admin.setUpdatedBy(loggedAccount.get("name"));
    admin.setUpdatedDate(new Date());
    adminRepository.save(admin);

    AdminOutputDto adminOutputDto = genericMapper.mapToAdminOutputDto(admin);
    if (admin.getRole().equals(RoleEnums.Owner.name())) {
      adminUtils.setRedisAdminInfo("OWNER", adminOutputDto);
    } else if (admin.getRole().equals(RoleEnums.Manager.name())) {
      adminUtils.setRedisAdminInfo("MANAGER", adminOutputDto);
    } else if (loggedAccount.get("email").equalsIgnoreCase(admin.getEmail())) {
      adminUtils.setRedisAdminInfo(admin.getEmail(), adminOutputDto);
    }

    CompletableFuture.runAsync(getAllAdminService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật thông tin quản trị viên thành công");

    log.info("<Result API> => Updating admin successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
