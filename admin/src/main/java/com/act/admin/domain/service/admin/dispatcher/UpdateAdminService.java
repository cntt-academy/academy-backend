package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.application.request.admin.UpdateAdminRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateAdminService
    extends BaseServiceRequestBody<UpdateAdminRequest, ResponseEntity<?>> {

}
