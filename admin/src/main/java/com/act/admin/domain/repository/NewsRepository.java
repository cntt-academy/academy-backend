package com.act.admin.domain.repository;

import com.act.admin.domain.entity.News;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface NewsRepository {

  Page<News> findAllByConditions(
      String state, String search, Integer typeId, Integer typeIdNot, PageRequest pageRequest);

  News findById(Integer newsId);

  News findByName(String name);

  void save(News news);
}
