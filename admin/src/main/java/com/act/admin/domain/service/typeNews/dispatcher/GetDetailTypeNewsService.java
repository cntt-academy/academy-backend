package com.act.admin.domain.service.typeNews.dispatcher;

import com.act.admin.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetDetailTypeNewsService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
