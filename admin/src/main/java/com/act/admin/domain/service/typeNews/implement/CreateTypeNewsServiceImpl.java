package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.enums.StateEnums;
import com.act.admin.application.request.typeNews.CreateTypeNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.CreateTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.GetAllTypeNewsService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTypeNewsService")
public class CreateTypeNewsServiceImpl implements CreateTypeNewsService {

  private final GetAllTypeNewsService getAllTypeNewsService;
  private final TypeNewsRepository typeNewsRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(CreateTypeNewsRequest request) {
    log.info("======== API CREATE TYPE NEWS ========");
    log.info("Request create type news: {}", gson.toJson(request));
    return CreateTypeNewsService.super.execute(request);
  }

  @Override
  public void validate(CreateTypeNewsRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền thêm loại tin tức");
    }

    TypeNews typeNews = typeNewsRepository.findByName(request.getName());
    log.info("<Query> => Result finding type news by name: {}", typeNews);
    if (typeNews != null) {
      throw new BadRequestException("Loại tin tức đã tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateTypeNewsRequest request) {
    TypeNews typeNews = genericMapper.mapToType(request, TypeNews.class);
    typeNews.setState(StateEnums.ACTIVE.name());
    typeNews.setCreatedBy(loggedAccount.get("name"));
    typeNews.setCreatedDate(new Date());
    typeNews.setUpdatedBy(loggedAccount.get("name"));
    typeNews.setUpdatedDate(new Date());
    typeNewsRepository.save(typeNews);

    // Set lại redis danh sách loại tin tức
    CompletableFuture.runAsync(getAllTypeNewsService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm loại tin tức thành công");

    log.info("<Result API> => Creating type news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
