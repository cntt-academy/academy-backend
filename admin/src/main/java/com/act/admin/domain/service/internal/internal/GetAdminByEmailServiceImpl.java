package com.act.admin.domain.service.internal.internal;

import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AESUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.internal.dispatcher.GetAdminByEmailService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAdminByEmailService")
public class GetAdminByEmailServiceImpl implements GetAdminByEmailService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(String email) {
    log.info("======== API GET ADMIN BY EMAIL ========");
    log.info("Email: {}", email);
    return GetAdminByEmailService.super.execute(email);
  }

  @Override
  public void validate(String email) {
  }

  @Override
  public ResponseEntity<?> process(String email) {
    Admin admin = adminRepository.findByEmail(aesUtils.decrypt(email));
    log.info("<Query> => Result finding admin by email: {}", admin);
    if (admin == null) {
      throw new ProcessException("Không tìm thấy thông tin quản trị viên theo email");
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin quản trị viên theo email thành công");
    response.setData(Map.of("admin", genericMapper.mapToAdminOutputDto(admin)));

    log.info("<Result API> => Getting admin by email successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
