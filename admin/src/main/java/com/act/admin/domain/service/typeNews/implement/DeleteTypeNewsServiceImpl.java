package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.application.enums.StateEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.DeleteTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.GetAllTypeNewsService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteTypeNewsService")
public class DeleteTypeNewsServiceImpl implements DeleteTypeNewsService {

  private final GetAllTypeNewsService getAllTypeNewsService;
  private final TypeNewsRepository typeNewsRepository;
  private final AdminUtils adminUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private TypeNews typeNews;

  @Override
  public ResponseEntity<?> execute(Integer typeNewsId) {
    log.info("======== API DELETE TYPE NEWS ========");
    log.info("Type news Id : {}", typeNewsId);
    return DeleteTypeNewsService.super.execute(typeNewsId);
  }

  @Override
  public void validate(Integer typeNewsId) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa loại tin tức");
    }

    if (typeNews.getId() == 1) {
      throw new BadRequestException("Loại tin tức không được phép xóa");
    }

    typeNews = typeNewsRepository.findById(typeNewsId);
    log.info("<Query> => Find type news by id result: {}", typeNews);
    if (typeNews == null) {
      throw new BadRequestException("Không tìm thấy loại tin tức muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer typeNewsId) {
    typeNews.setState(StateEnums.DELETED.name());
    typeNews.setUpdatedBy(loggedAccount.get("name"));
    typeNews.setUpdatedDate(new Date());
    typeNewsRepository.save(typeNews);

    // Set lại redis danh sách loại tin tức
    CompletableFuture.runAsync(getAllTypeNewsService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa loại tin tức thành công");

    log.info("<Result API> => Deleting type news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
