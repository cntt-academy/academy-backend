package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AESUtils;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.CheckExistsService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CheckExistsService")
public class CheckExistsServiceImpl implements CheckExistsService {

  private final AdminRepository adminRepository;
  private final AESUtils aesUtils;

  @Override
  public ResponseEntity<?> execute(String email) {
    log.info("======== API CHECK ADMIN EXISTS ========");
    log.info("Email admin: {}", email);
    return CheckExistsService.super.execute(email);
  }

  @Override
  public void validate(String email) {
  }

  @Override
  public ResponseEntity<?> process(String email) {
    Admin admin = adminRepository.findByEmail(aesUtils.decrypt(email));
    log.info("<Query> => Result finding admin by email: {}", admin);
    if (admin == null) {
      throw new ProcessException("Email không thuộc tài khoản Quản trị viên");
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Email thuộc tài khoản Quản trị viên");

    log.info("<Result API> => Check admin exists successfully");

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
