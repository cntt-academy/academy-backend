package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.TypeNewsOutputDto;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.GetDetailTypeNewsService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailTypeNewsService")
public class GetDetailTypeNewsServiceImpl implements GetDetailTypeNewsService {

  private final TypeNewsRepository typeNewsRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private TypeNews typeNews;

  @Override
  public ResponseEntity<?> execute(Integer typeNewsId) {
    log.info("======== API GET DETAIL TYPE NEWS ========");
    log.info("Type news Id: {}", typeNewsId);
    return GetDetailTypeNewsService.super.execute(typeNewsId);
  }

  @Override
  public void validate(Integer typeNewsId) {
    typeNews = typeNewsRepository.findById(typeNewsId);
    log.info("<Query> => Result finding type news by id: {}", typeNews);
    if (typeNews == null) {
      throw new BadRequestException("Không tìm thấy loại tin tức");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer typeNewsId) {
    TypeNewsOutputDto typeNewsOutputDto = genericMapper.mapToTypeNewsOutputDto(typeNews);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra loại tin tức thành công");
    response.setData(Map.of("typeNews", typeNewsOutputDto));
    log.info("<Result API> => Getting type news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
