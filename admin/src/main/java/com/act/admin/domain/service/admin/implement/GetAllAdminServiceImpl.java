package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetAllAdminService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllAdminService")
public class GetAllAdminServiceImpl implements GetAllAdminService {

  private final AdminRepository adminRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL ADMIN ========");

    List<Admin> admins = adminRepository.findAll();
    log.info("<Query> => Get all admin result: {}", gson.toJson(admins));

    // Lưu vào Redis
    String dataAdmin;
    Admin admin;
    RedisUtils.set("TOTAL_ADMIN", String.valueOf(admins.size()));
    for (int i = 1; i <= admins.size(); i++) {
      admin = admins.get(i - 1);
      dataAdmin =
          admin.getId() + "," + admin.getName() + "," + admin.getEmail() + "," + admin.getRole();
      RedisUtils.set("ADMIN_" + i, dataAdmin);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách tất cả quản trị viên và lưu vào Redis thành công");

    log.info("<Result API> => Getting all admin successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
