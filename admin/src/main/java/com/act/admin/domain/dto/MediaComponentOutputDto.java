package com.act.admin.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.admin.domain.entity.MediaComponent} entity
 */
@Data
public class MediaComponentOutputDto implements Serializable {

  private Integer id;
  private String name;
  private String url;
}