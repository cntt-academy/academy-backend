package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface CheckExistsService
    extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
