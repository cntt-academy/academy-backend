package com.act.admin.domain.service.mediaComponent.dispatcher;

import com.act.admin.application.request.mediaComponent.UpdateVideoBannerRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import com.act.admin.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface UpdateVideoBannerService extends
    BaseServiceRequestBody<UpdateVideoBannerRequest, ResponseEntity<?>> {

}
