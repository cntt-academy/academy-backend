package com.act.admin.domain.service.news.dispatcher;

import com.act.admin.application.request.news.CreateNewsRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateNewsService extends
    BaseServiceRequestBody<CreateNewsRequest, ResponseEntity<?>> {

}
