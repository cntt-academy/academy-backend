package com.act.admin.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "media_components")
public class MediaComponent {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Lob
  @Column(name = "url", nullable = false)
  private String url;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "MediaComponent{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", url='" + url + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}