package com.act.admin.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.admin.domain.entity.TypeNews} entity
 */
@Data
public class TypeNewsOutputDto implements Serializable {

  private Integer id;
  private String name;
  private String desc;
  private String createdBy;
  private String createdDate;
}