package com.act.admin.domain.service.mediaComponent.implement;

import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.MediaComponentOutputDto;
import com.act.admin.domain.entity.MediaComponent;
import com.act.admin.domain.repository.MediaComponentRepository;
import com.act.admin.domain.service.mediaComponent.dispatcher.GetMediaComponentService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetMediaComponentService")
public class GetMediaComponentServiceImpl implements GetMediaComponentService {

  private final MediaComponentRepository mediaComponentRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET MEDIA COMPONENT ========");

    List<MediaComponent> components = mediaComponentRepository.findAll();
    log.info("<Query> => Result finding all media components: {}", gson.toJson(components));

    List<MediaComponentOutputDto> mediaComponentOutputDtos =
        genericMapper.mapToListOfType(components, MediaComponentOutputDto.class);

    mediaComponentOutputDtos.forEach(i -> {
      log.info("{}: {}", i.getName(), i.getUrl());
      RedisUtils.hset("MEDIA_COMPONENT", i.getName().toUpperCase(), i.getUrl());
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách media component và lưu vào redis thành công");
    response.setData(Map.of("components", mediaComponentOutputDtos));

    log.info("<Result API> => Getting media component successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
