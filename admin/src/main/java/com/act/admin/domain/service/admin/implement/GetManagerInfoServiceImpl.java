package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetManagerInfoService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetManagerInfoService")
public class GetManagerInfoServiceImpl implements GetManagerInfoService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET MANAGER INFO ========");

    Admin manager = adminRepository.findByRole(RoleEnums.Manager.name());
    log.info("<Query> => Result finding manager: {}", manager);
    if (manager == null) {
      throw new ProcessException("Không tìm thấy thông tin quản lý");
    }

    AdminOutputDto managerOutputDto = genericMapper.mapToAdminOutputDto(manager);
    // Set thông tin quản lý vào Redis
    adminUtils.setRedisAdminInfo("MANAGER", managerOutputDto);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin quản lý và lưu vào redis thành công");

    log.info("<Result API> => Getting manager info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
