package com.act.admin.domain.service.mediaComponent.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.application.request.mediaComponent.UpdateVideoBannerRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.MediaComponentOutputDto;
import com.act.admin.domain.entity.MediaComponent;
import com.act.admin.domain.repository.MediaComponentRepository;
import com.act.admin.domain.service.mediaComponent.dispatcher.UpdateVideoBannerService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateVideoBannerService")
public class UpdateVideoBannerServiceImpl implements UpdateVideoBannerService {

  private final MediaComponentRepository mediaComponentRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(UpdateVideoBannerRequest request) {
    log.info("======== API UPDATE VIDEO BANNER ========");
    log.info("Request: {}", request);
    return UpdateVideoBannerService.super.execute(request);
  }

  @Override
  public void validate(UpdateVideoBannerRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền cập nhật video banner website trung tâm");
    }

    if (StringUtils.isBlank(request.getUrlVideoBanner())) {
      throw new BadRequestException("Đường dẫn video banner không thay đổi");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateVideoBannerRequest request) {
    MediaComponent videoBanner = mediaComponentRepository.findVideoBanner();
    log.info("<Query> => Result finding video banner: {}", gson.toJson(videoBanner));

    videoBanner.setUrl(request.getUrlVideoBanner());
    videoBanner.setUpdatedBy(loggedAccount.get("name"));
    mediaComponentRepository.save(videoBanner);

    RedisUtils.hset("MEDIA_COMPONENT", videoBanner.getName().toUpperCase(), videoBanner.getUrl());

    MediaComponentOutputDto videoBannerOutputDtos =
        genericMapper.mapToType(videoBanner, MediaComponentOutputDto.class);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Update video banner và lưu vào redis thành công");
    response.setData(Map.of("videoBannerOutputDtos", videoBannerOutputDtos));

    log.info("<Result API> => Updating video banner successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
