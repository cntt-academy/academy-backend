package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.request.admin.GetListCollaboratorRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetListCollaboratorService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListCollaboratorService")
public class GetListCollaboratorServiceImpl implements GetListCollaboratorService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListCollaboratorRequest request) {
    log.info("======== API GET LIST COLLABORATOR ========");
    log.info("Request get list collaborator: {}", gson.toJson(request));
    return GetListCollaboratorService.super.execute(request);
  }

  @Override
  public void validate(GetListCollaboratorRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListCollaboratorRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<Admin> admins = adminRepository.findAllByCondition(
        adminUtils.standardizedParameterQueryLike(request.getSearch()),
        RoleEnums.Collaborator.name(),
        pageRequest);
    log.info("<Query> => Result getting list collaborator by conditions: {}", gson.toJson(admins));

    List<AdminOutputDto> collaborators = genericMapper.mapToListAdminOutputDto(admins.getContent());

    Map<String, Object> data =
        Map.of("collaborators", collaborators,
            "totalElements", admins.getTotalElements(),
            "totalPages", admins.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách cộng tác viên thành công");
    response.setData(data);

    log.info("<Result API> =>  Getting list collaborator successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
