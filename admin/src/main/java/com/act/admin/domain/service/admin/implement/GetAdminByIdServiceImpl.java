package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AESUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetAdminByIdService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAdminByIdService")
public class GetAdminByIdServiceImpl implements GetAdminByIdService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(String adminId) {
    log.info("======== API GET ADMIN BY ID ========");
    log.info("Admin Id: {}", adminId);
    return GetAdminByIdService.super.execute(adminId);
  }

  @Override
  public void validate(String adminId) {
  }

  @Override
  public ResponseEntity<?> process(String adminId) {
    Admin admin = adminRepository.findById(Integer.valueOf(aesUtils.decrypt(adminId)));
    log.info("<Query> => Result finding admin by id: {}", admin);
    if (admin == null) {
      throw new ProcessException("Không tìm thấy thông tin quản trị viên theo id");
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin quản trị viên theo id thành công");
    response.setData(Map.of("admin", genericMapper.mapToAdminOutputDto(admin)));

    log.info("<Result API> => Getting admin by id successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
