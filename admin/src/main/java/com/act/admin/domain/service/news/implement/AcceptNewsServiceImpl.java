package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.service.news.dispatcher.AcceptNewsService;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("AcceptNewsService")
public class AcceptNewsServiceImpl implements AcceptNewsService {

  private final AdminRepository adminRepository;
  private final NewsRepository newsRepository;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  @Value("${path.sendEmail.newsAccepted}")
  private String pathSendEmailNewsAccepted;
  private News news;

  @Override
  public ResponseEntity<?> execute(Integer newsId) {
    log.info("======== API ACCEPT NEWS ========");
    log.info("News Id: {}", newsId);
    return AcceptNewsService.super.execute(newsId);
  }

  @Override
  public void validate(Integer newsId) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền duyệt tin tức");
    }

    news = newsRepository.findById(newsId);
    log.info("<Query> => Result finding news by id: {}", news);
    if (news == null || !NewsStateEnums.WAIT.name().equals(news.getState())) {
      throw new BadRequestException("Không tìm thấy tin tức muốn duyệt");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer newsId) {
    news.setState(NewsStateEnums.ACTIVE.name());
    news.setUpdatedBy(loggedAccount.get("name"));
    news.setUpdatedDate(new Date());
    newsRepository.save(news);

    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailNewsAccepted = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailNewsAccepted;

          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
          Admin collaborator = adminRepository.findById(news.getPosterId());

          Map<String, Object> bodySendEmail = new HashMap<>();
          bodySendEmail.put("time", sdf.format(new Date()));
          bodySendEmail.put("collaborator", collaborator.getEmail());
          bodySendEmail.put("name", news.getName());

          UnirestUtils
              .postByBodyWithoutHeader(urlSendEmailNewsAccepted, gson.toJson(bodySendEmail));
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Duyệt tin tức thành công");

    log.info("<Result API> => Accepting news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
