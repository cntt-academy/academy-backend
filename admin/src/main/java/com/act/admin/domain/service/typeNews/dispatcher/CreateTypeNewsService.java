package com.act.admin.domain.service.typeNews.dispatcher;

import com.act.admin.application.request.typeNews.CreateTypeNewsRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateTypeNewsService
    extends BaseServiceRequestBody<CreateTypeNewsRequest, ResponseEntity<?>> {

}
