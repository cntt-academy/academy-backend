package com.act.admin.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.admin.domain.entity.Admin} entity
 */
@Data
public class AdminOutputDto implements Serializable {

  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String dob;
  private String phone;
  private String email;
  private String role;
  private String address;
  private String job;
  private String workplace;
  private String createdBy;
  private String createdDate;
}