package com.act.admin.domain.service.typeNews.dispatcher;

import com.act.admin.application.request.typeNews.UpdateTypeNewsRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateTypeNewsService
    extends BaseServiceRequestBody<UpdateTypeNewsRequest, ResponseEntity<?>> {

}
