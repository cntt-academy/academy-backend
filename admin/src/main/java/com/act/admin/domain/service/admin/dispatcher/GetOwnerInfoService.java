package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetOwnerInfoService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
