package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.NewsOutputDto;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.service.news.dispatcher.GetDetailNewsService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailNewsService")
public class GetDetailNewsServiceImpl implements GetDetailNewsService {

  private final NewsRepository newsRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private News news;

  @Override
  public ResponseEntity<?> execute(Integer newsId) {
    log.info("======== API GET DETAIL NEWS ========");
    log.info("News Id: {}", newsId);
    return GetDetailNewsService.super.execute(newsId);
  }

  @Override
  public void validate(Integer newsId) {
    news = newsRepository.findById(newsId);
    log.info("<Query> => Find news by id result: {}", news);
    if (news == null) {
      throw new BadRequestException("Tin tức không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer newsId) {
    NewsOutputDto newsOutputDto = genericMapper.mapToNewsOutputDto(news);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin tin tức thành công");
    response.setData(Map.of("news", newsOutputDto));

    log.info("<Result API> => Getting news info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
