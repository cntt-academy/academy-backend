package com.act.admin.domain.repository;

import com.act.admin.domain.entity.MediaComponent;
import java.util.List;

public interface MediaComponentRepository {

  List<MediaComponent> findAllSlider();

  MediaComponent findVideoBanner();

  List<MediaComponent> findAll();

  void save(MediaComponent mediaComponent);

  void saveAll(List<MediaComponent> mediaComponents);
}
