package com.act.admin.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "admins")
public class Admin {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "avatar", nullable = false)
  private String avatar;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "gender", nullable = false, length = 10)
  private String gender;

  @Column(name = "dob", nullable = false)
  private Date dob;

  @Column(name = "phone", nullable = false, length = 10)
  private String phone;

  @Column(name = "email", nullable = false, length = 50)
  private String email;

  @Column(name = "role", nullable = false, length = 20)
  private String role;

  @Lob
  @Column(name = "address")
  private String address;

  @Lob
  @Column(name = "job")
  private String job;

  @Lob
  @Column(name = "workplace")
  private String workplace;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "Admin{" +
        "id=" + id +
        ", avatar='" + avatar + '\'' +
        ", name='" + name + '\'' +
        ", gender='" + gender + '\'' +
        ", dob=" + dob +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        ", role='" + role + '\'' +
        ", address='" + address + '\'' +
        ", job='" + job + '\'' +
        ", workplace='" + workplace + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}