package com.act.admin.domain.service.base;

public interface BaseServiceNoRequest<O> {

  O execute();
}
