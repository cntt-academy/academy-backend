package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.TypeNewsOutputDto;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.GetAllTypeNewsService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllTypeNewsService")
public class GetAllTypeNewsServiceImpl implements GetAllTypeNewsService {

  private final TypeNewsRepository typeNewsRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL TYPE NEWS ========");
    List<TypeNews> listTypeNews = typeNewsRepository.findAll();
    log.info("<Query> => Find all type news result: {}", gson.toJson(listTypeNews));

    // Lưu vào Redis
    String dataTypeNews;
    TypeNewsOutputDto typeNews;
    List<TypeNewsOutputDto> typeNewsOutputDtos =
        genericMapper.mapToListOfType(listTypeNews, TypeNewsOutputDto.class);
    RedisUtils.set("TOTAL_TYPE_NEWS", String.valueOf(listTypeNews.size()));
    for (int i = 1; i <= typeNewsOutputDtos.size(); i++) {
      typeNews = typeNewsOutputDtos.get(i - 1);
      dataTypeNews = typeNews.getId() + "," + typeNews.getName();
      RedisUtils.set("TYPE_NEWS_" + i, dataTypeNews);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách loại tin tức và lưu vào redis thành công");
    response.setData(Map.of("listTypeNews", typeNewsOutputDtos));

    log.info("<Result API> => Getting list type news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
