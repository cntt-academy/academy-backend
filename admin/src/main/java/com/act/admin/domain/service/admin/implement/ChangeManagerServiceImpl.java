package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.RedisEnums;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.request.admin.ChangeManagerRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.ChangeManagerService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangeManagerService")
public class ChangeManagerServiceImpl implements ChangeManagerService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Value("${path.sendEmail.changeManager}")
  private String pathSendEmailChangeManager;
  @Value("${path.changeManager}")
  private String pathChangeManager;

  private Map<String, String> loggedAccount;
  private Admin collaborator;
  private Admin manager;

  @Override
  public ResponseEntity<?> execute(ChangeManagerRequest request) {
    log.info("======== API CHANGE MANAGER ========");
    log.info("Email of new manager: {}", request.getEmail());
    return ChangeManagerService.super.execute(request);
  }

  @Override
  public void validate(ChangeManagerRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền đổi quản lý trung tâm");
    }

    manager = adminRepository.findByRole(RoleEnums.Manager.name());
    log.info("<Query> => Result finding manager: {}", manager);
    if (manager == null) {
      throw new BadRequestException("Không tìm thấy thông tin quản lý");
    }

    if (request.getEmail().equals(manager.getEmail())) {
      throw new BadRequestException("Email quản lý mới giống với email quản lý cũ");
    }

    collaborator =
        adminRepository.findByEmailAndRole(request.getEmail(), RoleEnums.Collaborator.name());
    log.info("<Query> => Result finding admin by name and role: {}", collaborator);
    if (collaborator == null) {
      throw new BadRequestException("Không tìm thấy tài khoản cộng tác viên");
    }
  }

  @Override
  public ResponseEntity<?> process(ChangeManagerRequest request) {
    String urlChangeManager = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/authentication" + pathChangeManager;

    Map<String, String> body =
        Map.of("email", request.getEmail(), "updatedBy", manager.getName());

    JsonObject resp = UnirestUtils.putWithoutHeader(urlChangeManager, gson.toJson(body));
    log.info("Url: {}", urlChangeManager);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    collaborator.setRole(RoleEnums.Manager.name());
    collaborator.setUpdatedBy(loggedAccount.get("name"));
    collaborator.setUpdatedDate(new Date());

    manager.setRole(RoleEnums.Collaborator.name());
    manager.setUpdatedBy(loggedAccount.get("name"));
    manager.setUpdatedDate(new Date());

    adminRepository.saveAll(List.of(collaborator, manager));

    AdminOutputDto adminOutputDto = genericMapper.mapToAdminOutputDto(collaborator);
    adminUtils.setRedisAdminInfo("MANAGER", adminOutputDto);
    RedisUtils.hset(manager.getEmail(), RedisEnums.ROLE.name(), RoleEnums.Collaborator.name());

    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailChangeManager = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailChangeManager;

          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
          List<String> collaborators = adminRepository.findAllEmailCollaborator();
          collaborators.remove(collaborator.getEmail());
          collaborators.remove(manager.getEmail());

          Map<String, Object> bodySendEmail = new HashMap<>();
          bodySendEmail.put("time", sdf.format(new Date()));
          bodySendEmail.put("oldManager", manager.getEmail());
          bodySendEmail.put("newManager", collaborator.getEmail());
          bodySendEmail.put("newManagerName", collaborator.getName());
          bodySendEmail.put("collaborators", collaborators);

          UnirestUtils
              .postByBodyWithoutHeader(urlSendEmailChangeManager, gson.toJson(bodySendEmail));
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đổi quản lý trung tâm thành công");

    log.info("<Result API> => Changing manager successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
