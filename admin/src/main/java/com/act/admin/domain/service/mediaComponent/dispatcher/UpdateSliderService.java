package com.act.admin.domain.service.mediaComponent.dispatcher;

import com.act.admin.application.request.mediaComponent.UpdateSliderRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateSliderService extends
    BaseServiceRequestBody<UpdateSliderRequest, ResponseEntity<?>> {

}
