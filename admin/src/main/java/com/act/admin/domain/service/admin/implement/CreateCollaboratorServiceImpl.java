package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.enums.StateEnums;
import com.act.admin.application.request.admin.CreateCollaboratorRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.CreateCollaboratorService;
import com.act.admin.domain.service.admin.dispatcher.GetAllAdminService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateCollaboratorService")
public class CreateCollaboratorServiceImpl implements CreateCollaboratorService {

  private final GetAllAdminService getAllAdminService;
  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Value("${path.createAccount}")
  private String pathCreateAccount;
  private Map<String, String> loggedAccount;
  private String gender;
  private Date dob;

  @Override
  public ResponseEntity<?> execute(CreateCollaboratorRequest request) {
    log.info("======== API CREATE COLLABORATOR ========");
    log.info("Request create collaborator: {}", gson.toJson(request));
    return CreateCollaboratorService.super.execute(request);
  }

  @Override
  public void validate(CreateCollaboratorRequest request) {
    Pattern pattern;
    Matcher matcher;

    pattern = Pattern.compile("^[^\\s@]+@[^\\s@]+$");
    matcher = pattern.matcher(request.getEmail());
    if (!matcher.matches()) {
      throw new BadRequestException("Email không hợp lệ");
    }

    pattern = Pattern.compile("(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\\b");
    matcher = pattern.matcher(request.getPhone());
    if (!matcher.matches()) {
      throw new BadRequestException("Số điện thoại không hợp lệ");
    }

    gender = adminUtils.validateAndStandardizedGender(request.getGender());

    try {
      dob = AdminUtils.sdf.parse(request.getDob());
    } catch (Exception e) {
      throw new BadRequestException("Ngày sinh không hợp lệ");
    }

    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền thêm cộng tác viên");
    }

    Admin admin = adminRepository.findByEmail(request.getEmail());
    log.info("<Query> => Result finding admin by email: {}", admin);
    if (admin != null) {
      throw new BadRequestException("Email đã được đăng ký tài khoản");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateCollaboratorRequest request) {
    String urlCreateAccount = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathCreateAccount;
    Map<String, String> body =
        Map.of(
            "avatar", request.getAvatar(),
            "name", request.getName(),
            "email", request.getEmail(),
            "role", RoleEnums.Collaborator.name(),
            "emailCreateBy", loggedAccount.get("email"),
            "nameCreateBy", loggedAccount.get("name"));
    JsonObject resp = UnirestUtils.postByBodyWithoutHeader(urlCreateAccount, gson.toJson(body));
    log.info("Url: {}", urlCreateAccount);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    Admin admin = genericMapper.mapToType(request, Admin.class);
    admin.setDob(dob);
    admin.setGender(gender);
    admin.setRole(RoleEnums.Collaborator.name());
    admin.setState(StateEnums.ACTIVE.name());
    admin.setCreatedBy(loggedAccount.get("name"));
    admin.setCreatedDate(new Date());
    admin.setUpdatedBy(loggedAccount.get("name"));
    admin.setUpdatedDate(new Date());
    adminRepository.save(admin);

    // Set lại redis danh sách quản trị viên
    CompletableFuture.runAsync(getAllAdminService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm cộng tác viên mới thành công");

    log.info("<Result API> => Creating collaborator successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }


}
