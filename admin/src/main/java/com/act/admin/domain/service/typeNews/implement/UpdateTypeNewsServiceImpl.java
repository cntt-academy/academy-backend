package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.application.request.typeNews.UpdateTypeNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.GetAllTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.UpdateTypeNewsService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateTypeNewsService")
public class UpdateTypeNewsServiceImpl implements UpdateTypeNewsService {

  private final GetAllTypeNewsService getAllTypeNewsService;
  private final TypeNewsRepository typeNewsRepository;
  private final AdminUtils adminUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private TypeNews typeNews;

  @Override
  public ResponseEntity<?> execute(UpdateTypeNewsRequest request) {
    log.info("======== API UPDATE TYPE NEWS ========");
    log.info("Request update type news: {}", gson.toJson(request));
    return UpdateTypeNewsService.super.execute(request);
  }

  @Override
  public void validate(UpdateTypeNewsRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa loại tin tức");
    }

    typeNews = typeNewsRepository.findById(request.getId());
    log.info("<Query> => Result finding type news by id: {}", typeNews);
    if (typeNews == null) {
      throw new BadRequestException("Không tìm thấy thông tin loại tin tức muốn cập nhật");
    }

    if (StringUtils.isNotBlank(request.getName())) {
      TypeNews typeNews = typeNewsRepository.findByName(request.getName());
      log.info("<Query> => Result finding type news by name: {}", typeNews);
      if (typeNews != null && !Objects.equals(typeNews.getId(), request.getId())) {
        throw new BadRequestException("Tên loại tin tức đã tồn tại");
      }
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateTypeNewsRequest request) {
    if (StringUtils.isNotBlank(request.getName())) {
      typeNews.setName(request.getName());
    }
    if (StringUtils.isNotBlank(request.getDesc())) {
      typeNews.setDesc(request.getDesc());
    }
    typeNews.setUpdatedBy(loggedAccount.get("name"));
    typeNews.setUpdatedDate(new Date());
    typeNewsRepository.save(typeNews);

    // Set lại redis danh sách loại tin tức
    CompletableFuture.runAsync(getAllTypeNewsService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật loại tin tức thành công");

    log.info("<Result API> => Updating type news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
