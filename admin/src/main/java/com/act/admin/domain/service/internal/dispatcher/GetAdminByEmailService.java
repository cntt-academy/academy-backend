package com.act.admin.domain.service.internal.dispatcher;

import com.act.admin.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetAdminByEmailService extends
    BaseServiceRequestParam<String, ResponseEntity<?>> {

}
