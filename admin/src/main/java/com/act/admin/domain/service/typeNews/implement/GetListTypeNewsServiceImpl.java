package com.act.admin.domain.service.typeNews.implement;

import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.request.typeNews.GetListTypeNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.TypeNewsOutputDto;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.typeNews.dispatcher.GetListTypeNewsService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListTypeNewsService")
public class GetListTypeNewsServiceImpl implements GetListTypeNewsService {

  private final TypeNewsRepository typeNewsRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListTypeNewsRequest request) {
    log.info("======== API GET LIST TYPE NEWS ========");
    log.info("Request get list type news: {}", gson.toJson(request));
    return GetListTypeNewsService.super.execute(request);
  }

  @Override
  public void validate(GetListTypeNewsRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListTypeNewsRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<TypeNews> listTypeNews = typeNewsRepository.findAllByCondition(
        adminUtils.standardizedParameterQueryLike(request.getName()),
        pageRequest);
    log.info("<Query> => Result getting list type news by conditions: {}", gson.toJson(listTypeNews));

    List<TypeNewsOutputDto> typeNewsOutputDtos =
        genericMapper.mapToListTypeNewsOutputDto(listTypeNews.getContent());

    Map<String, Object> data =
        Map.of("listTypeNews", typeNewsOutputDtos,
            "totalElements", listTypeNews.getTotalElements(),
            "totalPages", listTypeNews.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách loại tin tức thành công");
    response.setData(data);

    log.info("<Result API> =>  Getting list type news successfully: {}", gson.toJson(response));
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
