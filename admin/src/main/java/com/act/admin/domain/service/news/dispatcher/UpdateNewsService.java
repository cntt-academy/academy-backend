package com.act.admin.domain.service.news.dispatcher;

import com.act.admin.application.request.news.UpdateNewsRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateNewsService extends
    BaseServiceRequestBody<UpdateNewsRequest, ResponseEntity<?>> {

}
