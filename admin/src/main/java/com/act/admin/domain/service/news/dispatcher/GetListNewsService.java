package com.act.admin.domain.service.news.dispatcher;

import com.act.admin.application.request.news.GetListNewsRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListNewsService extends
    BaseServiceRequestBody<GetListNewsRequest, ResponseEntity<?>> {

}
