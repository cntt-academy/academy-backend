package com.act.admin.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.admin.domain.entity.News} entity
 */
@Data
public class NewsOutputDto implements Serializable {

  private Integer id;
  private String img;
  private String name;
  private String desc;
  private String content;
  private String type;
  private String source;
  private Integer posterId;
  private String state;
  private String createdBy;
  private String createdDate;
}