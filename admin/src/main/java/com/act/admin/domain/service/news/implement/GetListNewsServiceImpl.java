package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.application.request.news.GetListNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.NewsOutputDto;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.service.news.dispatcher.GetListNewsService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListNewsService")
public class GetListNewsServiceImpl implements GetListNewsService {

  private final NewsRepository newsRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListNewsRequest request) {
    log.info("======== API GET LIST NEWS ========");
    log.info("Request get list news: {}", gson.toJson(request));
    return GetListNewsService.super.execute(request);
  }

  @Override
  public void validate(GetListNewsRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListNewsRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    String state = null;
    if (StringUtils.isNotBlank(request.getState())) {
      try {
        NewsStateEnums newsStateEnums = NewsStateEnums.valueOf(request.getState().toUpperCase());
        state = newsStateEnums.name();
      } catch (Exception ignored) {
      }
    }

    Page<News> listNews = newsRepository.findAllByConditions(
        state,
        adminUtils.standardizedParameterQueryLike(request.getSearch()),
        adminUtils.standardizedParameterQueryEqual(request.getTypeId()),
        adminUtils.standardizedParameterQueryEqual(request.getTypeIdNot()),
        pageRequest);
    log.info("<Query> => Get list news by condition result: {}", gson.toJson(listNews));

    List<NewsOutputDto> newsOutputDtos =
        genericMapper.mapToListNewsOutputDto(listNews.getContent());

    Map<String, Object> data =
        Map.of("listNews", newsOutputDtos,
            "totalElements", listNews.getTotalElements(),
            "totalPages", listNews.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách tin tức thành công");
    response.setData(data);

    log.info("<Result API> => Getting list news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
