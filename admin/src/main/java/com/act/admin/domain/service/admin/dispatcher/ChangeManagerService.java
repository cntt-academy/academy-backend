package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.application.request.admin.ChangeManagerRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface ChangeManagerService
    extends BaseServiceRequestBody<ChangeManagerRequest, ResponseEntity<?>> {

}
