package com.act.admin.domain.service.mediaComponent.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.Infrastructure.utils.RedisUtils;
import com.act.admin.application.request.mediaComponent.UpdateSliderRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.MediaComponentOutputDto;
import com.act.admin.domain.entity.MediaComponent;
import com.act.admin.domain.repository.MediaComponentRepository;
import com.act.admin.domain.service.mediaComponent.dispatcher.UpdateSliderService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateSliderService")
public class UpdateSliderServiceImpl implements UpdateSliderService {

  private final MediaComponentRepository mediaComponentRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(UpdateSliderRequest request) {
    log.info("======== API UPDATE SLIDER ========");
    log.info("Request: {}", gson.toJson(request));
    return UpdateSliderService.super.execute(request);
  }

  @Override
  public void validate(UpdateSliderRequest request) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền cập nhật slider website trung tâm");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateSliderRequest request) {
    List<MediaComponent> sliders = mediaComponentRepository.findAllSlider();
    log.info("<Query> => Result finding all slider: {}", gson.toJson(sliders));

    Map<String, String> urlMap = Map.of(
        "slider_01", request.getUrlSlider1() == null ? "" : request.getUrlSlider1(),
        "slider_02", request.getUrlSlider2() == null ? "" : request.getUrlSlider2(),
        "slider_03", request.getUrlSlider3() == null ? "" : request.getUrlSlider3(),
        "slider_04", request.getUrlSlider4() == null ? "" : request.getUrlSlider4(),
        "slider_05", request.getUrlSlider5() == null ? "" : request.getUrlSlider5()
    );

    sliders.stream()
        .filter(i -> StringUtils.isNotBlank(urlMap.get(i.getName())))
        .forEach(i -> {
          i.setUrl(urlMap.get(i.getName()));
          i.setUpdatedBy(loggedAccount.get("name"));
        });
    mediaComponentRepository.saveAll(sliders);

    List<MediaComponentOutputDto> sliderOutputDtos =
        genericMapper.mapToListOfType(sliders, MediaComponentOutputDto.class);

    sliderOutputDtos.forEach(i -> {
      log.info("{}: {}", i.getName(), i.getUrl());
      RedisUtils.hset("MEDIA_COMPONENT", i.getName().toUpperCase(), i.getUrl());
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Update slider và lưu vào redis thành công");
    response.setData(Map.of("sliderOutputDtos", sliderOutputDtos));

    log.info("<Result API> => Updating slider successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
