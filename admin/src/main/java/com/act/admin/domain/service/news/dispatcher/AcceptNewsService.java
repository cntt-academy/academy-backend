package com.act.admin.domain.service.news.dispatcher;

import com.act.admin.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface AcceptNewsService extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
