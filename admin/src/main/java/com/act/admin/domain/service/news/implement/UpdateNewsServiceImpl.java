package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.request.news.UpdateNewsRequest;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.repository.TypeNewsRepository;
import com.act.admin.domain.service.news.dispatcher.UpdateNewsService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateNewsService")
public class UpdateNewsServiceImpl implements UpdateNewsService {

  private final TypeNewsRepository typeNewsRepository;
  private final AdminRepository adminRepository;
  private final NewsRepository newsRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private News news;

  @Override
  public ResponseEntity<?> execute(UpdateNewsRequest request) {
    log.info("======== API UPDATE COURSE ========");
    log.info("Request update news: {}", gson.toJson(request));
    return UpdateNewsService.super.execute(request);
  }

  @Override
  public void validate(UpdateNewsRequest request) {
    news = newsRepository.findByName(request.getName());
    log.info("<Query> => Result finding news by name : {}", news);
    if (news != null && !Objects.equals(news.getId(), request.getId())) {
      throw new BadRequestException("Đã tồn tại tin tức có tên giống với yêu cầu");
    }

    news = newsRepository.findById(request.getId());
    log.info("<Query> => Result finding news by id: {}", news);
    if (news == null) {
      throw new BadRequestException("Không tìm thấy tin tức muốn cập nhật");
    }

    loggedAccount = adminUtils.getAccountLoginInfo();
    Admin admin = adminRepository.findByEmail(loggedAccount.get("email"));

    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))
        && !Objects.equals(news.getPosterId(), admin.getId())) {
      throw new BadRequestException("Bạn không có quyền cập nhật tin tức");
    }

    if (request.getTypeId() != null && request.getTypeId() != 0) {
      TypeNews typeNews = typeNewsRepository.findById(request.getTypeId());
      log.info("<Query> => Result finding type news by id: {}", typeNews);
      if (typeNews == null) {
        throw new BadRequestException("Không tìm thấy loại tin tức");
      }
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateNewsRequest request) {
    News dataUpdate = genericMapper.mapToType(request, News.class);
    genericMapper.copyNonNullProperties(dataUpdate, news);
    news.setUpdatedBy(loggedAccount.get("name"));
    news.setUpdatedDate(new Date());
    newsRepository.save(news);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật thông tin tin tức thành công");

    log.info("<Result API> => Updating news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
