package com.act.admin.domain.repository;

import com.act.admin.domain.entity.Admin;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface AdminRepository {

  Page<Admin> findAllByCondition(String search, String role, PageRequest pageRequest);

  Admin findByEmailAndRole(String email, String role);

  Admin findByIdAndRole(Integer adminId, String role);

  Admin findById(Integer adminId);

  Admin findByEmail(String email);

  Admin findByRole(String role);

  List<String> findAllEmailCollaborator();

  List<Admin> findAll();

  void saveAll(List<Admin> admins);

  void save(Admin admin);
}
