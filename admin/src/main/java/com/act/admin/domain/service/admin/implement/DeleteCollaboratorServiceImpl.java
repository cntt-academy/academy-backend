package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AESUtils;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.enums.StateEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.DeleteCollaboratorService;
import com.act.admin.domain.service.admin.dispatcher.GetAllAdminService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteCollaboratorService")
public class DeleteCollaboratorServiceImpl implements DeleteCollaboratorService {

  private final GetAllAdminService getAllAdminService;
  private final AdminRepository adminRepository;
  private final EurekaClient eurekaClient;
  private final AdminUtils adminUtils;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Value("${path.deleteAccount}")
  private String pathDeleteAccount;
  private Map<String, String> loggedAccount;
  private Admin collaborator;

  @Override
  public ResponseEntity<?> execute(String collaboratorId) {
    log.info("======== API DELETE COLLABORATOR ========");
    log.info("Collaborator Id: {}", collaboratorId);
    return DeleteCollaboratorService.super.execute(collaboratorId);
  }

  @Override
  public void validate(String collaboratorId) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa tài khoản cộng tác viên");
    }

    collaborator =
        adminRepository.findByIdAndRole(
            Integer.valueOf(aesUtils.decrypt(collaboratorId)), RoleEnums.Collaborator.name());
    log.info("<Query> => Find collaborator by id result: {}", collaborator);
    if (collaborator == null) {
      throw new BadRequestException("Không tìm thấy cộng tác viên muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(String collaboratorId) {
    String urlDeleteAccount = eurekaClient
        .getNextServerFromEureka("authentication", false).getHomePageUrl()
        + "internal/account" + pathDeleteAccount;

    Map<String, String> body =
        Map.of(
            "email", collaborator.getEmail(),
            "role", RoleEnums.Collaborator.name(),
            "deletedBy", loggedAccount.get("name"));

    JsonObject resp = UnirestUtils.deleteWithoutHeader(urlDeleteAccount, gson.toJson(body));
    log.info("Url: {}", urlDeleteAccount);
    log.info("Body: {}", gson.toJson(body));
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
      throw new ProcessException(resp.get("message").getAsString());
    }

    collaborator.setState(StateEnums.DELETED.name());
    collaborator.setUpdatedBy(loggedAccount.get("name"));
    collaborator.setUpdatedDate(new Date());
    adminRepository.save(collaborator);

    // Set lại redis danh sách quản trị viên
    CompletableFuture.runAsync(getAllAdminService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa cộng tác viên thành công");

    log.info("<Result API> => Deleting collaborator successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
