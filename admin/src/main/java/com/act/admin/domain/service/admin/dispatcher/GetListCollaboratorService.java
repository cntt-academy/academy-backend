package com.act.admin.domain.service.admin.dispatcher;

import com.act.admin.application.request.admin.GetListCollaboratorRequest;
import com.act.admin.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListCollaboratorService
    extends BaseServiceRequestBody<GetListCollaboratorRequest, ResponseEntity<?>> {

}
