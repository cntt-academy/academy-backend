package com.act.admin.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "news")
public class News {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "img", nullable = false)
  private String img;

  @Lob
  @Column(name = "name", nullable = false)
  private String name;

  @Lob
  @Column(name = "`desc`", nullable = false)
  private String desc;

  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "poster_id", nullable = false)
  private Integer posterId;

  @Column(name = "type_id", nullable = false)
  private Integer typeId;

  @Lob
  @Column(name = "source")
  private String source;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "News{" +
        "id=" + id +
        ", img='" + img + '\'' +
        ", name='" + name + '\'' +
        ", desc='" + desc + '\'' +
        ", content='" + content + '\'' +
        ", posterId=" + posterId +
        ", source='" + source + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}