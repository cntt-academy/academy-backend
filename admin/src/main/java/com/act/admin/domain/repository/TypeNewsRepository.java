package com.act.admin.domain.repository;

import com.act.admin.domain.entity.TypeNews;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface TypeNewsRepository {

  Page<TypeNews> findAllByCondition(String name, PageRequest pageRequest);

  TypeNews findById(Integer typeId);

  TypeNews findByName(String name);

  String getNameById(Integer typeId);

  void save(TypeNews typeCourse);

  List<TypeNews> findAll();

}
