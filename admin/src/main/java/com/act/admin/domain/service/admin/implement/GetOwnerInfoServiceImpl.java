package com.act.admin.domain.service.admin.implement;

import com.act.admin.Infrastructure.exception.ProcessException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.Infrastructure.utils.GenericMapper;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import com.act.admin.domain.service.admin.dispatcher.GetOwnerInfoService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetOwnerInfoService")
public class GetOwnerInfoServiceImpl implements GetOwnerInfoService {

  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final AdminUtils adminUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET OWNER INFO ========");

    Admin owner = adminRepository.findByRole(RoleEnums.Owner.name());
    log.info("<Query> => Result finding owner: {}", owner);
    if (owner == null) {
      throw new ProcessException("Không tìm thấy thông tin Giám đốc trung tâm");
    }

    AdminOutputDto ownerOutputDto = genericMapper.mapToAdminOutputDto(owner);
    // Set thông tin giám đốc trung tâm vào Redis
    adminUtils.setRedisAdminInfo("OWNER", ownerOutputDto);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin giám đốc trung tâm và lưu vào redis thành công");

    log.info("<Result API> => Getting owner info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
