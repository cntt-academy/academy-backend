package com.act.admin.domain.service.mediaComponent.dispatcher;

import com.act.admin.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetMediaComponentService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
