package com.act.admin.domain.service.news.implement;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.utils.AdminUtils;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.application.response.JsonResponseBase;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.repository.NewsRepository;
import com.act.admin.domain.service.news.dispatcher.DeleteNewsService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteNewsService")
public class DeleteNewsServiceImpl implements DeleteNewsService {

  private final NewsRepository newsRepository;
  private final AdminUtils adminUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private News news;

  @Override
  public ResponseEntity<?> execute(Integer newsId) {
    log.info("======== API DELETE NEWS ========");
    log.info("News Id: {}", newsId);
    return DeleteNewsService.super.execute(newsId);
  }

  @Override
  public void validate(Integer newsId) {
    loggedAccount = adminUtils.getAccountLoginInfo();
    if (!adminUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa tin tức");
    }

    news = newsRepository.findById(newsId);
    log.info("<Query> => Result finding news by id: {}", news);
    if (news == null) {
      throw new BadRequestException("Không tìm thấy tin tức muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer newsId) {
    news.setState(NewsStateEnums.DELETED.name());
    news.setUpdatedBy(loggedAccount.get("name"));
    news.setUpdatedDate(new Date());
    newsRepository.save(news);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa tin tức thành công");

    log.info("<Result API> => Deleting news successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
