package com.act.admin.Infrastructure.persistance.impl;

import static com.act.admin.application.enums.RoleEnums.Collaborator;
import static com.act.admin.application.enums.RoleEnums.Owner;
import static com.act.admin.application.enums.StateEnums.ACTIVE;

import com.act.admin.Infrastructure.persistance.jpa.AdminJpaRepository;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.repository.AdminRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("AdminRepository")
public class AdminRepositoryImpl implements AdminRepository {

  private final AdminJpaRepository adminJpaRepository;

  @Override
  public Page<Admin> findAllByCondition(String search, String role, PageRequest pageRequest) {
    return adminJpaRepository.findAllByCondition(search, role, ACTIVE.name(), pageRequest);
  }

  @Override
  public Admin findByEmailAndRole(String email, String role) {
    return adminJpaRepository.findByEmailAndRoleAndState(email, role, ACTIVE.name()).orElse(null);
  }

  @Override
  public Admin findByIdAndRole(Integer adminId, String role) {
    return adminJpaRepository.findByIdAndRoleAndState(adminId, role, ACTIVE.name()).orElse(null);
  }

  @Override
  public Admin findById(Integer adminId) {
    return adminJpaRepository.findByIdAndState(adminId, ACTIVE.name()).orElse(null);
  }

  @Override
  public Admin findByEmail(String email) {
    return adminJpaRepository.findByEmailAndState(email, ACTIVE.name()).orElse(null);
  }

  @Override
  public Admin findByRole(String role) {
    return adminJpaRepository.findByRoleAndState(role, ACTIVE.name()).orElse(null);
  }

  @Override
  public void saveAll(List<Admin> admins) {
    adminJpaRepository.saveAll(admins);
  }

  @Override
  public void save(Admin admin) {
    adminJpaRepository.save(admin);
  }

  @Override
  public List<String> findAllEmailCollaborator() {
    return adminJpaRepository.findAllEmailCollaborator(Collaborator.name(), ACTIVE.name());
  }

  @Override
  public List<Admin> findAll() {
    return adminJpaRepository.findAllByRoleNotAndState(Owner.name(), ACTIVE.name());
  }
}
