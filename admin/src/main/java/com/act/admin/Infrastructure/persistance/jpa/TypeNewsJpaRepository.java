package com.act.admin.Infrastructure.persistance.jpa;

import com.act.admin.domain.entity.TypeNews;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TypeNewsJpaRepository extends JpaRepository<TypeNews, Integer> {

  @Query(
      value =
          "SELECT DISTINCT(tc) FROM TypeNews tc "
              + "WHERE (:name IS NULL OR tc.name LIKE :name) "
              + "AND tc.state = :state")
  Page<TypeNews> findAllByCondition(
      @Param(value = "name") String name,
      @Param(value = "state") String state,
      Pageable pageable);

  Optional<TypeNews> findByNameIgnoreCaseAndState(String name, String state);

  Optional<TypeNews> findByIdAndState(Integer typeId, String state);

  List<TypeNews> findAllByState(String state);
}