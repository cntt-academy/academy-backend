package com.act.admin.Infrastructure.utils;

import com.act.admin.Infrastructure.exception.BadRequestException;
import com.act.admin.Infrastructure.filter.UserInfo;
import com.act.admin.application.enums.GenderEnums;
import com.act.admin.application.enums.RedisEnums;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.domain.dto.AdminOutputDto;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("AdminUtils")
public class AdminUtils {

  public static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  public final EurekaClient eurekaClient;

  /**
   * Chuẩn hóa parameter Query Like
   */
  public String standardizedParameterQueryLike(String str) {
    return str == null || str.trim().isEmpty() ? null : "%" + str + "%";
  }


  /**
   * Chuẩn hóa parameter Query Equal type Integer
   */
  public Integer standardizedParameterQueryEqual(Integer number) {
    return number == null || number == 0 ? null : number;
  }


  /**
   * Lấy ra thông tin Account đang login
   */
  public Map<String, String> getAccountLoginInfo() {
    log.info("<AdminUtils> => Function getAdminLoginInfo");

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserInfo userInfo = (UserInfo) authentication.getDetails();

    String email = userInfo.getEmail();
    log.info("Email: {}", email);
    String name = userInfo.getName();
    log.info("Name: {}", name);
    String role = userInfo.getRole();
    log.info("Role: {}", role);

    return Map.of("email", email, "name", name, "role", role);
  }

  /**
   * Kiểm tra quyền của tài khoản
   */
  public Boolean isOwnerOrManager(String role) {
    return role.equals(RoleEnums.Owner.name()) || role.equals(RoleEnums.Manager.name());
  }

  /**
   * Validate và chuẩn hóa Gender
   */
  public String validateAndStandardizedGender(String gender) {
    try {
      GenderEnums genderEnums = GenderEnums.valueOf(StringUtils.capitalize(gender.toLowerCase()));
      return genderEnums.name();
    } catch (Exception ex) {
      throw new BadRequestException("Giới tính không hợp lệ");
    }
  }

  /**
   * Set thông tin Admin vào redis theo key
   */
  public void setRedisAdminInfo(String key, AdminOutputDto admin) {
    RedisUtils.hset(key, RedisEnums.ID.name(), admin.getId());
    RedisUtils.hset(key, RedisEnums.AVATAR.name(), admin.getAvatar());
    RedisUtils.hset(key, RedisEnums.NAME.name(), admin.getName());
    RedisUtils.hset(key, RedisEnums.GENDER.name(), admin.getGender());
    RedisUtils.hset(key, RedisEnums.DOB.name(), admin.getDob());
    RedisUtils.hset(key, RedisEnums.PHONE.name(), admin.getPhone());
    RedisUtils.hset(key, RedisEnums.EMAIL.name(), admin.getEmail());
    RedisUtils.hset(key, RedisEnums.ROLE.name(), admin.getRole());
    RedisUtils.hset(key, RedisEnums.ADDRESS.name(), admin.getAddress());
    RedisUtils.hset(key, RedisEnums.JOB.name(), admin.getJob());
    RedisUtils.hset(key, RedisEnums.WORK_PLACE.name(), admin.getWorkplace());
    RedisUtils.hset(key, RedisEnums.CREATED_BY.name(), admin.getCreatedBy());
    RedisUtils.hset(key, RedisEnums.CREATED_DATE.name(), admin.getCreatedDate());
  }
}
