package com.act.admin.Infrastructure.persistance.jpa;

import com.act.admin.domain.entity.Admin;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdminJpaRepository extends JpaRepository<Admin, Integer> {

  Optional<Admin> findByEmailAndRoleAndState(String email, String role, String state);

  Optional<Admin> findByIdAndRoleAndState(Integer adminId, String role, String state);

  Optional<Admin> findByEmailAndState(String email, String state);

  Optional<Admin> findByRoleAndState(String role, String state);

  Optional<Admin> findByIdAndState(Integer adminId, String state);

  List<Admin> findAllByRoleNotAndState(String roleNot, String state);

  @Query(value = "SELECT DISTINCT(a.email) FROM Admin a  WHERE a.role = :role AND a.state = :state")
  List<String> findAllEmailCollaborator(
      @Param(value = "role") String role,
      @Param(value = "state") String state);

  @Query(
      value =
          "SELECT DISTINCT(a) FROM Admin a "
              + "WHERE (:search IS NULL OR (a.name LIKE :search OR a.email LIKE :search OR a.phone LIKE :search)) "
              + "AND a.role = :role "
              + "AND a.state = :state")
  Page<Admin> findAllByCondition(
      @Param(value = "search") String search,
      @Param(value = "role") String role,
      @Param(value = "state") String state,
      Pageable pageable);
}