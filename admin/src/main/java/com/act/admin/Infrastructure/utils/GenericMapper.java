package com.act.admin.Infrastructure.utils;

import com.act.admin.application.enums.GenderEnums;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.domain.dto.AdminOutputDto;
import com.act.admin.domain.dto.NewsOutputDto;
import com.act.admin.domain.dto.TypeNewsOutputDto;
import com.act.admin.domain.entity.Admin;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@RequiredArgsConstructor
@Component("GenericMapper")
public class GenericMapper {

  private final TypeNewsRepository typeNewsRepository;
  private final ModelMapper modelMapper;

  // map cả null sang
  public <T, E> E mapToType(T source, Class<E> typeDestination) {
    if (source == null) {
      return null;
    }
    return modelMapper.map(source, typeDestination);
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (source == null || source.isEmpty()) {
      return new ArrayList<>();
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }

  public void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  public String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  public AdminOutputDto mapToAdminOutputDto(Admin admin) {
    AdminOutputDto adminOutputDto = mapToType(admin, AdminOutputDto.class);
    if (adminOutputDto != null) {
      adminOutputDto.setDob(AdminUtils.sdf.format(admin.getDob()));
      adminOutputDto.setGender(GenderEnums.valueOf(admin.getGender()).getValue());
      adminOutputDto.setCreatedDate(AdminUtils.sdf.format(admin.getCreatedDate()));
    }
    return adminOutputDto;
  }

  public List<AdminOutputDto> mapToListAdminOutputDto(List<Admin> admins) {
    if (CollectionUtils.isEmpty(admins)) {
      return new ArrayList<>();
    }
    return admins.stream()
        .map(this::mapToAdminOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public TypeNewsOutputDto mapToTypeNewsOutputDto(TypeNews typeNews) {
    TypeNewsOutputDto typeNewsOutputDto = mapToType(typeNews, TypeNewsOutputDto.class);
    if (typeNewsOutputDto != null) {
      typeNewsOutputDto.setCreatedDate(AdminUtils.sdf.format(typeNews.getCreatedDate()));
    }
    return typeNewsOutputDto;
  }

  public List<TypeNewsOutputDto> mapToListTypeNewsOutputDto(List<TypeNews> typeNews) {
    if (CollectionUtils.isEmpty(typeNews)) {
      return new ArrayList<>();
    }
    return typeNews.stream()
        .map(this::mapToTypeNewsOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public NewsOutputDto mapToNewsOutputDto(News news) {
    NewsOutputDto newsOutputDto = mapToType(news, NewsOutputDto.class);
    if (newsOutputDto != null) {
      newsOutputDto.setType(typeNewsRepository.getNameById(news.getTypeId()));
      newsOutputDto.setState(NewsStateEnums.valueOf(news.getState()).getValue());
      newsOutputDto.setCreatedDate(AdminUtils.sdf.format(news.getCreatedDate()));
    }
    return newsOutputDto;
  }

  public List<NewsOutputDto> mapToListNewsOutputDto(List<News> listNews) {
    if (CollectionUtils.isEmpty(listNews)) {
      return new ArrayList<>();
    }
    return listNews.stream()
        .map(this::mapToNewsOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}
