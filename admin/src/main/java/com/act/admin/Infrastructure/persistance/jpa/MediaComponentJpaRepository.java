package com.act.admin.Infrastructure.persistance.jpa;

import com.act.admin.domain.entity.MediaComponent;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaComponentJpaRepository extends JpaRepository<MediaComponent, Integer> {

  List<MediaComponent> findAllByNameNot(String nameNot);

  Optional<MediaComponent> findByName(String name);
}