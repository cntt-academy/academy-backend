package com.act.admin.Infrastructure.persistance.impl;

import com.act.admin.Infrastructure.persistance.jpa.NewsJpaRepository;
import com.act.admin.application.enums.NewsStateEnums;
import com.act.admin.domain.entity.News;
import com.act.admin.domain.repository.NewsRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("NewsRepository")
public class NewsRepositoryImpl implements NewsRepository {

  private final NewsJpaRepository newsJpaRepository;

  @Override
  public Page<News> findAllByConditions(
      String state, String search, Integer typeId, Integer typeIdNot, PageRequest pageRequest) {
    return newsJpaRepository
        .findAllByConditions(state, search, typeId, NewsStateEnums.DELETED.name(), typeIdNot, pageRequest);
  }

  @Override
  public News findById(Integer newsId) {
    return newsJpaRepository
        .findByIdAndStateNot(newsId, NewsStateEnums.DELETED.name())
        .orElse(null);
  }

  @Override
  public News findByName(String name) {
    return newsJpaRepository
        .findByNameIgnoreCaseAndStateNot(name, NewsStateEnums.DELETED.name())
        .orElse(null);
  }

  @Override
  public void save(News news) {
    newsJpaRepository.save(news);
  }
}
