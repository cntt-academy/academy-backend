package com.act.admin.Infrastructure.persistance.impl;

import com.act.admin.Infrastructure.persistance.jpa.MediaComponentJpaRepository;
import com.act.admin.domain.entity.MediaComponent;
import com.act.admin.domain.repository.MediaComponentRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("MediaComponentRepository")
public class MediaComponentRepositoryImpl implements MediaComponentRepository {

  private final MediaComponentJpaRepository mediaComponentJpaRepository;

  @Override
  public List<MediaComponent> findAllSlider() {
    return mediaComponentJpaRepository.findAllByNameNot("video_banner");
  }

  @Override
  public MediaComponent findVideoBanner() {
    return mediaComponentJpaRepository.findByName("video_banner").orElse(null);
  }

  @Override
  public List<MediaComponent> findAll() {
    return mediaComponentJpaRepository.findAll();
  }

  @Override
  public void save(MediaComponent mediaComponent) {
    mediaComponentJpaRepository.save(mediaComponent);
  }

  @Override
  public void saveAll(List<MediaComponent> mediaComponents) {
    mediaComponentJpaRepository.saveAll(mediaComponents);
  }
}
