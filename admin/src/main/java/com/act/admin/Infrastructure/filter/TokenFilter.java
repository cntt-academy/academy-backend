package com.act.admin.Infrastructure.filter;

import com.act.admin.Infrastructure.exception.UnauthorizedException;
import com.act.admin.Infrastructure.utils.UnirestUtils;
import com.act.admin.application.enums.RoleEnums;
import com.act.admin.application.response.ErrorResponseBase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@RequiredArgsConstructor
public class TokenFilter extends OncePerRequestFilter {

  private final EurekaClient eurekaClient;
  private final String pathVerifyToken;
  private final Gson gson;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws IOException {
    try {
      String token = request.getHeader("Authorization");
      if (StringUtils.isBlank(token)) {
        throw new UnauthorizedException("Token get from header is null or empty");
      }

      // Set headers
      Map<String, String> headers = new HashMap<>();
      headers.put("Content-Type", "application/json");
      headers.put("Authorization", token);

      // Call API to verify token
      String urlVerifyToken = eurekaClient
          .getNextServerFromEureka("authentication", false).getHomePageUrl()
          + "authentication" + pathVerifyToken;
      JsonObject resp = UnirestUtils.get(urlVerifyToken, headers);
      if (resp.get("status").getAsInt() != HttpStatus.OK.value()) {
        throw new UnauthorizedException("Verifying token failed");
      }

      // Get response API
      JsonObject data = resp.get("data").getAsJsonObject();
      String email = data.get("email").getAsString();
      String name = data.get("name").getAsString();
      String role = data.get("role").getAsString();
      if (StringUtils.isBlank(email)
          || StringUtils.isBlank(role)
          || role.equals(RoleEnums.User.name())) {
        throw new UnauthorizedException("UserDetails get from token is null");
      }

      // Set authentication
      UserInfo userInfo = new UserInfo(email, name, role);
      CustomUserDetails userDetails = new CustomUserDetails(userInfo);
      UsernamePasswordAuthenticationToken auth =
          new UsernamePasswordAuthenticationToken(
              userDetails.getUsername(), "", userDetails.getAuthorities());
      auth.setDetails(userInfo);
      SecurityContextHolder.getContext().setAuthentication(auth);

      filterChain.doFilter(request, response);
    } catch (Exception e) {
      log.error("Filter token error: {}", e.getMessage());
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      response.setContentType("application/json");
      response.getWriter()
          .write(gson.toJson(new ErrorResponseBase(401, HttpStatus.UNAUTHORIZED, e.getMessage())));
    }
  }
}
