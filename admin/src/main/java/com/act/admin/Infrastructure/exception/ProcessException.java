package com.act.admin.Infrastructure.exception;

public class ProcessException extends RuntimeException {

  public ProcessException(String message) {
    super(message);
  }
}
