package com.act.admin.Infrastructure.persistance.impl;

import com.act.admin.Infrastructure.persistance.jpa.TypeNewsJpaRepository;
import com.act.admin.application.enums.StateEnums;
import com.act.admin.domain.entity.TypeNews;
import com.act.admin.domain.repository.TypeNewsRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("TypeNewsRepository")
public class TypeNewsRepositoryImpl implements TypeNewsRepository {

  private final TypeNewsJpaRepository typeNewsJpaRepository;

  @Override
  public Page<TypeNews> findAllByCondition(String name, PageRequest pageRequest) {
    return typeNewsJpaRepository.findAllByCondition(name, StateEnums.ACTIVE.name(), pageRequest);
  }

  @Override
  public TypeNews findById(Integer typeId) {
    return typeNewsJpaRepository
        .findByIdAndState(typeId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public TypeNews findByName(String name) {
    return typeNewsJpaRepository
        .findByNameIgnoreCaseAndState(name, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public String getNameById(Integer typeId) {
    TypeNews type = findById(typeId);
    return type == null ? "" : type.getName();
  }

  @Override
  public void save(TypeNews typeCourse) {
    typeNewsJpaRepository.save(typeCourse);
  }

  @Override
  public List<TypeNews> findAll() {
    return typeNewsJpaRepository.findAllByState(StateEnums.ACTIVE.name());
  }
}
