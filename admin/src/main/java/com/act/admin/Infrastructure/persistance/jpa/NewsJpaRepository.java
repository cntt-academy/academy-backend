package com.act.admin.Infrastructure.persistance.jpa;

import com.act.admin.domain.entity.News;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NewsJpaRepository extends JpaRepository<News, Integer> {

  Optional<News> findByNameIgnoreCaseAndStateNot(String name, String stateNot);

  List<News> findAllByTypeIdAndStateNot(Integer typeId, String stateNot);

  Optional<News> findByIdAndStateNot(Integer newsId, String stateNot);

  @Query(
      value =
          "SELECT DISTINCT(n) FROM News n "
              + "WHERE (:search IS NULL OR (n.name LIKE :search OR n.source LIKE :search)) "
              + "AND (:typeIdNot IS NULL OR n.typeId <> :typeIdNot) "
              + "AND (:typeId IS NULL OR n.typeId = :typeId) "
              + "AND (:state IS NULL OR n.state = :state) "
              + "AND n.state <> :stateNot")
  Page<News> findAllByConditions(
      @Param(value = "state") String state,
      @Param(value = "search") String search,
      @Param(value = "typeId") Integer typeId,
      @Param(value = "stateNot") String stateNot,
      @Param(value = "typeIdNot") Integer typeIdNot,
      Pageable pageable);

}