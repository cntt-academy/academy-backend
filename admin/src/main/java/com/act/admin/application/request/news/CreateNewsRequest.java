package com.act.admin.application.request.news;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateNewsRequest implements Serializable {

  @NotBlank(message = "Ảnh đại diện")
  private String img;

  @NotBlank(message = "Tên tin tức")
  private String name;

  @NotBlank(message = "Mô tả tin tức")
  private String desc;

  @NotBlank(message = "Nội dung tin tức")
  private String content;

  @NotNull(message = "Id loại tin tức")
  private Integer typeId;

  private String source;
}
