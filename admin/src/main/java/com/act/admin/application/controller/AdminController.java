package com.act.admin.application.controller;

import com.act.admin.application.request.admin.ChangeManagerRequest;
import com.act.admin.application.request.admin.CreateCollaboratorRequest;
import com.act.admin.application.request.admin.GetListCollaboratorRequest;
import com.act.admin.application.request.admin.UpdateAdminRequest;
import com.act.admin.domain.service.admin.dispatcher.ChangeManagerService;
import com.act.admin.domain.service.admin.dispatcher.CheckExistsService;
import com.act.admin.domain.service.admin.dispatcher.CreateCollaboratorService;
import com.act.admin.domain.service.admin.dispatcher.DeleteCollaboratorService;
import com.act.admin.domain.service.admin.dispatcher.GetAdminByIdService;
import com.act.admin.domain.service.admin.dispatcher.GetAllAdminService;
import com.act.admin.domain.service.admin.dispatcher.GetListCollaboratorService;
import com.act.admin.domain.service.admin.dispatcher.GetManagerInfoService;
import com.act.admin.domain.service.admin.dispatcher.GetOwnerInfoService;
import com.act.admin.domain.service.admin.dispatcher.UpdateAdminService;
import com.act.admin.domain.service.internal.dispatcher.GetAdminByEmailService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("AdminController")
@RequestMapping(value = "/admin", produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminController {

  private final GetListCollaboratorService getListCollaboratorService;
  private final DeleteCollaboratorService deleteCollaboratorService;
  private final CreateCollaboratorService createCollaboratorService;
  private final GetManagerInfoService getManagerInfoService;
  private final ChangeManagerService changeManagerService;
  private final GetOwnerInfoService getOwnerInfoService;
  private final GetAdminByIdService getAdminByIdService;
  private final UpdateAdminService updateAdminService;
  private final GetAllAdminService getAllAdminService;
  private final CheckExistsService checkExistsService;

  @GetMapping(value = "/checkExists")
  public Object checkExists(@RequestParam String email) {
    return checkExistsService.execute(email);
  }

  @GetMapping(value = "/getById")
  public Object getById(@RequestParam String adminId) {
    return getAdminByIdService.execute(adminId);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllAdminService.execute();
  }

  @GetMapping(value = "/getOwnerInfo")
  public Object getOwnerInfo() {
    return getOwnerInfoService.execute();
  }

  @GetMapping(value = "/getManagerInfo")
  public Object getManagerInfo() {
    return getManagerInfoService.execute();
  }

  @PostMapping(value = "/getListCollaborator")
  public Object getListCollaborator(@Valid @RequestBody GetListCollaboratorRequest request) {
    return getListCollaboratorService.execute(request);
  }

  @PostMapping(value = "/create")
  public Object createCollaborator(@Valid @RequestBody CreateCollaboratorRequest request) {
    return createCollaboratorService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object updateAdmin(@Valid @RequestBody UpdateAdminRequest request) {
    return updateAdminService.execute(request);
  }

  @PutMapping(value = "/changeManager")
  public Object changeManager(
      HttpServletRequest httpServletRequest,
      @Valid @RequestBody ChangeManagerRequest request) {
    request.setHttpServletRequest(httpServletRequest);
    return changeManagerService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object deleteCollaborator(@Valid @RequestParam String collaboratorId) {
    return deleteCollaboratorService.execute(collaboratorId);
  }
}
