package com.act.admin.application.controller;

import com.act.admin.application.request.news.CreateNewsRequest;
import com.act.admin.application.request.news.GetListNewsRequest;
import com.act.admin.application.request.news.UpdateNewsRequest;
import com.act.admin.domain.service.news.dispatcher.AcceptNewsService;
import com.act.admin.domain.service.news.dispatcher.CreateNewsService;
import com.act.admin.domain.service.news.dispatcher.DeleteNewsService;
import com.act.admin.domain.service.news.dispatcher.GetDetailNewsService;
import com.act.admin.domain.service.news.dispatcher.GetListNewsService;
import com.act.admin.domain.service.news.dispatcher.UpdateNewsService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("NewsController")
@RequestMapping(value = "/news", produces = MediaType.APPLICATION_JSON_VALUE)
public class NewsController {

  private final GetDetailNewsService getDetailNewsService;
  private final GetListNewsService getListNewsService;
  private final CreateNewsService createNewsService;
  private final UpdateNewsService updateNewsService;
  private final DeleteNewsService deleteNewsService;
  private final AcceptNewsService acceptNewsService;

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer newsId) {
    return getDetailNewsService.execute(newsId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListNewsRequest request) {
    return getListNewsService.execute(request);
  }

  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateNewsRequest request) {
    return createNewsService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateNewsRequest request) {
    return updateNewsService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer newsId) {
    return deleteNewsService.execute(newsId);
  }

  @GetMapping(value = "/accept")
  public Object accept(@RequestParam Integer newsId) {
    return acceptNewsService.execute(newsId);
  }
}
