package com.act.admin.application.request.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateAdminRequest implements Serializable {

  @NotNull(message = "Id cộng tác viên")
  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String dob;
  private String phone;
  private String address;
  private String job;
  private String workplace;
}