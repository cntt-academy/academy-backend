package com.act.admin.application.request.mediaComponent;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSliderRequest {

  private String urlSlider1;
  private String urlSlider2;
  private String urlSlider3;
  private String urlSlider4;
  private String urlSlider5;
}
