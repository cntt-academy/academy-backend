package com.act.admin.application.request.admin;

import com.act.admin.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListCollaboratorRequest implements Serializable {

  private String search;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
