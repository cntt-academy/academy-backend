package com.act.admin.application.controller;

import com.act.admin.application.request.typeNews.CreateTypeNewsRequest;
import com.act.admin.application.request.typeNews.GetListTypeNewsRequest;
import com.act.admin.application.request.typeNews.UpdateTypeNewsRequest;
import com.act.admin.domain.service.typeNews.dispatcher.CreateTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.DeleteTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.GetAllTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.GetDetailTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.GetListTypeNewsService;
import com.act.admin.domain.service.typeNews.dispatcher.UpdateTypeNewsService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("TypeNewsController")
@RequestMapping(value = "/type-news", produces = MediaType.APPLICATION_JSON_VALUE)
public class TypeNewsController {

  private final GetDetailTypeNewsService getDetailTypeNewsService;
  private final GetListTypeNewsService getListTypeNewsService;
  private final GetAllTypeNewsService getAllTypeNewsService;
  private final CreateTypeNewsService createTypeNewsService;
  private final UpdateTypeNewsService updateTypeNewsService;
  private final DeleteTypeNewsService deleteTypeNewsService;

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer typeNewsId) {
    return getDetailTypeNewsService.execute(typeNewsId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListTypeNewsRequest request) {
    return getListTypeNewsService.execute(request);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllTypeNewsService.execute();
  }


  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateTypeNewsRequest request) {
    return createTypeNewsService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateTypeNewsRequest request) {
    return updateTypeNewsService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer typeNewsId) {
    return deleteTypeNewsService.execute(typeNewsId);
  }
}
