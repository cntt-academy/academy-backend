package com.act.admin.application.request.news;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateNewsRequest implements Serializable {

  @NotNull(message = "Id tin tức")
  private Integer id;
  private String img;
  private String name;
  private String desc;
  private String content;
  private Integer typeId;
  private String source;
}
