package com.act.admin.application.request.typeNews;

import com.act.admin.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListTypeNewsRequest implements Serializable {

  private String name;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
