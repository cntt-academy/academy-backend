package com.act.admin.application.controller;

import com.act.admin.application.request.mediaComponent.UpdateSliderRequest;
import com.act.admin.application.request.mediaComponent.UpdateVideoBannerRequest;
import com.act.admin.domain.service.mediaComponent.dispatcher.GetMediaComponentService;
import com.act.admin.domain.service.mediaComponent.dispatcher.UpdateSliderService;
import com.act.admin.domain.service.mediaComponent.dispatcher.UpdateVideoBannerService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("MediaComponentController")
@RequestMapping(value = "/media-component", produces = MediaType.APPLICATION_JSON_VALUE)
public class MediaComponentController {

  private final GetMediaComponentService getMediaComponentService;
  private final UpdateVideoBannerService updateVideoBannerService;
  private final UpdateSliderService updateSliderService;

  @GetMapping(value = "/getMediaComponent")
  public Object getMediaComponent() {
    return getMediaComponentService.execute();
  }

  @PutMapping(value = "/updateVideoBanner")
  public Object updateVideoBanner(@Valid @RequestBody UpdateVideoBannerRequest request) {
    return updateVideoBannerService.execute(request);
  }

  @PutMapping(value = "/updateSlider")
  public Object updateSlider(@Valid @RequestBody UpdateSliderRequest request) {
    return updateSliderService.execute(request);
  }
}
