package com.act.admin.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NewsStateEnums {
  ACTIVE("Hoạt động"),
  DELETED("Đã xóa"),
  WAIT("Chờ duyệt");
  private final String value;
}
