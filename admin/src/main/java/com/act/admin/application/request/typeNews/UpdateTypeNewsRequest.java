package com.act.admin.application.request.typeNews;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateTypeNewsRequest implements Serializable {

  @NotNull(message = "Id loại tin tức")
  private Integer id;
  private String name;
  private String desc;
}