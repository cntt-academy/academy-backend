package com.act.admin.application.request.news;

import com.act.admin.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListNewsRequest implements Serializable {

  private String state;
  private String search;
  private Integer typeId;
  private Integer typeIdNot;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
