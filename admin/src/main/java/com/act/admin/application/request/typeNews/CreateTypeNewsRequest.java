package com.act.admin.application.request.typeNews;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateTypeNewsRequest implements Serializable {

  @NotBlank(message = "Tên loại tin tức")
  private String name;

  @NotBlank(message = "Mô tả")
  private String desc;
}