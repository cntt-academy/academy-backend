package com.act.admin.application.request.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChangeManagerRequest implements Serializable {

  private HttpServletRequest httpServletRequest;

  @NotBlank(message = "Email Quản lý trung tâm mới")
  private String email;
}