package com.act.admin.application.request.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateCollaboratorRequest implements Serializable {

  @NotBlank(message = "Ảnh đại diện")
  private String avatar;

  @NotBlank(message = "Tên cộng tác viên")
  private String name;

  @NotBlank(message = "Giới tính")
  private String gender;

  @NotBlank(message = "Ngày sinh")
  private String dob;

  @NotBlank(message = "Số điện thoại")
  private String phone;

  @NotBlank(message = "Email")
  private String email;

  private String address;

  private String job;

  private String workplace;
}