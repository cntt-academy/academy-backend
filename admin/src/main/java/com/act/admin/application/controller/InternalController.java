package com.act.admin.application.controller;

import com.act.admin.domain.service.internal.dispatcher.GetAdminByEmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("InternalController")
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalController {

  private final GetAdminByEmailService getAdminByEmailService;

  @GetMapping(value = "/admin/getByEmail")
  public Object getByEmail(@RequestParam String email) {
    return getAdminByEmailService.execute(email);
  }
}
