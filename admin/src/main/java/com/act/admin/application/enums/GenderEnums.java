package com.act.admin.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenderEnums {
  Male("Nam"),
  Female("Nữ");
  private final String value;
}
