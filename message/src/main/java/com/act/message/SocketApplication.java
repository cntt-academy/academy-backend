package com.act.message;

import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.enums.MessageStateEnums;
import com.act.message.domain.dto.MessageOutputDto;
import com.act.message.domain.entity.Message;
import com.act.message.domain.repository.MessageRepository;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SocketApplication {

  public static void startSocketApplication(
      String host, Integer port, MessageRepository messageRepository, MessageUtils messageUtils) {
    Configuration configuration = new Configuration();
    configuration.setHostname(host);
    configuration.setPort(port);
    configuration.setTransports(Transport.POLLING);

    final SocketIOServer server = new SocketIOServer(configuration);

    server.addEventListener("client-leave-room", String.class, (client, data, ackRequest) -> {
      log.info("client-leave-room: {}", data);
      client.leaveRoom(data);
    });

    server.addEventListener("client-join-room", String.class, (client, data, ackRequest) -> {
      log.info("client-join-room: {}", data);
      client.joinRoom(data);
    });

    server.addEventListener("client-send-message", String.class, (client, data, ackRequest) -> {
      log.info("client-send-message: {}", data);

      ObjectMapper objectMapper = new ObjectMapper();
      Message message = objectMapper.readValue(data, Message.class);
      message.setSendTime(new Date());
      message.setState(MessageStateEnums.WAIT.name());
      messageRepository.save(message);

      CompletableFuture<Map<String, Object>> accountInfo =
          CompletableFuture.supplyAsync(() -> messageUtils.getAccountById(message.getAccountId()));

      MessageOutputDto messageOutputDto = new MessageOutputDto();
      messageOutputDto.setContent(message.getContent());
      messageOutputDto.setRoomId(message.getRoomId());
      messageOutputDto.setAccountId(message.getAccountId());
      messageOutputDto.setAccountName((String) accountInfo.get().get("name"));
      messageOutputDto.setSendTime(messageUtils.formatSendTimeMessage(message.getSendTime()));

      log.info("client-send-message");
      log.info("room: " + message.getRoomId());
      log.info("server-send-message");
      log.info(objectMapper.writeValueAsString(messageOutputDto));

      server
          .getRoomOperations(message.getRoomId().toString())
          .sendEvent("server-send-message", objectMapper.writeValueAsString(messageOutputDto));
    });

    server.start();
  }
}