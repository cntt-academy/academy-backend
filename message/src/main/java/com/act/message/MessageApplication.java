package com.act.message;

import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.domain.repository.MessageRepository;
import java.util.Properties;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@EnableDiscoveryClient
@SpringBootApplication
@RequiredArgsConstructor
public class MessageApplication implements CommandLineRunner {

  private final MessageRepository messageRepository;
  private final MessageUtils messageUtils;
  // EMAIL
  @Value("${config.mail.systemEmail}")
  private String systemEmail;
  @Value("${config.mail.password}")
  private String password;
  @Value("${config.mail.host}")
  private String host;
  @Value("${config.mail.port}")
  private String port;
  // SOCKET
  @Value("${socket.port}")
  private Integer socketPort;
  @Value("${socket.host}")
  private String socketHost;

  public static void main(String[] args) {
    SpringApplication.run(MessageApplication.class, args);
  }

  @Override
  public void run(String... args) {
    SocketApplication.startSocketApplication(
        socketHost, socketPort, messageRepository, messageUtils);
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    return modelMapper;
  }

  @Bean
  public JavaMailSender getJavaMailSender() {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(host);
    mailSender.setPort(Integer.parseInt(port));
    mailSender.setUsername(systemEmail);
    mailSender.setPassword(password);
    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", "true");
    props.put("mail.smtp.ssl.enable", "false");
    props.put("mail.smtp.socketFactory.class", "");
    props.put("mail.smtp.socketFactory.fallBack", "false");
    return mailSender;
  }
}
