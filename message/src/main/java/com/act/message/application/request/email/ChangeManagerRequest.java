package com.act.message.application.request.email;

import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ChangeManagerRequest implements Serializable {

  @NotBlank(message = "Thời gian")
  private String time;

  @NotBlank(message = "Email quản lý cũ")
  private String oldManager;

  @NotBlank(message = "Email quản lý mới")
  private String newManager;

  @NotBlank(message = "Tên quản lý mới")
  private String newManagerName;

  private List<String> collaborators;
}
