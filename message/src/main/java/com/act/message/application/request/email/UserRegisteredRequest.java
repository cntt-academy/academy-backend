package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserRegisteredRequest implements Serializable {

  @NotBlank(message = "Tên học viên")
  private String name;

  @NotBlank(message = "Email học viên")
  private String email;

  @NotBlank(message = "Khóa học")
  private String course;

  @NotBlank(message = "Hình thức học")
  private String howToLearn;

  @NotBlank(message = "Tên lớp học")
  private String className;

  @NotBlank(message = "Email email")
  private String classManagerEmail;

  @NotNull(message = "Có phải lớp dự bị ?")
  private Boolean isPrepClass;

  @NotBlank(message = "Thời gian")
  private String time;
}
