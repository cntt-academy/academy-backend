package com.act.message.application.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SendToConstants {
  public static final String USER = "User";
  public static final String MANAGER = "Manager";
  public static final String SUPPORTER = "Supporter";
  public static final String NEW_MANAGER = "New manager";
  public static final String OLD_MANAGER = "Old manager";
  public static final String CLASS_MANAGER = "Class manager";
  public static final String COLLABORATOR = "Collaborator";
  public static final String NOT_COLLABORATOR = "Not collaborator";
}