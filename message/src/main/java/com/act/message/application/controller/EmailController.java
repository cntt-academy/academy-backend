package com.act.message.application.controller;

import com.act.message.application.request.email.AccountCreatedRequest;
import com.act.message.application.request.email.ChangeManagerRequest;
import com.act.message.application.request.email.EncouragePayTuitionRequest;
import com.act.message.application.request.email.ForgetPasswordRequest;
import com.act.message.application.request.email.NewsAcceptedRequest;
import com.act.message.application.request.email.NewsCreatedRequest;
import com.act.message.application.request.email.UserAddedIntoClassRequest;
import com.act.message.application.request.email.UserCreatedRequest;
import com.act.message.application.request.email.UserRegisteredRequest;
import com.act.message.domain.service.email.dispatcher.AccountCreatedService;
import com.act.message.domain.service.email.dispatcher.ChangeManagerService;
import com.act.message.domain.service.email.dispatcher.EncouragePayTuitionService;
import com.act.message.domain.service.email.dispatcher.ForgetPasswordService;
import com.act.message.domain.service.email.dispatcher.NewsAcceptedService;
import com.act.message.domain.service.email.dispatcher.NewsCreatedService;
import com.act.message.domain.service.email.dispatcher.UserAddedIntoClassService;
import com.act.message.domain.service.email.dispatcher.UserCreatedService;
import com.act.message.domain.service.email.dispatcher.UserRegisteredService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("EmailController")
@RequestMapping(value = "/email", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmailController {

  private final EncouragePayTuitionService encouragePayTuitionService;
  private final UserAddedIntoClassService userAddedIntoClassService;
  private final UserRegisteredService userRegisteredService;
  private final ForgetPasswordService forgetPasswordService;
  private final AccountCreatedService accountCreatedService;
  private final ChangeManagerService changeManagerService;
  private final NewsAcceptedService newsAcceptedService;
  private final NewsCreatedService newsCreatedService;
  private final UserCreatedService userCreatedService;

  @PostMapping(value = "/encouragePayTuition")
  public Object encouragePayTuition(@Valid @RequestBody EncouragePayTuitionRequest request) {
    return encouragePayTuitionService.execute(request);
  }

  @PostMapping(value = "/userAddedIntoClass")
  public Object userAddedIntoClass(@Valid @RequestBody UserAddedIntoClassRequest request) {
    return userAddedIntoClassService.execute(request);
  }

  @PostMapping(value = "/userRegistered")
  public Object userRegistered(@Valid @RequestBody UserRegisteredRequest request) {
    return userRegisteredService.execute(request);
  }

  @PostMapping(value = "/forgetPassword")
  public Object forgetPassword(@Valid @RequestBody ForgetPasswordRequest request) {
    return forgetPasswordService.execute(request);
  }

  @PostMapping(value = "/accountCreated")
  public Object accountCreated(@Valid @RequestBody AccountCreatedRequest request) {
    return accountCreatedService.execute(request);
  }

  @PostMapping(value = "/changeManager")
  public Object changeManager(@Valid @RequestBody ChangeManagerRequest request) {
    return changeManagerService.execute(request);
  }

  @PostMapping(value = "/newsCreated")
  public Object newsCreated(@Valid @RequestBody NewsCreatedRequest request) {
    return newsCreatedService.execute(request);
  }

  @PostMapping(value = "/newsAccepted")
  public Object userCreated(@Valid @RequestBody NewsAcceptedRequest request) {
    return newsAcceptedService.execute(request);
  }

  @PostMapping(value = "/userCreated")
  public Object userCreated(@Valid @RequestBody UserCreatedRequest request) {
    return userCreatedService.execute(request);
  }
}
