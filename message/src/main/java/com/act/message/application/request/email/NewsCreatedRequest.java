package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class NewsCreatedRequest implements Serializable {

  @NotBlank(message = "Cộng tác viên viết bài")
  private String collaborator;

  @NotBlank(message = "Tiêu đề bài viết")
  private String name;

  @NotBlank(message = "Thời gian")
  private String time;
}
