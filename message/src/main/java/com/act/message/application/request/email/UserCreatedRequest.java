package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserCreatedRequest implements Serializable {

  @NotBlank(message = "Tên học viên")
  private String name;

  @NotBlank(message = "Giới tính")
  private String gender;

  @NotBlank(message = "Ngày sinh")
  private String dob;

  @NotBlank(message = "Số điện thoại")
  private String phone;

  @NotBlank(message = "Email")
  private String email;

  @NotBlank(message = "Thời gian")
  private String time;

  private String collaborator;
}
