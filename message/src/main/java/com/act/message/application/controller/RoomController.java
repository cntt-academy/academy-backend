package com.act.message.application.controller;

import com.act.message.domain.service.room.dispatcher.CreateRoomService;
import com.act.message.domain.service.room.dispatcher.FindRoomWithFriendService;
import com.act.message.domain.service.room.dispatcher.GetListRoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("RoomController")
@RequestMapping(value = "/room", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {

  private final FindRoomWithFriendService findRoomWithFriendService;
  private final GetListRoomService getListRoomService;
  private final CreateRoomService createRoomService;

  @GetMapping(value = "/findRoomWithFriend")
  public Object findRoomWithFriend(@RequestParam Integer friendId) {
    return findRoomWithFriendService.execute(friendId);
  }

  @GetMapping(value = "/getList")
  public Object getList() {
    return getListRoomService.execute();
  }

  @PostMapping(value = "/create")
  public Object create(@RequestParam Integer friendId) {
    return createRoomService.execute(friendId);
  }
}
