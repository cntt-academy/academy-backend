package com.act.message.application.controller;

import com.act.message.domain.service.message.dispatcher.GetListMessageService;
import com.act.message.domain.service.message.dispatcher.MarkAsReadMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("MessageController")
@RequestMapping(value = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
public class MessageController {

  private final MarkAsReadMessageService markAsReadMessageService;
  private final GetListMessageService getListMessageService;

  @GetMapping(value = "/markAsRead")
  public Object markAsRead(@RequestParam Integer roomId) {
    return markAsReadMessageService.execute(roomId);
  }

  @GetMapping(value = "/getList")
  public Object getList(@RequestParam Integer roomId) {
    return getListMessageService.execute(roomId);
  }
}
