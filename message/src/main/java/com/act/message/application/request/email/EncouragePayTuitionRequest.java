package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class EncouragePayTuitionRequest implements Serializable {

  @NotBlank(message = "Email quản lý lớp")
  private String classManagerEmail;

  @NotBlank(message = "Tên lớp học")
  private String className;

  @NotBlank(message = "Họ và tên học viên")
  private String studentName;

  @NotBlank(message = "Email học viên")
  private String studentEmail;

  @NotBlank(message = "Số điện thoại học viên")
  private String studentPhone;

  @NotBlank(message = "Thời gian")
  private String time;
}
