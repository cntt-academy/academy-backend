package com.act.message.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoomTypeEnums {
  Single("Phòng chat đơn"),
  Group("Phòng chat nhón");
  private final String value;
}
