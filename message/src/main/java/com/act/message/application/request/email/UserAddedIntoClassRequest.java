package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserAddedIntoClassRequest implements Serializable {

  @NotBlank(message = "Thời gian")
  private String time;

  @NotBlank(message = "Tên lớp học")
  private String className;

  @NotBlank(message = "Email email")
  private String classManagerEmail;

  @NotBlank(message = "Tên học viên")
  private String userName;

  @NotBlank(message = "Email học viên")
  private String userEmail;

  @NotBlank(message = "Tên cộng tác viên")
  private String collaboratorName;

  @NotBlank(message = "Email cộng tác viên")
  private String collaboratorEmail;

}
