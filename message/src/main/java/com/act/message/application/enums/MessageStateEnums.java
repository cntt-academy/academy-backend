package com.act.message.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageStateEnums {
  WAIT("Chờ đọc"),
  READ("Đã đọc");
  private final String value;
}