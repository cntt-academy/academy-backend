package com.act.message.application.request.email;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ForgetPasswordRequest implements Serializable {

  @NotBlank(message = "Email tài khoản")
  private String email;

  @NotBlank(message = "Phân quyền")
  private String role;

  @NotBlank(message = "Mật khẩu mới")
  private String newPassword;

  @NotBlank(message = "Thời gian")
  private String time;
}
