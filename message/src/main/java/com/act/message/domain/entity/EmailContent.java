package com.act.message.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email_contents")
public class EmailContent {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Size(max = 50)
  @NotNull
  @Column(name = "type", nullable = false, length = 50)
  private String type;

  @Size(max = 50)
  @NotNull
  @Column(name = "send_to", nullable = false, length = 50)
  private String sendTo;

  @NotNull
  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @Size(max = 10)
  @NotNull
  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Size(max = 50)
  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Size(max = 50)
  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @NotNull
  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "EmailContent{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", sendTo='" + sendTo + '\'' +
        ", content='" + content + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}