package com.act.message.domain.service.base;

public interface BaseServiceNoRequest<O> {

  O execute();
}
