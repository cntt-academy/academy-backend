package com.act.message.domain.service.email.dispatcher;

import com.act.message.application.request.email.EncouragePayTuitionRequest;
import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface EncouragePayTuitionService extends
    BaseServiceRequestBody<EncouragePayTuitionRequest, ResponseEntity<?>> {

}
