package com.act.message.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "rooms")
public class Room {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Size(max = 50)
  @NotNull
  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Size(max = 10)
  @NotNull
  @Column(name = "type", nullable = false, length = 10)
  private String type;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Transient String friendAvatar;
  @Transient String friendName;
  @Transient String friendEmail;
  @Transient String lastMessage;
  @Transient Date sendTime;
  @Transient Boolean isReadLastMessage;
}