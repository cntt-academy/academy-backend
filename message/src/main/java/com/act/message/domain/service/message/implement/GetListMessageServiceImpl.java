package com.act.message.domain.service.message.implement;

import com.act.message.Infrastructure.exception.BadRequestException;
import com.act.message.Infrastructure.utils.GenericMapper;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.Member;
import com.act.message.domain.entity.Message;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.MemberRepository;
import com.act.message.domain.repository.MessageRepository;
import com.act.message.domain.repository.RoomRepository;
import com.act.message.domain.service.message.dispatcher.GetListMessageService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListMessageService")
public class GetListMessageServiceImpl implements GetListMessageService {

  private final MessageRepository messageRepository;
  private final MemberRepository memberRepository;
  private final RoomRepository roomRepository;
  private final GenericMapper genericMapper;
  private final MessageUtils messageUtils;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(Integer roomId) {
    log.info("======== API GET LIST MESSAGE ========");
    log.info("Room Id: {}", roomId);
    return GetListMessageService.super.execute(roomId);
  }

  @Override
  public void validate(Integer roomId) {
    Room room = roomRepository.findById(roomId);
    log.info("<Query> => Result finding room by id: {}", gson.toJson(room));
    if (room == null) {
      throw new BadRequestException("Phòng chat không tồn tại");
    }

    List<Member> members = memberRepository.findByRoomId(roomId);
    log.info("<Query> => Result finding members by roomId: {}", gson.toJson(members));

    Map<String, Object> loggedAccount = messageUtils.getAccountLoginInfo();
    boolean flag = members.stream()
        .anyMatch(member -> member.getAccountId().equals(loggedAccount.get("accountId")));

    if (!flag) {
      throw new BadRequestException(
          "Bạn không có quyền lấy ra danh sách tin nhắn của phòng chat này");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer roomId) {
    List<Message> messages = messageRepository.getAllByRoomId(roomId);
    log.info("<Query> => Result getting list message by roomId: {}", gson.toJson(messages));

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách tin nhắn theo phòng chat thành công");
    response.setData(Map.of("messages", genericMapper.mapToListMessageOutputDto(messages)));

    log.info("<Result API> => Getting list message successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
