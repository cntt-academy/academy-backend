package com.act.message.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "history_send_emails")
public class HistorySendEmail {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Size(max = 50)
  @NotNull
  @Column(name = "type", nullable = false, length = 50)
  private String type;

  @Size(max = 50)
  @NotNull
  @Column(name = "send_to", nullable = false, length = 50)
  private String sendTo;

  @Size(max = 50)
  @NotNull
  @Column(name = "email", nullable = false, length = 50)
  private String email;

  @NotNull
  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @NotNull
  @Column(name = "send_time", nullable = false)
  private Date sendTime;

  @Override
  public String toString() {
    return "HistorySendEmail{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", sendTo='" + sendTo + '\'' +
        ", email='" + email + '\'' +
        ", content='" + content + '\'' +
        ", sendTime=" + sendTime +
        '}';
  }
}