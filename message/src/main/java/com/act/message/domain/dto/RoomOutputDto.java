package com.act.message.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.message.domain.entity.Room} entity
 */
@Data
public class RoomOutputDto implements Serializable {

  private Integer id;
  private String name;
  private String type;
  private String friendAvatar;
  private String friendName;
  private String friendEmail;
  private String lastMessage;
  private String sendTime;
  private Boolean isReadLastMessage;
  private String createdDate;
}