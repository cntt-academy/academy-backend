package com.act.message.domain.service.email.implement;

import static com.act.message.Infrastructure.utils.MessageUtils.sdf;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.RedisUtils;
import com.act.message.application.constants.SendToConstants;
import com.act.message.application.request.email.NewsCreatedRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.NewsCreatedService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("NewsCreatedService")
public class NewsCreatedServiceImpl implements NewsCreatedService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(NewsCreatedRequest request) {
    log.info("======== API SEND EMAIL NEWS CREATED ========");
    log.info("Request: {}", gson.toJson(request));
    return NewsCreatedService.super.execute(request);
  }

  @Override
  public void validate(NewsCreatedRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(NewsCreatedRequest request) {
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    helper.setSubject("THÔNG BÁO BÀI VIẾT TIN TỨC MỚI NGÀY " + sdf.format(new Date()));

    String managerEmail = RedisUtils.hget("MANAGER", "EMAIL").toString();
    helper.setTo(managerEmail);

    String type = "News created";
    String sendTo = SendToConstants.MANAGER;
    EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
    if (emailContent == null) {
      throw new ProcessException("Không tìm thấy nội dung email mẫu trong database");
    }

    String content = emailContent.getContent();
    content = content.replace("%time%", request.getTime());
    content = content.replace("%collaborator%", request.getCollaborator());
    content = content.replace("%name%", request.getName());
    message.setContent(content, "text/html; charset=UTF-8");

    javaMailSender.send(message);

    HistorySendEmail history = new HistorySendEmail();
    history.setType(type);
    history.setSendTo(sendTo);
    history.setEmail(managerEmail);
    history.setContent(content);
    history.setSendTime(new Date());
    historySendEmailRepository.save(history);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo bài viết tin tức mới");

    log.info("<Result API> => Sending email notifying news created successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
