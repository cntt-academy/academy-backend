package com.act.message.domain.repository;

import com.act.message.domain.entity.Message;
import java.util.List;

public interface MessageRepository {

  void marKAsReadMessage(Integer roomId, Integer accountId);

  List<Message> getAllByRoomId(Integer roomId);

  void save(Message message);
}
