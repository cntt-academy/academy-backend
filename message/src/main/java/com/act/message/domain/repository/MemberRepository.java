package com.act.message.domain.repository;

import com.act.message.domain.entity.Member;
import java.util.List;

public interface MemberRepository {

  Member findFriend(Integer roomId, Integer accountId);
  List<Member> findByRoomId(Integer roomId);
  void saveAll(List<Member> member);
}
