package com.act.message.domain.service.room.dispatcher;

import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface FindRoomWithFriendService
    extends BaseServiceRequestBody<Integer, ResponseEntity<?>> {

}
