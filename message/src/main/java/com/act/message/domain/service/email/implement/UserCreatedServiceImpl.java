package com.act.message.domain.service.email.implement;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.Infrastructure.utils.RedisUtils;
import com.act.message.application.constants.SendToConstants;
import com.act.message.application.request.email.UserCreatedRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.UserCreatedService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UserCreatedService")
public class UserCreatedServiceImpl implements UserCreatedService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(UserCreatedRequest request) {
    log.info("======== API SEND EMAIL USER CREATED ========");
    log.info("Request: {}", gson.toJson(request));
    return UserCreatedService.super.execute(request);
  }

  @Override
  public void validate(UserCreatedRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(UserCreatedRequest request) {
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    helper.setSubject(
        "THÔNG BÁO THÔNG TIN HỌC VIÊN MỚI NGÀY " + MessageUtils.sdf.format(new Date()));

    String managerEmail = RedisUtils.hget("MANAGER", "EMAIL").toString();
    helper.setTo(managerEmail);

    String sendTo = SendToConstants.MANAGER;
    String type =
        StringUtils.isNotBlank(request.getCollaborator()) ? "User created" : "User sign-up";
    EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);

    if (emailContent == null) {
      throw new ProcessException("Không tìm thấy nội dung email mẫu trong database");
    }

    String content = emailContent.getContent();
    content = content.replace("%time%", request.getTime());
    content = content.replace("%name%", request.getName());
    content = content.replace("%gender%", request.getGender());
    content = content.replace("%dob%", request.getDob());
    content = content.replace("%phone%", request.getPhone());
    content = content.replace("%email%", request.getEmail());
    if (StringUtils.isNotBlank(request.getCollaborator())) {
      content = content.replace("%collaborator%", request.getCollaborator());
    }
    message.setContent(content, "text/html; charset=UTF-8");

    javaMailSender.send(message);

    HistorySendEmail history = new HistorySendEmail();
    history.setType(type);
    history.setSendTo(sendTo);
    history.setEmail(managerEmail);
    history.setContent(content);
    history.setSendTime(new Date());
    historySendEmailRepository.save(history);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo thông tin học viên mới");

    log.info("<Result API> => Sending email notifying create user successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
