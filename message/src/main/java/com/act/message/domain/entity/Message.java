package com.act.message.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "messages")
public class Message {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "room_id", nullable = false)
  private Integer roomId;

  @Column(name = "account_id", nullable = false)
  private Integer accountId;

  @NotNull
  @Column(name = "send_time", nullable = false)
  private Date sendTime;

  @Column(name = "state", nullable = false, length = 10)
  private String state;
}