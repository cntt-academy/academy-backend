package com.act.message.domain.service.email.implement;

import static com.act.message.Infrastructure.utils.MessageUtils.sdf;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.RedisUtils;
import com.act.message.application.constants.SendToConstants;
import com.act.message.application.request.email.EncouragePayTuitionRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.EncouragePayTuitionService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("EncouragePayTuitionService")
public class EncouragePayTuitionServiceImpl implements EncouragePayTuitionService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(EncouragePayTuitionRequest request) {
    log.info("======== API SEND EMAIL ENCOURAGE PAY TUITION ========");
    log.info("Request: {}", gson.toJson(request));
    return EncouragePayTuitionService.super.execute(request);
  }

  @Override
  public void validate(EncouragePayTuitionRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(EncouragePayTuitionRequest request) {
    String type = "Encourage pay tuition";

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject(
            "THÔNG BÁO TƯ VẤN HỌC VIÊN ĐÓNG HỌC PHÍ NGÀY " + sdf.format(new Date()));

        String managerEmail = RedisUtils.hget("MANAGER", "EMAIL").toString();
        helper.setTo(managerEmail);

        String sendTo = SendToConstants.MANAGER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Quản lý trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%time%", request.getTime());
        content = content.replace("%class%", request.getClassName());
        content = content.replace("%name%", request.getStudentName());
        content = content.replace("%email%", request.getStudentEmail());
        content = content.replace("%phone%", request.getStudentPhone());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(managerEmail);
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject(
            "THÔNG BÁO TƯ VẤN HỌC VIÊN ĐÓNG HỌC PHÍ NGÀY " + sdf.format(new Date()));
        helper.setTo(request.getClassManagerEmail());

        String sendTo = SendToConstants.CLASS_MANAGER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Cộng tác viên trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%collaborator%", request.getClassManagerEmail());
        content = content.replace("%class%", request.getClassName());
        content = content.replace("%time%", request.getTime());
        content = content.replace("%name%", request.getStudentName());
        content = content.replace("%email%", request.getStudentEmail());
        content = content.replace("%phone%", request.getStudentPhone());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(request.getClassManagerEmail());
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo thúc đẩy học viên đóng học phí");

    log.info(
        "<Result API> => Sending email notifying encourage student to pay tuition fees successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
