package com.act.message.domain.service.email.implement;

import static com.act.message.Infrastructure.utils.MessageUtils.sdf;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.RedisUtils;
import com.act.message.application.constants.SendToConstants;
import com.act.message.application.request.email.UserAddedIntoClassRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.UserAddedIntoClassService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UserAddedIntoClassService")
public class UserAddedIntoClassServiceImpl implements UserAddedIntoClassService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(UserAddedIntoClassRequest request) {
    log.info("======== API SEND EMAIL USER ADDED INTO CLASS ========");
    log.info("Request: {}", gson.toJson(request));
    return UserAddedIntoClassService.super.execute(request);
  }

  @Override
  public void validate(UserAddedIntoClassRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(UserAddedIntoClassRequest request) {
    String managerEmail = RedisUtils.hget("MANAGER", "EMAIL").toString();
    String type = "User added into class";

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject("THÔNG BÁO THÊM HỌC VIÊN VÀO LỚP HỌC NGÀY " + sdf.format(new Date()));
        helper.setTo(managerEmail);

        String sendTo = SendToConstants.MANAGER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Quản lý trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%time%", request.getTime());
        content = content.replace("%collaborator%", request.getCollaboratorName());
        content = content.replace("%name%", request.getUserName());
        content = content.replace("%email%", request.getUserEmail());
        content = content.replace("%class%", request.getClassName());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(managerEmail);
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    CompletableFuture.runAsync(() -> {
      if (!managerEmail.equalsIgnoreCase(request.getCollaboratorEmail())) {
        try {
          MimeMessage message = javaMailSender.createMimeMessage();
          MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
          helper.setSubject("THÔNG BÁO THÊM HỌC VIÊN VÀO LỚP HỌC NGÀY " + sdf.format(new Date()));
          helper.setTo(request.getCollaboratorEmail());

          String sendTo = SendToConstants.COLLABORATOR;
          EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
          if (emailContent == null) {
            throw new ProcessException(
                "Không tìm thấy nội dung email mẫu gửi cho Cộng tác viên trong database");
          }

          String content = emailContent.getContent();
          content = content.replace("%collaborator%", request.getCollaboratorName());
          content = content.replace("%time%", request.getTime());
          content = content.replace("%name%", request.getUserName());
          content = content.replace("%email%", request.getUserEmail());
          content = content.replace("%class%", request.getClassName());
          message.setContent(content, "text/html; charset=UTF-8");

          javaMailSender.send(message);

          HistorySendEmail history = new HistorySendEmail();
          history.setType(type);
          history.setSendTo(sendTo);
          history.setEmail(request.getCollaboratorEmail());
          history.setContent(content);
          history.setSendTime(new Date());
          historySendEmailRepository.save(history);
        } catch (MessagingException ignored) {
        }
      }
    });

    CompletableFuture.runAsync(() -> {
      if (!request.getCollaboratorEmail().equals(request.getClassManagerEmail())
          && !managerEmail.equalsIgnoreCase(request.getClassManagerEmail())) {
        try {
          MimeMessage message = javaMailSender.createMimeMessage();
          MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
          helper.setSubject("THÔNG BÁO THÔNG TIN HỌC VIÊN MỚI NGÀY " + sdf.format(new Date()));
          helper.setTo(request.getClassManagerEmail());

          String sendTo = SendToConstants.CLASS_MANAGER;
          EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
          if (emailContent == null) {
            throw new ProcessException(
                "Không tìm thấy nội dung email mẫu gửi cho Quản lý lớp trong database");
          }

          String content = emailContent.getContent();
          content = content.replace("%collaborator%", request.getCollaboratorName());
          content = content.replace("%time%", request.getTime());
          content = content.replace("%name%", request.getUserName());
          content = content.replace("%email%", request.getUserEmail());
          content = content.replace("%class%", request.getClassName());
          message.setContent(content, "text/html; charset=UTF-8");

          javaMailSender.send(message);

          HistorySendEmail history = new HistorySendEmail();
          history.setType(type);
          history.setSendTo(sendTo);
          history.setEmail(request.getCollaboratorEmail());
          history.setContent(content);
          history.setSendTime(new Date());
          historySendEmailRepository.save(history);
        } catch (MessagingException ignored) {
        }
      }
    });

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject(
            "THÔNG BÁO HỌC VIÊN ĐƯỢC THÊM VÀO LỚP HỌC NGÀY " + sdf.format(new Date()));
        helper.setTo(request.getUserEmail());

        String sendTo = SendToConstants.USER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Học viên trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%collaborator%", request.getCollaboratorName());
        content = content.replace("%time%", request.getTime());
        content = content.replace("%email%", request.getUserEmail());
        content = content.replace("%class%", request.getClassName());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(request.getUserEmail());
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo thay đổi Quản lý trung tâm");

    log.info("<Result API> => Sending email notifying change manager successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
