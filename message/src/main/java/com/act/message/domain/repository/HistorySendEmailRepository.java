package com.act.message.domain.repository;

import com.act.message.domain.entity.HistorySendEmail;

public interface HistorySendEmailRepository {

  void save(HistorySendEmail historySendEmail);
}
