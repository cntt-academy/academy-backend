package com.act.message.domain.service.email.implement;

import static com.act.message.Infrastructure.utils.MessageUtils.sdf;
import static com.act.message.application.constants.SendToConstants.COLLABORATOR;
import static com.act.message.application.constants.SendToConstants.USER;
import static com.act.message.application.enums.RoleEnums.Collaborator;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.application.request.email.AccountCreatedRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.AccountCreatedService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("AccountCreatedService")
public class AccountCreatedServiceImpl implements AccountCreatedService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(AccountCreatedRequest request) {
    log.info("======== API SEND EMAIL ACCOUNT CREATED ========");
    log.info("Request: {}", gson.toJson(request));
    return AccountCreatedService.super.execute(request);
  }

  @Override
  public void validate(AccountCreatedRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(AccountCreatedRequest request) {
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    helper.setSubject("THÔNG BÁO TẠO TÀI KHOẢN NGÀY " + sdf.format(new Date()));
    helper.setTo(request.getEmail());

    EmailContent emailContent;
    String type = "Account created";
    String sendTo =
        request.getRole().equals(Collaborator.name()) ? COLLABORATOR : USER;
    emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);

    if (emailContent == null) {
      throw new ProcessException("Không tìm thấy nội dung email mẫu trong database");
    }

    String content = emailContent.getContent();
    content = content.replace("%email%", request.getEmail());
    content = content.replace("%time%", request.getTime());
    message.setContent(content, "text/html; charset=UTF-8");

    javaMailSender.send(message);

    HistorySendEmail history = new HistorySendEmail();
    history.setType(type);
    history.setSendTo(sendTo);
    history.setEmail(request.getEmail());
    history.setContent(content);
    history.setSendTime(new Date());
    historySendEmailRepository.save(history);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo tạo tài khoản");

    log.info("<Result API> => Sending email notifying account created successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
