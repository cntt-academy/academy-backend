package com.act.message.domain.service.email.implement;

import static com.act.message.Infrastructure.utils.MessageUtils.sdf;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.application.constants.SendToConstants;
import com.act.message.application.request.email.ChangeManagerRequest;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.EmailContentRepository;
import com.act.message.domain.repository.HistorySendEmailRepository;
import com.act.message.domain.service.email.dispatcher.ChangeManagerService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangeManagerService")
public class ChangeManagerServiceImpl implements ChangeManagerService {

  private final HistorySendEmailRepository historySendEmailRepository;
  private final EmailContentRepository emailContentRepository;
  private final JavaMailSender javaMailSender;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(ChangeManagerRequest request) {
    log.info("======== API SEND EMAIL CHANGE MANAGER ========");
    log.info("Request: {}", gson.toJson(request));
    return ChangeManagerService.super.execute(request);
  }

  @Override
  public void validate(ChangeManagerRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(ChangeManagerRequest request) {
    String type = "Change manager";

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject("THÔNG BÁO THAY ĐỔI QUẢN LÝ TRUNG TÂM NGÀY " + sdf.format(new Date()));
        helper.setTo(request.getOldManager());

        String sendTo = SendToConstants.OLD_MANAGER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Quản lý cũ trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%time%", request.getTime());
        content = content.replace("%email%", request.getOldManager());
        content = content.replace("%newManager%", request.getNewManagerName());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(request.getOldManager());
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    CompletableFuture.runAsync(() -> {
      try {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
        helper.setSubject("THÔNG BÁO THAY ĐỔI QUẢN LÝ TRUNG TÂM NGÀY " + sdf.format(new Date()));
        helper.setTo(request.getNewManager());

        String sendTo = SendToConstants.NEW_MANAGER;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Quản lý mới trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%time%", request.getTime());
        content = content.replace("%email%", request.getNewManager());
        content = content.replace("%newManager%", request.getNewManagerName());
        message.setContent(content, "text/html; charset=UTF-8");

        javaMailSender.send(message);

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setEmail(request.getNewManager());
        history.setContent(content);
        history.setSendTime(new Date());
        historySendEmailRepository.save(history);
      } catch (MessagingException ignored) {
      }
    });

    CompletableFuture.runAsync(() -> {
      try {
        String sendTo = SendToConstants.COLLABORATOR;
        EmailContent emailContent = emailContentRepository.findByTypeAndSendTo(type, sendTo);
        if (emailContent == null) {
          throw new ProcessException(
              "Không tìm thấy nội dung email mẫu gửi cho Cộng tác viên trong database");
        }

        String content = emailContent.getContent();
        content = content.replace("%time%", request.getTime());
        content = content.replace("%newManager%", request.getNewManagerName());

        HistorySendEmail history = new HistorySendEmail();
        history.setType(type);
        history.setSendTo(sendTo);
        history.setContent(content);
        history.setSendTime(new Date());

        MimeMessage message;
        MimeMessageHelper helper;
        for (String item : request.getCollaborators()) {
          message = javaMailSender.createMimeMessage();
          helper = new MimeMessageHelper(message, true, "utf-8");
          helper.setSubject("THÔNG BÁO THAY ĐỔI QUẢN LÝ TRUNG TÂM NGÀY " + sdf.format(new Date()));
          helper.setTo(item);
          message.setContent(content, "text/html; charset=UTF-8");

          javaMailSender.send(message);

          history.setEmail(item);
          historySendEmailRepository.save(history);
        }
      } catch (MessagingException ignored) {
      }
    });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đã gửi email thông báo thay đổi Quản lý trung tâm");

    log.info("<Result API> => Sending email notifying change manager successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
