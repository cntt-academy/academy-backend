package com.act.message.domain.service.room.dispatcher;

import com.act.message.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetListRoomService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
