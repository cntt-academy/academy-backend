package com.act.message.domain.service.message.implement;

import com.act.message.Infrastructure.exception.BadRequestException;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.entity.Member;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.MemberRepository;
import com.act.message.domain.repository.MessageRepository;
import com.act.message.domain.repository.RoomRepository;
import com.act.message.domain.service.message.dispatcher.MarkAsReadMessageService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("MarkAsReadMessageService")
public class MarkAsReadMessageServiceImpl implements MarkAsReadMessageService {

  private final MessageRepository messageRepository;
  private final MemberRepository memberRepository;
  private final RoomRepository roomRepository;
  private final MessageUtils messageUtils;
  private final Gson gson;

  private Map<String, Object> loggedAccount;

  @Override
  public ResponseEntity<?> execute(Integer roomId) {
    log.info("======== API GET LIST MESSAGE ========");
    log.info("Room Id: {}", roomId);
    return MarkAsReadMessageService.super.execute(roomId);
  }

  @Override
  public void validate(Integer roomId) {
    Room room = roomRepository.findById(roomId);
    log.info("<Query> => Result finding room by id: {}", gson.toJson(room));
    if (room == null) {
      throw new BadRequestException("Phòng chat không tồn tại");
    }

    List<Member> members = memberRepository.findByRoomId(roomId);
    log.info("<Query> => Result finding members by roomId: {}", gson.toJson(members));

    loggedAccount = messageUtils.getAccountLoginInfo();
    boolean flag = members.stream()
        .anyMatch(member -> member.getAccountId().equals(loggedAccount.get("accountId")));

    if (!flag) {
      throw new BadRequestException(
          "Bạn không có quyền đánh dấu đã đọc tin nhắn của phòng chat này");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer roomId) {
    messageRepository.marKAsReadMessage(roomId, (Integer) loggedAccount.get("accountId"));

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đánh dấu đã đọc tin nhắn trong nhóm chat thành công");

    log.info("<Result API> => Marking as read message successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
