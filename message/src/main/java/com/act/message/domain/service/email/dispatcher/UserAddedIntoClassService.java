package com.act.message.domain.service.email.dispatcher;

import com.act.message.application.request.email.UserAddedIntoClassRequest;
import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UserAddedIntoClassService extends
    BaseServiceRequestBody<UserAddedIntoClassRequest, ResponseEntity<?>> {

}
