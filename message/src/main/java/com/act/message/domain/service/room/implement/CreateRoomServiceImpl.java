package com.act.message.domain.service.room.implement;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.GenericMapper;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.enums.RoomTypeEnums;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.dto.RoomOutputDto;
import com.act.message.domain.entity.Member;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.MemberRepository;
import com.act.message.domain.repository.RoomRepository;
import com.act.message.domain.service.room.dispatcher.CreateRoomService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateRoomService")
public class CreateRoomServiceImpl implements CreateRoomService {

  private final MemberRepository memberRepository;
  private final RoomRepository roomRepository;
  private final GenericMapper genericMapper;
  private final MessageUtils messageUtils;
  private final Gson gson;

  private Map<String, Object> loggedAccount;

  @Override
  public ResponseEntity<?> execute(Integer friendId) {
    log.info("======== API CREATE ROOM ========");
    log.info("Friend Id: {}", gson.toJson(friendId));
    return CreateRoomService.super.execute(friendId);
  }

  @Override
  public void validate(Integer friendId) {
    loggedAccount = messageUtils.getAccountLoginInfo();
    if (loggedAccount.get("accountId") == friendId) {
      throw new ProcessException("Bạn không được tạo phòng chat với chính mình");
    }

    Map<String, Object> friend = messageUtils.getAccountById(friendId);
    if (friend == null) {
      throw new ProcessException("Bạn chat không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer friendId) {
    Room room =
        roomRepository.findRoomWithFriend((Integer) loggedAccount.get("accountId"), friendId);
    log.info("<Query> => Result find room with friend: {}", gson.toJson(room));
    if (room != null) {
      throw new ProcessException("Đã tồn tại phòng chat");
    }

    room = new Room();
    room.setName("Phòng chat " + System.currentTimeMillis());
    room.setType(RoomTypeEnums.Single.name());
    room.setCreatedDate(new Date());
    roomRepository.save(room);

    List<Member> members = new ArrayList<>();

    Member member = new Member();
    member.setRoomId(room.getId());
    member.setJoinedDate(new Date());
    member.setAccountId((Integer) loggedAccount.get("accountId"));
    members.add(member);

    member = new Member();
    member.setRoomId(room.getId());
    member.setJoinedDate(new Date());
    member.setAccountId(friendId);
    members.add(member);

    memberRepository.saveAll(members);

    RoomOutputDto roomOutputDto = genericMapper.mapToRoomOutputDto(room);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Tạo phòng chat thành công");
    response.setData(Map.of("room", roomOutputDto));

    log.info("<Result API> => Creating room successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
