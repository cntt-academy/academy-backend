package com.act.message.domain.service.message.dispatcher;

import com.act.message.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetListMessageService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
