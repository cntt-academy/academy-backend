package com.act.message.domain.service.email.dispatcher;

import com.act.message.application.request.email.NewsCreatedRequest;
import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface NewsCreatedService extends
    BaseServiceRequestBody<NewsCreatedRequest, ResponseEntity<?>> {

}
