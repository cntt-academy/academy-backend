package com.act.message.domain.service.room.implement;

import com.act.message.Infrastructure.exception.ProcessException;
import com.act.message.Infrastructure.utils.GenericMapper;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.dto.RoomOutputDto;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.RoomRepository;
import com.act.message.domain.service.room.dispatcher.CreateRoomService;
import com.act.message.domain.service.room.dispatcher.FindRoomWithFriendService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("FindRoomWithAccountService")
public class FindRoomWithFriendServiceImpl implements FindRoomWithFriendService {

  private final CreateRoomService createRoomService;
  private final RoomRepository roomRepository;
  private final GenericMapper genericMapper;
  private final MessageUtils messageUtils;
  private final Gson gson;

  private Map<String, Object> loggedAccount;

  @Override
  public ResponseEntity<?> execute(Integer friendId) {
    log.info("======== API FIND ROOM WITH FRIEND ========");
    log.info("friendId Id: {}", friendId);
    return FindRoomWithFriendService.super.execute(friendId);
  }

  @Override
  public void validate(Integer friendId) {
    loggedAccount = messageUtils.getAccountLoginInfo();
    if (loggedAccount.get("accountId") == friendId) {
      throw new ProcessException("Không tồn tại phòng chat với chính bản thân");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer friendId) {
    Room room =
        roomRepository.findRoomWithFriend((Integer) loggedAccount.get("accountId"), friendId);
    log.info("<Query> => Result find room with friend: {}", gson.toJson(room));

    RoomOutputDto roomOutputDto;
    if (room == null) {
      ResponseEntity<?> response = createRoomService.execute(friendId);
      if (response.getStatusCodeValue() != 200 || response.getBody() == null) {
        throw new ProcessException("Tạo phòng chat thất bại");
      }
      JsonResponseBase<?> body = (JsonResponseBase<?>) response.getBody();
      Map<?, ?> data = (Map<?, ?>) body.getData();
      roomOutputDto = (RoomOutputDto) data.get("room");
    } else {
      Map<String, Object> friendInfo = messageUtils.getAccountById(friendId);
      room.setFriendAvatar((String) friendInfo.get("avatar"));
      room.setFriendName((String) friendInfo.get("name"));
      room.setFriendEmail((String) friendInfo.get("email"));
      roomOutputDto = genericMapper.mapToRoomOutputDto(room);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Tạo phòng chat thành công");
    response.setData(Map.of("room", roomOutputDto));

    log.info("<Result API> => Finding room with friend successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
