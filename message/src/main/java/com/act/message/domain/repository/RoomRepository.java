package com.act.message.domain.repository;

import com.act.message.domain.entity.Room;
import java.util.List;

public interface RoomRepository {

  Room findRoomWithFriend(Integer accountId, Integer friendId);

  List<Room> findAllByAccountId(Integer accountId);

  Room findById(Integer roomId);

  void save(Room room);
}
