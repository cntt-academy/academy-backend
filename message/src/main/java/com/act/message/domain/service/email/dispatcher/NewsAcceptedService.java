package com.act.message.domain.service.email.dispatcher;

import com.act.message.application.request.email.NewsAcceptedRequest;
import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface NewsAcceptedService extends
    BaseServiceRequestBody<NewsAcceptedRequest, ResponseEntity<?>> {

}
