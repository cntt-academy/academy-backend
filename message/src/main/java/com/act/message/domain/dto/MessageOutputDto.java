package com.act.message.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.message.domain.entity.Message} entity
 */
@Data
public class MessageOutputDto implements Serializable {

  private Integer id;
  private String content;
  private Integer roomId;
  private Integer accountId;
  private String accountName;
  private String sendTime;
}