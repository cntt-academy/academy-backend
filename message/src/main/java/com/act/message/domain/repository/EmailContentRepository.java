package com.act.message.domain.repository;

import com.act.message.domain.entity.EmailContent;

public interface EmailContentRepository {

  EmailContent findByTypeAndSendTo(String type, String sendTo);
}
