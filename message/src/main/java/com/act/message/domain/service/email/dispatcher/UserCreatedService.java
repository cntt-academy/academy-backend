package com.act.message.domain.service.email.dispatcher;

import com.act.message.application.request.email.UserCreatedRequest;
import com.act.message.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UserCreatedService extends
    BaseServiceRequestBody<UserCreatedRequest, ResponseEntity<?>> {

}
