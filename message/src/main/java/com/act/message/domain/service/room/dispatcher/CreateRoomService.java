package com.act.message.domain.service.room.dispatcher;

import com.act.message.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface CreateRoomService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
