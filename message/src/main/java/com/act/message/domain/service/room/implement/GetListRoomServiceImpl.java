package com.act.message.domain.service.room.implement;

import com.act.message.Infrastructure.utils.GenericMapper;
import com.act.message.Infrastructure.utils.MessageUtils;
import com.act.message.application.enums.MessageStateEnums;
import com.act.message.application.response.JsonResponseBase;
import com.act.message.domain.dto.RoomOutputDto;
import com.act.message.domain.entity.Member;
import com.act.message.domain.entity.Message;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.MemberRepository;
import com.act.message.domain.repository.MessageRepository;
import com.act.message.domain.repository.RoomRepository;
import com.act.message.domain.service.room.dispatcher.GetListRoomService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("GetListRoomService")
public class GetListRoomServiceImpl implements GetListRoomService {

  private final MessageRepository messageRepository;
  private final MemberRepository memberRepository;
  private final RoomRepository roomRepository;

  private final GenericMapper genericMapper;
  private final MessageUtils messageUtils;
  private final Gson gson;

  private Map<String, Object> loggedAccount;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET LIST ROOM ========");

    loggedAccount = messageUtils.getAccountLoginInfo();
    List<Room> rooms = roomRepository.findAllByAccountId((Integer) loggedAccount.get("accountId"));
    log.info("<Query> => Result getting list room by accountId: {}", gson.toJson(rooms));

    List<Room> tmpRooms = setFriendAndLassMessageInfo(rooms);
    List<RoomOutputDto> roomOutputDtos = genericMapper.mapToListRoomOutputDto(tmpRooms);

    Map<String, Object> data =
        Map.of("rooms", roomOutputDtos);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách phòng chat thành công");
    response.setData(data);

    log.info("<Result API> => Getting list room successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  private List<Room> setFriendAndLassMessageInfo(List<Room> rooms) {
    Member friend;
    Message lastMessage;
    Map<String, Object> friendInfo;
    List<Room> modifiedRooms = new ArrayList<>();

    for (Room room : rooms) {
      lastMessage = getLastMessageByRoomId(room.getId());
      log.info("<Query> => Result getting last message by roomId: {}", gson.toJson(lastMessage));

      if (lastMessage != null) {
        room.setLastMessage(lastMessage.getContent());
        room.setSendTime(lastMessage.getSendTime());
        room.setIsReadLastMessage(
            MessageStateEnums.READ.name().equals(lastMessage.getState())
                || lastMessage.getAccountId() == loggedAccount.get("accountId"));

        friend =
            memberRepository.findFriend(room.getId(), (Integer) loggedAccount.get("accountId"));
        friendInfo = messageUtils.getAccountById(friend.getAccountId());
        room.setFriendAvatar((String) friendInfo.get("avatar"));
        room.setFriendName((String) friendInfo.get("name"));
        room.setFriendEmail((String) friendInfo.get("email"));

        modifiedRooms.add(room);
      }
    }
    return modifiedRooms;
  }

  private Message getLastMessageByRoomId(Integer roomId) {
    List<Message> messages = messageRepository.getAllByRoomId(roomId);
    if (!CollectionUtils.isEmpty(messages)) {
      return messages.get(messages.size() - 1);
    }
    return null;
  }
}
