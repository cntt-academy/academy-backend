package com.act.message.Infrastructure.persistance.impl;

import com.act.message.Infrastructure.persistance.jpa.MessageJpaRepository;
import com.act.message.application.enums.MessageStateEnums;
import com.act.message.domain.entity.Message;
import com.act.message.domain.repository.MessageRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("MessageRepository")
public class MessageRepositoryImpl implements MessageRepository {

  private final MessageJpaRepository messageJpaRepository;

  @Override
  public void marKAsReadMessage(Integer roomId, Integer accountId) {
    messageJpaRepository.marKAsReadMessage(MessageStateEnums.READ.name(), roomId, accountId);
  }

  @Override
  public List<Message> getAllByRoomId(Integer roomId) {
    return messageJpaRepository.findAllByRoomIdOrderBySendTime(roomId);
  }

  @Override
  public void save(Message message) {
    messageJpaRepository.save(message);
  }
}
