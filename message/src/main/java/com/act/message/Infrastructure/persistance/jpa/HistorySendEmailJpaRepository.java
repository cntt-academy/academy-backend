package com.act.message.Infrastructure.persistance.jpa;

import com.act.message.domain.entity.HistorySendEmail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistorySendEmailJpaRepository extends JpaRepository<HistorySendEmail, Integer> {

}