package com.act.message.Infrastructure.persistance.jpa;

import com.act.message.domain.entity.Room;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoomJpaRepository extends JpaRepository<Room, Integer> {

  @Query(value =
      " SELECT DISTINCT(r) FROM Room r "
          + " JOIN Member m ON r.id = m.roomId "
          + " JOIN Message ms ON r.id = ms.roomId "
          + " WHERE m.accountId = :accountId "
          + " ORDER BY ms.sendTime ")
  List<Room> findAllByAccountId(@Param(value = "accountId") Integer accountId);

  @Query(value =
      "SELECT r FROM Room r"
          + " JOIN Member m1 ON r.id = m1.roomId "
          + " JOIN Member m2 ON r.id = m2.roomId "
          + " WHERE m1.accountId = :accountId "
          + " AND m2.accountId = :friendId ")
  Optional<Room> findRoomWithFriend(
      @Param(value = "accountId") Integer accountId,
      @Param(value = "friendId") Integer friendId);
}