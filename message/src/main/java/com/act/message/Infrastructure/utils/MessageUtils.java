package com.act.message.Infrastructure.utils;

import com.act.message.Infrastructure.filter.UserInfo;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("MessageUtils")
public class MessageUtils {

  public static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  private final EurekaClient eurekaClient;
  private final AESUtils aesUtils;

  @Value("${path.getAccountById}")
  private String pathGetAccountById;

  public String formatSendTimeMessage(Date sendTime) {
    DateFormat df1 = new SimpleDateFormat("hh:mm a");
    DateFormat df2 = new SimpleDateFormat("dd 'thg' MM");
    DateFormat df3 = new SimpleDateFormat("dd 'thg' MM',' yyyy");

    String strSendTime;
    if (isToday(sendTime)) {
      strSendTime = df1.format(sendTime);
    } else {
      int currentYear = getYearFromDate(new Date());
      int yearSend = getYearFromDate(sendTime);
      if (currentYear == yearSend) {
        strSendTime = df2.format(sendTime);
      } else {
        strSendTime = df3.format(sendTime);
      }
    }
    return strSendTime;
  }

  public boolean isToday(Date date) {
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    String strToday = dateFormat.format(new Date());
    String strDate = dateFormat.format(date);
    return strToday.equals(strDate);
  }

  public int getYearFromDate(Date date) {
    DateFormat yearFormat = new SimpleDateFormat("yyyy");
    return Integer.parseInt(yearFormat.format(date));
  }

  /**
   * Lấy ra thông tin Account đang login
   */
  public Map<String, Object> getAccountLoginInfo() {
    log.info("<AdminUtils> => Function getAdminLoginInfo");

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserInfo userInfo = (UserInfo) authentication.getDetails();

    Integer accountId = userInfo.getAccountId();
    log.info("Account id: {}", accountId);
    String email = userInfo.getEmail();
    log.info("Email: {}", email);
    String name = userInfo.getName();
    log.info("Name: {}", name);
    String role = userInfo.getRole();
    log.info("Role: {}", role);

    return Map.of("accountId", accountId, "email", email, "name", name, "role", role);
  }

  /**
   * Lấy ra thông tin Account theo id
   */
  public Map<String, Object> getAccountById(Integer accountId) {
    Map<String, Object> parameters =
        Map.of("accountId", aesUtils.encrypt(String.valueOf(accountId)));

    String url =
        eurekaClient.getNextServerFromEureka("authentication", false).getHomePageUrl()
            + "internal/account" + pathGetAccountById;
    JsonObject resp = UnirestUtils.getWithoutHeader(url, parameters);

    log.info("Url: {}", url);
    log.info("Account Id: {}", accountId);
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
      JsonObject account = resp.get("data").getAsJsonObject().get("account").getAsJsonObject();
      Map<String, Object> response = new HashMap<>();
      response.put("id", account.get("accountId").getAsInt());
      response.put("avatar", account.get("avatar").getAsString());
      response.put("name", account.get("name").getAsString());
      response.put("email", account.get("email").getAsString());
      return response;
    }
    return null;
  }
}
