package com.act.message.Infrastructure.persistance.impl;

import com.act.message.Infrastructure.persistance.jpa.EmailContentJpaRepository;
import com.act.message.application.enums.StateEnums;
import com.act.message.domain.entity.EmailContent;
import com.act.message.domain.repository.EmailContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("EmailContentRepository")
public class EmailContentRepositoryImpl implements EmailContentRepository {

  private final EmailContentJpaRepository emailContentJpaRepository;

  @Override
  public EmailContent findByTypeAndSendTo(String type, String sendTo) {
    return emailContentJpaRepository
        .findByTypeAndSendToAndState(type, sendTo, StateEnums.ACTIVE.name())
        .orElse(null);
  }
}
