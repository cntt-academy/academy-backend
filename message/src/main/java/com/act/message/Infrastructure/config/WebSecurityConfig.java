package com.act.message.Infrastructure.config;

import com.act.message.Infrastructure.filter.TokenFilter;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final EurekaClient eurekaClient;
  private final Gson gson;
  @Value("${path.verifyToken}")
  private String pathVerifyToken;

  @Bean
  public RedirectStrategy redirectStrategy() {
    return new DefaultRedirectStrategy();
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring().antMatchers("/email/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers("/email/**")
        .permitAll() // Cho phép tất cả mọi người truy cập vào địa chỉ này
        .anyRequest()
        .authenticated()
        .and()
        .addFilterBefore(
            new TokenFilter(eurekaClient, pathVerifyToken, gson),
            UsernamePasswordAuthenticationFilter.class)
        .cors()// Ngăn chặn request từ một domain khác
        .and().csrf().disable(); // Tất cả các request khác đều cần phải xác thực mới được truy cập
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.headers().cacheControl();
  }
}
