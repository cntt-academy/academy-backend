package com.act.message.Infrastructure.exception;

public class ProcessException extends RuntimeException {

  public ProcessException(String message) {
    super(message);
  }
}
