package com.act.message.Infrastructure.persistance.impl;

import com.act.message.Infrastructure.persistance.jpa.MemberJpaRepository;
import com.act.message.domain.entity.Member;
import com.act.message.domain.repository.MemberRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("MemberRepository")
public class MemberRepositoryImpl implements MemberRepository {

  private final MemberJpaRepository memberJpaRepository;

  @Override
  public Member findFriend(Integer roomId, Integer accountId) {
    return memberJpaRepository.findFriend(roomId, accountId).orElse(null);
  }

  @Override
  public List<Member> findByRoomId(Integer roomId) {
    return memberJpaRepository.findAllByRoomId(roomId);
  }

  @Override
  public void saveAll(List<Member> member) {
    memberJpaRepository.saveAll(member);
  }
}
