package com.act.message.Infrastructure.persistance.jpa;

import com.act.message.domain.entity.EmailContent;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailContentJpaRepository extends JpaRepository<EmailContent, Integer> {

  Optional<EmailContent> findByTypeAndSendToAndState(String type, String sendTo, String state);
}