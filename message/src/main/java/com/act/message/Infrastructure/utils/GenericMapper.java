package com.act.message.Infrastructure.utils;

import com.act.message.domain.dto.MessageOutputDto;
import com.act.message.domain.dto.RoomOutputDto;
import com.act.message.domain.entity.Message;
import com.act.message.domain.entity.Room;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
@RequiredArgsConstructor
public class GenericMapper {

  private final ModelMapper modelMapper;
  private final MessageUtils messageUtils;

  public <T, E> E mapToType(T source, Class<E> typeDestination) {
    if (source == null) {
      return null;
    }
    return modelMapper.map(source, typeDestination);
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (source == null || source.isEmpty()) {
      return null;
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }

  public void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  public String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  public RoomOutputDto mapToRoomOutputDto(Room room) {
    RoomOutputDto roomOutputDto = mapToType(room, RoomOutputDto.class);
    if (roomOutputDto != null && StringUtils.isNotBlank(room.getLastMessage())) {
      roomOutputDto.setSendTime(messageUtils.formatSendTimeMessage(room.getSendTime()));
    }
    return roomOutputDto;
  }

  public List<RoomOutputDto> mapToListRoomOutputDto(List<Room> rooms) {
    if (CollectionUtils.isEmpty(rooms)) {
      return new ArrayList<>();
    }
    return rooms.stream()
        .map(this::mapToRoomOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  @SneakyThrows
  public MessageOutputDto mapToMessageOutputDto(Message message) {
    MessageOutputDto messageOutputDto = mapToType(message, MessageOutputDto.class);
    if (messageOutputDto != null) {
      CompletableFuture<Map<String, Object>> accountInfo =
          CompletableFuture.supplyAsync(() -> messageUtils.getAccountById(message.getAccountId()));
      messageOutputDto.setAccountName((String) accountInfo.get().get("name"));
      messageOutputDto.setSendTime(messageUtils.formatSendTimeMessage(message.getSendTime()));
    }
    return messageOutputDto;
  }

  public List<MessageOutputDto> mapToListMessageOutputDto(List<Message> messages) {
    if (CollectionUtils.isEmpty(messages)) {
      return new ArrayList<>();
    }
    return messages.stream()
        .map(this::mapToMessageOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}
