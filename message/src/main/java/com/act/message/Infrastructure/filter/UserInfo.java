package com.act.message.Infrastructure.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

  private Integer accountId;
  private String email;
  private String name;
  private String role;
}
