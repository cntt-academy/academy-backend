package com.act.message.Infrastructure.persistance.jpa;

import com.act.message.domain.entity.Member;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MemberJpaRepository extends JpaRepository<Member, Integer> {

  @Query(value = "SELECT m FROM Member m WHERE m.roomId = :roomId AND m.accountId <> :accountId")
  Optional<Member> findFriend(
      @Param(value = "roomId") Integer roomId,
      @Param(value = "accountId") Integer accountId);

  List<Member> findAllByRoomId(Integer roomId);
}