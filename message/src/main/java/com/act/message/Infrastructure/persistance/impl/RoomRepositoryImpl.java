package com.act.message.Infrastructure.persistance.impl;

import com.act.message.Infrastructure.persistance.jpa.RoomJpaRepository;
import com.act.message.domain.entity.Room;
import com.act.message.domain.repository.RoomRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("RoomRepository")
public class RoomRepositoryImpl implements RoomRepository {

  private final RoomJpaRepository roomJpaRepository;

  @Override
  public Room findRoomWithFriend(Integer accountId, Integer friendId) {
    return roomJpaRepository.findRoomWithFriend(accountId, friendId).orElse(null);
  }

  @Override
  public List<Room> findAllByAccountId(Integer accountId) {
    return roomJpaRepository.findAllByAccountId(accountId);
  }

  @Override
  public Room findById(Integer roomId) {
    return roomJpaRepository.findById(roomId).orElse(null);
  }

  @Override
  public void save(Room room) {
    roomJpaRepository.save(room);
  }
}
