package com.act.message.Infrastructure.persistance.jpa;

import com.act.message.domain.entity.Message;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface MessageJpaRepository extends JpaRepository<Message, Integer> {

  List<Message> findAllByRoomIdOrderBySendTime(Integer roomId);

  @Modifying
  @SneakyThrows
  @Transactional
  @Query(value =
      " UPDATE Message m SET m.state = :state "
          + " WHERE m.roomId = :roomId AND m.accountId <> :accountId ")
  void marKAsReadMessage(
      @Param(value = "state") String state,
      @Param(value = "roomId") Integer roomId,
      @Param(value = "accountId") Integer accountId);
}