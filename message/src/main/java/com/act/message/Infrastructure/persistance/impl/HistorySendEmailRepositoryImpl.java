package com.act.message.Infrastructure.persistance.impl;

import com.act.message.Infrastructure.persistance.jpa.HistorySendEmailJpaRepository;
import com.act.message.domain.entity.HistorySendEmail;
import com.act.message.domain.repository.HistorySendEmailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("HistorySendEmailRepository")
public class HistorySendEmailRepositoryImpl implements HistorySendEmailRepository {

  private final HistorySendEmailJpaRepository historySendEmailJpaRepository;

  @Override
  public void save(HistorySendEmail historySendEmail) {
    historySendEmailJpaRepository.save(historySendEmail);
  }
}
