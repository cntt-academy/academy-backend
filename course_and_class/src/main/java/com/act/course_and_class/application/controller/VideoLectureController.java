package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.videoLecture.CreateVideoLectureRequest;
import com.act.course_and_class.application.request.videoLecture.GetListVideoLectureRequest;
import com.act.course_and_class.application.request.videoLecture.UpdateVideoLectureRequest;
import com.act.course_and_class.domain.service.historyWatch.dispatcher.AddHistoryWatchService;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.CreateVideoLectureService;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.DeleteVideoLectureService;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.GetDetailVideoLectureService;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.GetListVideoLectureService;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.UpdateVideoLectureService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("VideoLectureController")
@RequestMapping(value = "/video-lecture", produces = MediaType.APPLICATION_JSON_VALUE)
public class VideoLectureController {

  private final GetDetailVideoLectureService getDetailVideoLectureService;
  private final GetListVideoLectureService getListVideoLectureService;
  private final CreateVideoLectureService createVideoLectureService;
  private final UpdateVideoLectureService updateVideoLectureService;
  private final DeleteVideoLectureService deleteVideoLectureService;
  private final AddHistoryWatchService addHistoryWatchService;

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer videoLectureId) {
    return getDetailVideoLectureService.execute(videoLectureId);
  }

  @GetMapping(value = "/addHistoryWatch")
  public Object addHistoryWatch(@RequestParam Integer videoLectureId) {
    return addHistoryWatchService.execute(videoLectureId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListVideoLectureRequest request) {
    return getListVideoLectureService.execute(request);
  }

  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateVideoLectureRequest request) {
    return createVideoLectureService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateVideoLectureRequest request) {
    return updateVideoLectureService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer videoLectureId) {
    return deleteVideoLectureService.execute(videoLectureId);
  }
}
