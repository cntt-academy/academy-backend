package com.act.course_and_class.application.request.videoLecture;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateVideoLectureRequest implements Serializable {

  @NotBlank(message = "Tên bài giảng")
  private String name;

  @NotBlank(message = "Mô tả bài giảng")
  private String desc;

  @NotBlank(message = "Đường dẫn")
  private String url;

  @NotNull(message = "Id lớp học")
  private Integer classId;
}