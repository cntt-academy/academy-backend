package com.act.course_and_class.application.request.videoLecture;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListVideoLectureRequest implements Serializable {

  private String name;

  @NotNull(message = "Id lớp học")
  private Integer classId;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
