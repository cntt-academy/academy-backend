package com.act.course_and_class.application.request.typeCourse;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateTypeCourseRequest implements Serializable {

  @NotBlank(message = "Tên loại khóa học")
  private String name;

  @NotBlank(message = "Mô tả")
  private String desc;
}