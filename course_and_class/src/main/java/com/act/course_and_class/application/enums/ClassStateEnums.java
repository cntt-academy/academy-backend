package com.act.course_and_class.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClassStateEnums {
  PREP("Dự bị"),
  WAIT("Chờ khai giảng"),
  DOING("Đang diễn ra"),
  FINISHED("Đã kết thúc"),
  DELETED("Đã Xóa");
  private final String value;
}