package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.teacher.CreateTeacherRequest;
import com.act.course_and_class.application.request.teacher.GetListTeacherRequest;
import com.act.course_and_class.application.request.teacher.UpdateTeacherRequest;
import com.act.course_and_class.domain.service.teacher.dispatcher.CreateTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.DeleteTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetAllTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetListTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetTeacherInfoService;
import com.act.course_and_class.domain.service.teacher.dispatcher.UpdateTeacherService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("TeacherController")
@RequestMapping(value = "/teacher", produces = MediaType.APPLICATION_JSON_VALUE)
public class TeacherController {

  private final GetListTeacherService getListTeacherService;
  private final GetTeacherInfoService getTeacherInfoService;
  private final GetAllTeacherService getAllTeacherService;
  private final CreateTeacherService createTeacherService;
  private final UpdateTeacherService updateTeacherService;
  private final DeleteTeacherService deleteTeacherService;

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListTeacherRequest request) {
    return getListTeacherService.execute(request);
  }

  @GetMapping(value = "/getInfo")
  public Object getInfo(@Valid @RequestParam String teacherId) {
    return getTeacherInfoService.execute(teacherId);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllTeacherService.execute();
  }


  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateTeacherRequest request) {
    return createTeacherService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateTeacherRequest request) {
    return updateTeacherService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@Valid @RequestParam String teacherId) {
    return deleteTeacherService.execute(teacherId);
  }
}
