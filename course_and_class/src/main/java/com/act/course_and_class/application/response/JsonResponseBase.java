package com.act.course_and_class.application.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class JsonResponseBase<T> implements Serializable {

  private String timestamp;
  private Integer status;
  private String message;
  private T data;

  public JsonResponseBase() {
    timestamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
  }
}
