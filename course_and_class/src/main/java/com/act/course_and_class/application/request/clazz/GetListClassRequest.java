package com.act.course_and_class.application.request.clazz;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListClassRequest implements Serializable {

  private String search;

  private String state;
  private String stateNot;

  private Integer userId;
  private String adminEmail;
  private Integer teacherId;

  private String howToLearn;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
