package com.act.course_and_class.application.request.teacher;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListTeacherRequest implements Serializable {

  private String search;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
