package com.act.course_and_class.application.request.typeCourse;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListTypeCourseRequest implements Serializable {

  private String name;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
