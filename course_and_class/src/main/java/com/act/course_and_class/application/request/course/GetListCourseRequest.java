package com.act.course_and_class.application.request.course;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListCourseRequest implements Serializable {

  private String name;

  private Integer typeId;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
