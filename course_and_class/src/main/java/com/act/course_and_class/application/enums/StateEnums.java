package com.act.course_and_class.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateEnums {
  ACTIVE("Hoạt động"),
  DELETED("Xóa");
  private final String value;
}