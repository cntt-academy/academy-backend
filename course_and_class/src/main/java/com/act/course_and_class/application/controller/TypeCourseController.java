package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.typeCourse.CreateTypeCourseRequest;
import com.act.course_and_class.application.request.typeCourse.GetListTypeCourseRequest;
import com.act.course_and_class.application.request.typeCourse.UpdateTypeCourseRequest;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.CreateTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.DeleteTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetAllTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetDetailTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetListTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.UpdateTypeCourseService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("TypeCourseController")
@RequestMapping(value = "/type-course", produces = MediaType.APPLICATION_JSON_VALUE)
public class TypeCourseController {

  private final GetDetailTypeCourseService getDetailTypeCourseService;
  private final GetListTypeCourseService getListTypeCourseService;
  private final GetAllTypeCourseService getAllTypeCourseService;
  private final CreateTypeCourseService createTypeCourseService;
  private final UpdateTypeCourseService updateTypeCourseService;
  private final DeleteTypeCourseService deleteTypeCourseService;

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer typeCourseId) {
    return getDetailTypeCourseService.execute(typeCourseId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListTypeCourseRequest request) {
    return getListTypeCourseService.execute(request);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllTypeCourseService.execute();
  }


  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateTypeCourseRequest request) {
    return createTypeCourseService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateTypeCourseRequest request) {
    return updateTypeCourseService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer typeCourseId) {
    return deleteTypeCourseService.execute(typeCourseId);
  }
}
