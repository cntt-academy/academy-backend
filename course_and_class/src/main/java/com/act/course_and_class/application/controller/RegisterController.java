package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.register.CountTotalStudentRequest;
import com.act.course_and_class.application.request.register.DetailStudentStatisticRequest;
import com.act.course_and_class.application.request.register.GetListRegisterRequest;
import com.act.course_and_class.application.request.register.OverviewStudentStatisticRequest;
import com.act.course_and_class.application.request.register.RegisterRequest;
import com.act.course_and_class.application.request.register.UpdateStateRegisterRequest;
import com.act.course_and_class.domain.service.register.dispatcher.CountTotalStudentService;
import com.act.course_and_class.domain.service.register.dispatcher.DeleteRegisterService;
import com.act.course_and_class.domain.service.register.dispatcher.DetailStudentStatisticService;
import com.act.course_and_class.domain.service.register.dispatcher.GetListRegisterService;
import com.act.course_and_class.domain.service.register.dispatcher.OverviewStudentStatisticService;
import com.act.course_and_class.domain.service.register.dispatcher.RegisterService;
import com.act.course_and_class.domain.service.register.dispatcher.UpdateStateRegisterService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("RegisterController")
@RequestMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegisterController {

  private final OverviewStudentStatisticService overviewStudentStatisticService;
  private final DetailStudentStatisticService detailStudentStatisticService;
  private final UpdateStateRegisterService updateStateRegisterService;
  private final CountTotalStudentService countTotalStudentService;
  private final GetListRegisterService getListRegisterService;
  private final DeleteRegisterService deleteRegisterService;
  private final RegisterService registerService;

  @PostMapping("/overviewStatistic")
  public Object overviewStatistic(@Valid @RequestBody OverviewStudentStatisticRequest request) {
    return overviewStudentStatisticService.execute(request);
  }

  @PostMapping("/detailStatistic")
  public Object detailStatistic(@Valid @RequestBody DetailStudentStatisticRequest request) {
    return detailStudentStatisticService.execute(request);
  }

  @PostMapping("/countTotal")
  public Object countTotal(@Valid @RequestBody CountTotalStudentRequest request) {
    return countTotalStudentService.execute(request);
  }

  @PostMapping("/getList")
  public Object register(@Valid @RequestBody GetListRegisterRequest request) {
    return getListRegisterService.execute(request);
  }

  @PostMapping("/registerStudy")
  public Object register(@Valid @RequestBody RegisterRequest request) {
    return registerService.execute(request);
  }

  @PutMapping(value = "/updateState")
  public Object updateState(@Valid @RequestBody UpdateStateRegisterRequest request) {
    return updateStateRegisterService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer registerId) {
    return deleteRegisterService.execute(registerId);
  }
}
