package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.clazz.CheckClassExistsRequest;
import com.act.course_and_class.application.request.clazz.CreateClassRequest;
import com.act.course_and_class.application.request.clazz.GetListClassRequest;
import com.act.course_and_class.application.request.clazz.UpdateClassRequest;
import com.act.course_and_class.domain.service.clazz.dispatcher.CheckClassExistsService;
import com.act.course_and_class.domain.service.clazz.dispatcher.CreateClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.DeleteClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetAllClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetDetailClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetListClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.UpdateClassService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("ClassController")
@RequestMapping(value = "/class", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClassController {

  private final CheckClassExistsService checkClassExistsService;
  private final GetDetailClassService getDetailClassService;
  private final GetListClassService getListClassService;
  private final GetAllClassService getAllClassService;
  private final CreateClassService createClassService;
  private final UpdateClassService updateClassService;
  private final DeleteClassService deleteClassService;

  @PostMapping(value = "/checkExists")
  public Object checkExists(@Valid @RequestBody CheckClassExistsRequest request) {
    return checkClassExistsService.execute(request);
  }

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer classId) {
    return getDetailClassService.execute(classId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListClassRequest request) {
    return getListClassService.execute(request);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllClassService.execute();
  }

  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateClassRequest request) {
    return createClassService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateClassRequest request) {
    return updateClassService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer classId) {
    return deleteClassService.execute(classId);
  }
}
