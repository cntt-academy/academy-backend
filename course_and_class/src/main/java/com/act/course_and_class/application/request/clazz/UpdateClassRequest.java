package com.act.course_and_class.application.request.clazz;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateClassRequest implements Serializable {

  @NotNull(message = "Id lớp học")
  private Integer id;

  private String fullName;
  private String name;
  private String howToLearn;
  private String urlClass;
  private Integer numberStudent;
  private String dateOpening;
  private Integer teacherId;
  private String managerEmail;
  private String supporterEmail;
  private Integer courseId;
  private String special;
  private String state;
}
