package com.act.course_and_class.application.request.teacher;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateTeacherRequest implements Serializable {

  @NotBlank(message = "Ảnh đại diện")
  private String avatar;

  @NotBlank(message = "Họ và tên")
  private String name;

  @NotBlank(message = "Giới tính")
  private String gender;

  @NotBlank(message = "Số điện thoại")
  private String phone;

  @NotBlank(message = "Email")
  private String email;
}