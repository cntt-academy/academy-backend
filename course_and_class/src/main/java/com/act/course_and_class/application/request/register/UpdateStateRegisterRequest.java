package com.act.course_and_class.application.request.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateStateRegisterRequest implements Serializable {

  @NotNull(message = "Id đăng ký")
  private Integer registerId;

  @NotBlank(message = "Trạng thái mới")
  private String state;
}
