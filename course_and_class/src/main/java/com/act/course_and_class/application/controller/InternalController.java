package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.register.DeleteAllRegisterRequest;
import com.act.course_and_class.domain.service.internal.dispatcher.DeleteAllRegisterService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("InternalController")
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalController {

  private final DeleteAllRegisterService deleteAllRegisterService;

  @DeleteMapping(value = "/register/deleteAll")
  public Object deleteAllByStudentId(@Valid @RequestBody DeleteAllRegisterRequest request) {
    return deleteAllRegisterService.execute(request);
  }
}
