package com.act.course_and_class.application.request.historyWatch;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddHistoryWatchRequest implements Serializable {

  @NotNull(message = "Id bài giảng")
  private Integer videoLectureId;
}
