package com.act.course_and_class.application.request.register;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DeleteAllRegisterRequest implements Serializable {

  @NotNull(message = "Id học viên")
  private Integer studentId;

  @NotBlank(message = "Người xóa")
  private String deletedBy;
}
