package com.act.course_and_class.application.request.clazz;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckClassExistsRequest implements Serializable {

  private Integer classId;

  private Integer courseId;

  private String howToLearn;
}
