package com.act.course_and_class.application.request.course;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateCourseRequest implements Serializable {

  @NotNull(message = "Id khóa học")
  private Integer id;

  private String img;

  private String name;

  private String desc;

  private String content;

  private Double price;

  private Integer totalHourStudy;

  private Integer typeId;
}
