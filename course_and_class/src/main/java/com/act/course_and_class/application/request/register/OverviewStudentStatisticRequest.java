package com.act.course_and_class.application.request.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OverviewStudentStatisticRequest implements Serializable {

  private String fromDate;

  private String toDate;
}
