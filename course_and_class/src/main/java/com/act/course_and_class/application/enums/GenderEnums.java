package com.act.course_and_class.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenderEnums {
  Male("Nam"),
  Female("Nữ");
  private final String value;
}
