package com.act.course_and_class.application.request.clazz;

import com.act.course_and_class.domain.entity.Course;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreatePrepClassRequest implements Serializable {

  @NotNull(message = "Khóa học")
  private Course course;
}
