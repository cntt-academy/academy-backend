package com.act.course_and_class.application.enums;

public enum HowToLearnEnums {
  Offline,
  Online
}
