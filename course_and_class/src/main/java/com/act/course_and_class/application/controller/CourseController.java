package com.act.course_and_class.application.controller;

import com.act.course_and_class.application.request.course.CreateCourseRequest;
import com.act.course_and_class.application.request.course.GetListCourseRequest;
import com.act.course_and_class.application.request.course.UpdateCourseRequest;
import com.act.course_and_class.domain.service.course.dispatcher.CreateCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.DeleteCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.GetAllCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.GetDetailCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.GetListCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.UpdateCourseService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController("CourseController")
@RequestMapping(value = "/course", produces = MediaType.APPLICATION_JSON_VALUE)
public class CourseController {

  private final GetDetailCourseService getDetailCourseService;
  private final GetListCourseService getListCourseService;
  private final GetAllCourseService getAllCourseService;
  private final CreateCourseService createCourseService;
  private final UpdateCourseService updateCourseService;
  private final DeleteCourseService deleteCourseService;

  @GetMapping(value = "/getDetail")
  public Object getDetail(@RequestParam Integer courseId) {
    return getDetailCourseService.execute(courseId);
  }

  @PostMapping(value = "/getList")
  public Object getList(@Valid @RequestBody GetListCourseRequest request) {
    return getListCourseService.execute(request);
  }

  @GetMapping(value = "/getAll")
  public Object getAll() {
    return getAllCourseService.execute();
  }

  @PostMapping(value = "/create")
  public Object create(@Valid @RequestBody CreateCourseRequest request) {
    return createCourseService.execute(request);
  }

  @PutMapping(value = "/update")
  public Object update(@Valid @RequestBody UpdateCourseRequest request) {
    return updateCourseService.execute(request);
  }

  @DeleteMapping(value = "/delete")
  public Object delete(@RequestParam Integer courseId) {
    return deleteCourseService.execute(courseId);
  }
}
