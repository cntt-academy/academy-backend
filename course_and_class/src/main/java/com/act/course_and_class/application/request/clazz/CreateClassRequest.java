package com.act.course_and_class.application.request.clazz;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateClassRequest implements Serializable {

  @NotBlank(message = "Tên đầy đủ")
  private String fullName;

  @NotBlank(message = "Tên lớp học")
  private String name;

  @NotBlank(message = "Hình thức học")
  private String howToLearn;

  private String urlClass;

  @NotBlank(message = "Ngày khai giảng")
  private String dateOpening;

  @NotNull(message = "Id giảng viên")
  private Integer teacherId;

  @NotNull(message = "Email quản lý lớp")
  private String managerEmail;

  @NotNull(message = "Email trợ giảng")
  private String supporterEmail;

  @NotNull(message = "Id khóa học")
  private Integer courseId;

  private String special;
}
