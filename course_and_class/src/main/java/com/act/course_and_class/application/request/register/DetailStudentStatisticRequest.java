package com.act.course_and_class.application.request.register;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DetailStudentStatisticRequest implements Serializable {

  private String fromDate;

  private String toDate;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
