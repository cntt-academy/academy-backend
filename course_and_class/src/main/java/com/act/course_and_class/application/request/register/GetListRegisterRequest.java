package com.act.course_and_class.application.request.register;

import com.act.course_and_class.application.request.PageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetListRegisterRequest implements Serializable {

  @NotNull(message = "Id lớp học")
  private Integer classId;

  @NotNull(message = "Phân trang")
  private PageRequest pageRequest;
}
