package com.act.course_and_class.application.request.teacher;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateTeacherRequest implements Serializable {

  @NotNull(message = "Id tài khoản")
  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String phone;
  private String email;
}