package com.act.course_and_class.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RegisterStateEnums {
  NEW("Mới đăng kí"),
  WAIT("Chưa đóng học"),
  PAY_HALF("Đã đóng một phần"),
  CONFIRM("Đã đóng học"),
  DELETED("Đã xóa");
  private final String value;
}
