package com.act.course_and_class.application.request.course;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateCourseRequest implements Serializable {

  @NotBlank(message = "Ảnh đại diện")
  private String img;

  @NotBlank(message = "Tên khóa học")
  private String name;

  @NotBlank(message = "Mô tả khóa học")
  private String desc;

  @NotBlank(message = "Nội dung khóa học")
  private String content;

  @NotNull(message = "Học phí")
  @Min(value = 1, message = "Học phí")
  private Double price;

  @NotNull(message = "Tổng số giờ học")
  @Min(value = 1, message = "Tổng số giờ học")
  private Integer totalHourStudy;

  @NotNull(message = "Loại khóa học")
  private Integer typeId;
}
