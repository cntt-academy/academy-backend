package com.act.course_and_class.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VideoLectureEnums {
  WAIT("Chờ xem"),
  WATCHED("Đã xem"),
  BLOCKED("Bị khóa");
  private final String value;
}