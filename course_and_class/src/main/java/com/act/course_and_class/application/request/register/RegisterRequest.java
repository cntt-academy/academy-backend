package com.act.course_and_class.application.request.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterRequest implements Serializable {

  @NotBlank(message = "Email học viên")
  private String userEmail;

  private Integer courseId;

  private String howToLearn;

  private Integer classId;

  private String adminEmail;
}
