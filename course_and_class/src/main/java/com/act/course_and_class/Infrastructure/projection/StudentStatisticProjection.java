package com.act.course_and_class.Infrastructure.projection;

import java.util.Date;

public interface StudentStatisticProjection {

  String getState();

  Integer getClassId();

  String getClassName();

  Date getCreatedDate();

  String getClassState();

  Integer getNumberStudent();
}
