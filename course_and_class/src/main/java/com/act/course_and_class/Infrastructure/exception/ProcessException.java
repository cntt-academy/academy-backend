package com.act.course_and_class.Infrastructure.exception;

public class ProcessException extends RuntimeException {

  public ProcessException(String message) {
    super(message);
  }
}
