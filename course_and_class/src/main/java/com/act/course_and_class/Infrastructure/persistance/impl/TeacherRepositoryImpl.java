package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.TeacherJpaRepository;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("TeacherRepository")
public class TeacherRepositoryImpl implements TeacherRepository {

  private final TeacherJpaRepository teacherJpaRepository;

  @Override
  public Page<Teacher> findAllByCondition(String search, PageRequest pageRequest) {
    return teacherJpaRepository
        .findAllByCondition(search, StateEnums.ACTIVE.name(), pageRequest);
  }

  @Override
  public Teacher findByEmail(String email) {
    return teacherJpaRepository
        .findByEmailAndState(email, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public Teacher findById(Integer teacherId) {
    return teacherJpaRepository
        .findByIdAndState(teacherId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public void save(Teacher teacher) {
    teacherJpaRepository.save(teacher);
  }

  @Override
  public List<Teacher> findAll() {
    return teacherJpaRepository.findAllByState(StateEnums.ACTIVE.name());
  }
}
