package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.TypeCourse;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TypeCourseJpaRepository extends JpaRepository<TypeCourse, Integer> {

  @Query(
      value =
          "SELECT DISTINCT(tc) FROM TypeCourse tc "
              + "WHERE (:name IS NULL OR tc.name LIKE :name) "
              + "AND tc.state = :state")
  Page<TypeCourse> findAllByCondition(
      @Param(value = "name") String name,
      @Param(value = "state") String state,
      Pageable pageable);

  Optional<TypeCourse> findByNameIgnoreCaseAndState(String name, String state);

  Optional<TypeCourse> findByIdAndState(Integer typeId, String state);

  List<TypeCourse> findAllByState(String state);
}