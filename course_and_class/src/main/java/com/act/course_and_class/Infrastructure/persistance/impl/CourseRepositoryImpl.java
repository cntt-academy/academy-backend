package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.CourseJpaRepository;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.CourseRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("CourseRepository")
public class CourseRepositoryImpl implements CourseRepository {

  private final CourseJpaRepository courseJpaRepository;

  @Override
  public Page<Course> findAllByConditions(String name, Integer typeId, PageRequest pageRequest) {
    return courseJpaRepository
        .findAllByConditions(name, typeId, StateEnums.ACTIVE.name(), pageRequest);
  }

  @Override
  public Course findById(Integer courseId) {
    return courseJpaRepository
        .findByIdAndState(courseId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public Course findByName(String name) {
    return courseJpaRepository
        .findByNameIgnoreCaseAndState(name, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public void save(Course course) {
    courseJpaRepository.save(course);
  }

  @Override
  public List<Course> findAll() {
    return courseJpaRepository.findAllByState(StateEnums.ACTIVE.name());
  }
}
