package com.act.course_and_class.Infrastructure.utils;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.filter.UserInfo;
import com.act.course_and_class.application.enums.GenderEnums;
import com.act.course_and_class.application.enums.RoleEnums;
import com.google.gson.JsonObject;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("CourseAndClassUtils")
public class CourseAndClassUtils {

  public static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  private final AESUtils aesUtils;
  private final EurekaClient eurekaClient;

  @Value("${path.getUserByEmail}")
  private String pathGetUserByEmail;
  @Value("${path.getAdminByEmail}")
  private String pathGetAdminByEmail;

  /**
   * Lấy ra thông tin Account đang login
   */
  public Map<String, String> getAccountLoginInfo() {
    log.info("<AdminUtils> => Function getAdminLoginInfo");

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserInfo userInfo = (UserInfo) authentication.getDetails();

    String email = userInfo.getEmail();
    log.info("Email: {}", email);
    String name = userInfo.getName();
    log.info("Name: {}", name);
    String role = userInfo.getRole();
    log.info("Role: {}", role);

    return Map.of("email", email, "name", name, "role", role);
  }

  /**
   * Kiểm tra quyền của tài khoản
   */
  public Boolean isOwnerOrManager(String role) {
    return role.equals(RoleEnums.Owner.name()) || role.equals(RoleEnums.Manager.name());
  }

  /**
   * Chuẩn hóa parameter Query Like
   */
  public String standardizedParameterQueryLike(String str) {
    return str == null || str.trim().isEmpty() ? null : "%" + str + "%";
  }

  /**
   * Chuẩn hóa parameter Query Equal type String
   */
  public String standardizedParameterQueryEqual(String str) {
    return StringUtils.isBlank(str) ? null : str;
  }

  /**
   * Chuẩn hóa parameter Query Equal type Integer
   */
  public Integer standardizedParameterQueryEqual(Integer number) {
    return number == null || number == 0 ? null : number;
  }

  /**
   * Validate và chuẩn hóa Gender
   */
  public String validateAndStandardizedGender(String gender) {
    try {
      GenderEnums genderEnums = GenderEnums.valueOf(StringUtils.capitalize(gender.toLowerCase()));
      return genderEnums.name();
    } catch (Exception ex) {
      throw new BadRequestException("Giới tính không hợp lệ");
    }
  }

  /**
   * Lấy ra thông tin User theo email
   */
  public Map<String, Object> getUserByEmail(String email) {
    Map<String, Object> parameters = Map.of("email", aesUtils.encrypt(email));

    String url = eurekaClient
        .getNextServerFromEureka("user", false).getHomePageUrl()
        + "internal/user" + pathGetUserByEmail;
    JsonObject resp = UnirestUtils.getWithoutHeader(url, parameters);

    log.info("Url: {}", url);
    log.info("Email: {}", email);
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
      JsonObject user = resp.get("data").getAsJsonObject().get("user").getAsJsonObject();
      Map<String, Object> response = new HashMap<>();
      response.put("id", user.get("id").getAsInt());
      response.put("name", user.get("name").getAsString());
      response.put("email", user.get("email").getAsString());
      response.put("phone", user.get("phone").getAsString());
      return response;
    }
    return null;
  }

  /**
   * Lấy ra thông tin Admin theo email
   */
  public Map<String, Object> getAdminByEmail(String email) {
    Map<String, Object> parameters = Map.of("email", aesUtils.encrypt(email));

    String url = eurekaClient
        .getNextServerFromEureka("admin", false).getHomePageUrl()
        + "internal/admin" + pathGetAdminByEmail;
    JsonObject resp = UnirestUtils.getWithoutHeader(url, parameters);

    log.info("Url: {}", url);
    log.info("Email: {}", email);
    log.info("Result calling API: {}", resp);

    if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
      JsonObject admin = resp.get("data").getAsJsonObject().get("admin").getAsJsonObject();
      Map<String, Object> response = new HashMap<>();
      response.put("id", admin.get("id").getAsInt());
      response.put("name", admin.get("name").getAsString());
      response.put("email", admin.get("email").getAsString());
      return response;
    }
    return null;
  }
}
