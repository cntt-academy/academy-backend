package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.VideoLecture;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface VideoLectureJpaRepository extends JpaRepository<VideoLecture, Integer> {

  @Query(value =
      "SELECT DISTINCT(vl) FROM VideoLecture vl "
          + "WHERE (:classId IS NULL OR vl.classId = :classId) "
          + "AND (:name IS NULL OR vl.name LIKE :name)")
  Page<VideoLecture> findAllByCondition(
      @Param(value = "name") String name,
      @Param(value = "classId") Integer classId,
      Pageable pageable);

  @Query(value =
      "SELECT IFNULL(MAX(vl.video_order), 0) "
          + "FROM video_lectures vl "
          + "WHERE (:classId IS NULL OR vl.class_id = :classId)",
      nativeQuery = true)
  Integer getOrderByClassId(@Param(value = "classId") Integer classId);

  List<VideoLecture> findAllByClassIdOrderByVideoOrderAsc(Integer classId);

  Optional<VideoLecture> findByVideoOrderAndClassId(Integer videoOrder, Integer classId);

  Optional<VideoLecture> findByNameIgnoreCase(String name);

  Optional<VideoLecture> findByUrl(String url);


}