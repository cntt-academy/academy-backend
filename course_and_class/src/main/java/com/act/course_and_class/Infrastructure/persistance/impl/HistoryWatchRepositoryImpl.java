package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.HistoryWatchJpaRepository;
import com.act.course_and_class.domain.entity.HistoryWatch;
import com.act.course_and_class.domain.repository.HistoryWatchRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("HistoryWatchRepository")
public class HistoryWatchRepositoryImpl implements HistoryWatchRepository {

  private final HistoryWatchJpaRepository historyWatchJpaRepository;

  @Override
  public List<HistoryWatch> findAllByVideoLectureIdAndUserEmail(
      Integer videoLectureId, String userEmail) {
    return historyWatchJpaRepository.findAllByVideoLectureIdAndUserEmail(videoLectureId, userEmail);
  }

  @Override
  public List<Integer> findAllVideoIdByClassIdAndUserEmail(Integer classId, String userEmail) {
    return historyWatchJpaRepository.findAllVideoIdByClassIdAndUserEmail(classId, userEmail);
  }

  @Override
  public void save(HistoryWatch historyWatch) {
    historyWatchJpaRepository.save(historyWatch);
  }
}
