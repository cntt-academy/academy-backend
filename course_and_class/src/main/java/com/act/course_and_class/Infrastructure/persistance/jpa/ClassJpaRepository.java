package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.Clazz;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClassJpaRepository extends JpaRepository<Clazz, Integer> {

  List<Clazz> findAllByCourse_IdAndHowToLearnAndState(
      Integer courseId, String howToLearn, String state);

  Optional<Clazz> findByFullNameAndStateNot(String fullName, String stateNot);

  Optional<Clazz> findByCourse_IdAndState(Integer courseId, String state);

  Optional<Clazz> findByIdAndStateNot(Integer classId, String stateNot);

  Optional<Clazz> findByNameAndStateNot(String name, String stateNot);

  List<Clazz> findAllByStateNot(String stateNot);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Clazz c "
              + "WHERE (:search IS NULL "
              + "       OR c.name LIKE :search"
              + "       OR c.manager LIKE :search"
              + "       OR c.supporter LIKE :search)"
              + "AND (:howToLearn IS NULL OR c.howToLearn = :howToLearn) "
              + "AND (:state IS NULL OR c.state = :state) "
              + "AND c.state <> :stateDeleted")
  Page<Clazz> findAllByConditions(
      @Param(value = "state") String state,
      @Param(value = "search") String search,
      @Param(value = "howToLearn") String howToLearn,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Clazz c "
              + " WHERE (:adminEmail IS NULL "
              + "        OR c.supporterEmail = :adminEmail "
              + "        OR c.managerEmail = :adminEmail) "
              + " AND c.state NOT IN :stateNot")
  Page<Clazz> findAllByAdmin(
      @Param(value = "adminEmail") String adminEmail,
      @Param(value = "stateNot") String stateNot,
      Pageable pageable);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Clazz c "
              + " WHERE (:teacherId IS NULL OR c.teacher.id = :teacherId) "
              + " AND c.state NOT IN :stateNot")
  Page<Clazz> findAllByTeacher(
      @Param(value = "teacherId") Integer teacherId,
      @Param(value = "stateNot") String stateNot,
      Pageable pageable);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Clazz c JOIN Register r "
              + "ON c.id = r.classId "
              + "WHERE (:search IS NULL "
              + "       OR c.name LIKE :search"
              + "       OR c.manager LIKE :search"
              + "       OR c.supporter LIKE :search)"
              + "AND (:studentId IS NULL OR r.studentId = :studentId) "
              + "AND (:state IS NULL OR c.state = :state) "
              + "AND c.state NOT IN :classStateNot "
              + "AND r.state <> :registerStateNot ")
  Page<Clazz> findAllByUser(
      @Param(value = "classStateNot") List<String> classStateNot,
      @Param(value = "registerStateNot") String registerStateNot,
      @Param(value = "studentId") Integer studentId,
      @Param(value = "search") String search,
      @Param(value = "state") String state,
      Pageable pageable);
}