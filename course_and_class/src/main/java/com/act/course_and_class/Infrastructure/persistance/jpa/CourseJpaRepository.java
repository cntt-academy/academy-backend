package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.Course;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CourseJpaRepository extends JpaRepository<Course, Integer> {

  Optional<Course> findByNameIgnoreCaseAndState(String name, String state);

  Optional<Course> findByIdAndState(Integer courseId, String state);

  List<Course> findAllByState(String state);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Course c "
              + "WHERE (:name IS NULL OR (c.name LIKE :name)) "
              + "AND (:typeId IS NULL OR c.typeId = :typeId) "
              + "AND c.state = :state")
  Page<Course> findAllByConditions(
      @Param(value = "name") String name,
      @Param(value = "typeId") Integer typeId,
      @Param(value = "state") String state,
      Pageable pageable);
}