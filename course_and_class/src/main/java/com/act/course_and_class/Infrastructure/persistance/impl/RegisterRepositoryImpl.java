package com.act.course_and_class.Infrastructure.persistance.impl;

import static com.act.course_and_class.application.enums.RegisterStateEnums.DELETED;

import com.act.course_and_class.Infrastructure.persistance.jpa.RegisterJpaRepository;
import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.RegisterRepository;
import java.util.Date;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("RegisterRepository")
public class RegisterRepositoryImpl implements RegisterRepository {

  private final RegisterJpaRepository registerJpaRepository;

  @Override
  public List<StudentStatisticProjection> overviewStatisticStudent(Date fromDate, Date toDate) {
    return registerJpaRepository.overviewStatisticStudent(
        DELETED.name(), fromDate, toDate);
  }

  @Override
  public Page<StudentStatisticProjection> detailStatisticStudent(
      Date fromDate, Date toDate, PageRequest pageRequest) {
    return registerJpaRepository.detailStatisticStudent(
        DELETED.name(), fromDate, toDate, pageRequest);
  }

  @Override
  public Page<Register> findAllByClassId(Integer classId, PageRequest pageRequest) {
    return registerJpaRepository
        .findAllByClassIdAndStateNot(classId, DELETED.name(), pageRequest);
  }

  @Override
  public Register findByStudentIdAndClassId(Integer studentId, Integer classId) {
    return registerJpaRepository
        .findByStudentIdAndClassIdAndStateNot(studentId, classId, DELETED.name())
        .orElse(null);
  }

  @Override
  public List<Register> findAllByStudentId(Integer studentId) {
    return registerJpaRepository.findAllByStudentIdAndStateNot(studentId, DELETED.name());
  }

  @Override
  public Long countTotalStudent(Date fromDate, Date toDate) {
    return registerJpaRepository.countTotalStudent(
        DELETED.name(), fromDate, toDate);
  }

  @Override
  public Register findById(Integer registerId) {
    return registerJpaRepository
        .findByIdAndStateNot(registerId, DELETED.name())
        .orElse(null);
  }

  @Override
  public void saveAll(List<Register> registers) {
    registerJpaRepository.saveAll(registers);
  }

  @Override
  public void save(Register register) {
    registerJpaRepository.save(register);
  }
}
