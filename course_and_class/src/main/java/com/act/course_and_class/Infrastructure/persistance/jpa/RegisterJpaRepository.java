package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.domain.entity.Register;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RegisterJpaRepository extends JpaRepository<Register, Integer> {

  List<Register> findAllByStudentIdAndStateNot(Integer studentId, String stateNot);

  Page<Register> findAllByClassIdAndStateNot(
      Integer classId,
      String stateNot,
      Pageable pageable);

  Optional<Register> findByStudentIdAndClassIdAndStateNot(
      Integer studentId,
      Integer classId,
      String stateNot);

  Optional<Register> findByIdAndStateNot(Integer registerId, String stateNot);

  @Query(
      value =
          " SELECT COALESCE(COUNT(DISTINCT r.studentId), 0) "
              + " FROM Register r "
              + " WHERE (:fromDate IS NULL OR r.createdDate >= :fromDate) "
              + "  AND (:toDate IS NULL OR r.createdDate <= :toDate) "
              + "  And r.state <> :stateNot ")
  Long countTotalStudent(
      @Param(value = "stateNot") String stateNot,
      @Param(value = "fromDate") Date fromDate,
      @Param(value = "toDate") Date toDate);

  @Query(
      value =
          "SELECT r.createdDate            AS createdDate, "
              + " COALESCE(COUNT(r.id), 0) AS numberStudent "
              + " FROM Register r "
              + " WHERE (:fromDate IS NULL OR r.createdDate >= :fromDate) "
              + "  AND (:toDate IS NULL OR r.createdDate <= :toDate) "
              + "  And r.state <> :stateNot "
              + " GROUP BY r.createdDate "
              + " ORDER BY r.createdDate")
  List<StudentStatisticProjection> overviewStatisticStudent(
      @Param(value = "stateNot") String stateNot,
      @Param(value = "fromDate") Date fromDate,
      @Param(value = "toDate") Date toDate);

  @Query(
      value =
          "SELECT r.classId                AS classId, "
              + " r.className              AS className, "
              + " c.state                  AS classState, "
              + " r.state                  AS state, "
              + " COALESCE(COUNT(r.id), 0) AS numberStudent "
              + " FROM Register r JOIN Clazz c "
              + " ON r.classId = c.id "
              + " WHERE (:fromDate IS NULL OR r.createdDate >= :fromDate) "
              + "  AND (:toDate IS NULL OR r.createdDate <= :toDate) "
              + "  And r.state <> :stateNot "
              + " GROUP BY r.classId, r.state "
              + " ORDER BY r.classId ")
  Page<StudentStatisticProjection> detailStatisticStudent(
      @Param(value = "stateNot") String stateNot,
      @Param(value = "fromDate") Date fromDate,
      @Param(value = "toDate") Date toDate,
      Pageable pageable);
}