package com.act.course_and_class.Infrastructure.utils;

import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.GenderEnums;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.domain.dto.ClassOutputDto;
import com.act.course_and_class.domain.dto.CourseOutputDto;
import com.act.course_and_class.domain.dto.RegisterOutputDto;
import com.act.course_and_class.domain.dto.StudentStatisticOutputDto;
import com.act.course_and_class.domain.dto.TeacherOutputDto;
import com.act.course_and_class.domain.dto.TypeCourseOutputDto;
import com.act.course_and_class.domain.dto.VideoLectureOutputDto;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@RequiredArgsConstructor
@Component("GenericMapper")
public class GenericMapper {

  private final TypeCourseRepository typeCourseRepository;
  private final ModelMapper modelMapper;

  // map cả null sang
  public <T, E> E mapToType(T source, Class<E> typeDestination) {
    if (source == null) {
      return null;
    }
    return modelMapper.map(source, typeDestination);
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (source == null || source.isEmpty()) {
      return new ArrayList<>();
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }

  public void copyNonNullProperties(Object src, Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  public String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  public TypeCourseOutputDto mapToTypeCourseOutputDto(TypeCourse typeCourse) {
    TypeCourseOutputDto typeCourseOutputDto = mapToType(typeCourse, TypeCourseOutputDto.class);
    if (typeCourseOutputDto != null) {
      typeCourseOutputDto.setCreatedDate(
          CourseAndClassUtils.sdf.format(typeCourse.getCreatedDate()));
    }
    return typeCourseOutputDto;
  }

  public List<TypeCourseOutputDto> mapToListTypeCourseOutputDto(List<TypeCourse> typeCourses) {
    if (CollectionUtils.isEmpty(typeCourses)) {
      return new ArrayList<>();
    }
    return typeCourses.stream()
        .map(this::mapToTypeCourseOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public CourseOutputDto mapToCourseOutputDto(Course course) {
    CourseOutputDto courseOutputDto = mapToType(course, CourseOutputDto.class);
    if (courseOutputDto != null) {
      courseOutputDto.setType(typeCourseRepository.getDescById(course.getTypeId()));
      courseOutputDto.setCreatedDate(CourseAndClassUtils.sdf.format(course.getCreatedDate()));
    }
    return courseOutputDto;
  }

  public List<CourseOutputDto> mapToListCourseOutputDto(List<Course> courses) {
    if (CollectionUtils.isEmpty(courses)) {
      return new ArrayList<>();
    }
    return courses.stream()
        .map(this::mapToCourseOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public TeacherOutputDto mapToTeacherOutputDto(Teacher teacher) {
    TeacherOutputDto teacherOutputDto = mapToType(teacher, TeacherOutputDto.class);
    if (teacherOutputDto != null) {
      teacherOutputDto.setGender(GenderEnums.valueOf(teacher.getGender()).getValue());
      teacherOutputDto.setCreatedDate(CourseAndClassUtils.sdf.format(teacher.getCreatedDate()));
    }
    return teacherOutputDto;
  }

  public List<TeacherOutputDto> mapToListTeacherOutputDto(List<Teacher> teachers) {
    if (CollectionUtils.isEmpty(teachers)) {
      return new ArrayList<>();
    }
    return teachers.stream()
        .map(this::mapToTeacherOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public ClassOutputDto mapToClassOutputDto(Clazz clazz) {
    ClassOutputDto classOutputDto = mapToType(clazz, ClassOutputDto.class);
    if (classOutputDto != null) {
      classOutputDto.setAvatar(clazz.getCourse().getImg());
      if (classOutputDto.getDateOpening() != null) {
        classOutputDto.setDateOpening(CourseAndClassUtils.sdf.format(clazz.getDateOpening()));
      }
      if (classOutputDto.getTeacher() != null) {
        classOutputDto.setTeacher(clazz.getTeacher().getName());
      }
      classOutputDto.setCourseId(clazz.getCourse().getId());
      classOutputDto.setCourse(clazz.getCourse().getName());
      classOutputDto.setState(ClassStateEnums.valueOf(clazz.getState()).getValue());
      classOutputDto.setCreatedDate(CourseAndClassUtils.sdf.format(clazz.getCreatedDate()));
    }
    return classOutputDto;
  }

  public List<ClassOutputDto> mapToListClassOutputDto(List<Clazz> classes) {
    if (CollectionUtils.isEmpty(classes)) {
      return new ArrayList<>();
    }
    return classes.stream()
        .map(this::mapToClassOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public VideoLectureOutputDto mapToVideoLectureOutputDto(VideoLecture videoLecture) {
    VideoLectureOutputDto videoLectureOutputDto =
        mapToType(videoLecture, VideoLectureOutputDto.class);
    if (videoLectureOutputDto != null) {
      videoLectureOutputDto.setCreatedDate(
          CourseAndClassUtils.sdf.format(videoLecture.getCreatedDate()));
    }
    return videoLectureOutputDto;
  }

  public List<VideoLectureOutputDto> mapToListVideoLectureOutputDto(
      List<VideoLecture> videoLectures) {
    if (CollectionUtils.isEmpty(videoLectures)) {
      return new ArrayList<>();
    }
    return videoLectures.stream()
        .map(this::mapToVideoLectureOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public RegisterOutputDto mapToRegisterOutputDto(Register register) {
    RegisterOutputDto registerOutputDto = mapToType(register, RegisterOutputDto.class);
    if (registerOutputDto != null) {
      registerOutputDto.setState(RegisterStateEnums.valueOf(register.getState()).getValue());
      registerOutputDto.setCreatedDate(CourseAndClassUtils.sdf.format(register.getCreatedDate()));
    }
    return registerOutputDto;
  }

  public List<RegisterOutputDto> mapToListRegisterOutputDto(List<Register> registers) {
    if (CollectionUtils.isEmpty(registers)) {
      return new ArrayList<>();
    }
    return registers.stream()
        .map(this::mapToRegisterOutputDto)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }
}
