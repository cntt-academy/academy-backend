package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.ClassJpaRepository;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.repository.ClassRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("ClassRepository")
public class ClassRepositoryImpl implements ClassRepository {

  private final ClassJpaRepository classJpaRepository;

  @Override
  public List<Clazz> findAllByCourseIdAndHowToLearnAndState(
      Integer courseId, String howToLearn, String state) {
    return classJpaRepository
        .findAllByCourse_IdAndHowToLearnAndState(courseId, howToLearn, state);
  }

  @Override
  public Page<Clazz> findAllByConditions(
      String search, String state, String howToLearn, PageRequest pageRequest) {
    return classJpaRepository
        .findAllByConditions(
            state, search, howToLearn, ClassStateEnums.DELETED.name(), pageRequest);
  }

  @Override
  public Page<Clazz> findAllByUser(
      Integer studentId, String search, String state, String stateNot, PageRequest pageRequest) {
    List<String> classStateNot = new ArrayList<>(List.of(ClassStateEnums.DELETED.name()));
    if (StringUtils.isNotBlank(stateNot)) {
      classStateNot.add(stateNot);
    }
    return classJpaRepository.findAllByUser(
        classStateNot, RegisterStateEnums.DELETED.name(), studentId, search, state, pageRequest);
  }

  @Override
  public Page<Clazz> findAllByTeacher(Integer teacherId, PageRequest pageRequest) {
    return classJpaRepository
        .findAllByTeacher(teacherId, ClassStateEnums.DELETED.name(), pageRequest);
  }

  @Override
  public Page<Clazz> findAllByAdmin(String adminEmail, PageRequest pageRequest) {
    return classJpaRepository
        .findAllByAdmin(adminEmail, ClassStateEnums.DELETED.name(), pageRequest);
  }

  @Override
  public Clazz findByCourseIdAndState(Integer courseId, String state) {
    return classJpaRepository
        .findByCourse_IdAndState(courseId, state)
        .orElse(null);
  }

  @Override
  public Clazz findByFullName(String fullName) {
    return classJpaRepository
        .findByFullNameAndStateNot(fullName, ClassStateEnums.DELETED.name())
        .orElse(null);
  }

  @Override
  public Clazz findById(Integer classId) {
    return classJpaRepository
        .findByIdAndStateNot(classId, ClassStateEnums.DELETED.name())
        .orElse(null);
  }

  @Override
  public Clazz findByName(String name) {
    return classJpaRepository
        .findByNameAndStateNot(name, ClassStateEnums.DELETED.name())
        .orElse(null);
  }

  @Override
  public void saveAll(List<Clazz> classes) {
    classJpaRepository.saveAll(classes);
  }

  @Override
  public void save(Clazz clazz) {
    classJpaRepository.save(clazz);
  }

  @Override
  public List<Clazz> findAll() {
    return classJpaRepository.findAllByStateNot(ClassStateEnums.DELETED.name());
  }
}
