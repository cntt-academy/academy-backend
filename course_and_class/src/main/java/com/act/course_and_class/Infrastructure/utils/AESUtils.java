package com.act.course_and_class.Infrastructure.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component("AESUtils")
public class AESUtils {

  private static final String TRANSFORMATION = "AES/ECB/PKCS5PADDING";
  private static final String AES = "AES";
  @Value("${aesKey}")
  private String aesKey;

  @SneakyThrows
  public String encrypt(String strToEncrypt) {
    SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), AES);

    Cipher cipher = Cipher.getInstance(TRANSFORMATION);
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    byte[] encryptedBytes = cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8));

    return Base64.getUrlEncoder().withoutPadding().encodeToString(encryptedBytes);
  }

  @SneakyThrows
  public String decrypt(String strToDecrypt) {
    SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), AES);

    Cipher cipher = Cipher.getInstance(TRANSFORMATION);
    cipher.init(Cipher.DECRYPT_MODE, secretKey);
    byte[] decryptedBytes = cipher.doFinal(Base64.getUrlDecoder().decode(strToDecrypt));

    return new String(decryptedBytes, StandardCharsets.UTF_8);
  }
}
