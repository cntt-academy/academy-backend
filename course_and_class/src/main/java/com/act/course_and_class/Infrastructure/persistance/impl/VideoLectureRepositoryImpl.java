package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.VideoLectureJpaRepository;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("VideoLectureRepository")
public class VideoLectureRepositoryImpl implements VideoLectureRepository {

  private final VideoLectureJpaRepository videoLectureJpaRepository;

  @Override
  public Page<VideoLecture> findAllByCondition(
      Integer classId, String name, PageRequest pageRequest) {
    return videoLectureJpaRepository.findAllByCondition(name, classId, pageRequest);
  }

  @Override
  public VideoLecture findByVideoOrderAndClassId(Integer videoOrder, Integer classId) {
    return videoLectureJpaRepository
        .findByVideoOrderAndClassId(videoOrder, classId)
        .orElse(null);
  }

  @Override
  public List<VideoLecture> findAllByClassId(Integer classId) {
    return videoLectureJpaRepository.findAllByClassIdOrderByVideoOrderAsc(classId);
  }

  @Override
  public VideoLecture findById(Integer videoLectureId) {
    return videoLectureJpaRepository.findById(videoLectureId).orElse(null);
  }

  @Override
  public Integer getOrderByClassId(Integer classId) {
    return videoLectureJpaRepository.getOrderByClassId(classId) + 1;
  }

  @Override
  public VideoLecture findByName(String name) {
    return videoLectureJpaRepository.findByNameIgnoreCase(name).orElse(null);
  }

  @Override
  public VideoLecture findByUrl(String url) {
    return videoLectureJpaRepository.findByUrl(url).orElse(null);
  }

  @Override
  public void saveAll(List<VideoLecture> videos) {
    videoLectureJpaRepository.saveAll(videos);
  }

  @Override
  public void delete(VideoLecture video) {
    videoLectureJpaRepository.delete(video);
  }

  @Override
  public void save(VideoLecture video) {
    videoLectureJpaRepository.save(video);
  }
}
