package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.HistoryWatch;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface HistoryWatchJpaRepository extends JpaRepository<HistoryWatch, Integer> {

  List<HistoryWatch> findAllByVideoLectureIdAndUserEmail(Integer videoLectureId, String userEmail);

  @Query(value = "SELECT DISTINCT (hw.videoLectureId) FROM HistoryWatch hw "
      + "WHERE hw.classId = :classId AND hw.userEmail = :userEmail")
  List<Integer> findAllVideoIdByClassIdAndUserEmail(
      @Param(value = "classId") Integer classId,
      @Param(value = "userEmail") String userEmail);
}