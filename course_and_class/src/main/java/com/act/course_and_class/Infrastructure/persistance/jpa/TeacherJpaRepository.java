package com.act.course_and_class.Infrastructure.persistance.jpa;

import com.act.course_and_class.domain.entity.Teacher;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TeacherJpaRepository extends JpaRepository<Teacher, Integer> {

  Optional<Teacher> findByEmailAndState(String email, String state);

  Optional<Teacher> findByIdAndState(Integer userId, String state);

  @Query(
      value =
          "SELECT DISTINCT(t) FROM Teacher t "
              + "WHERE (:search IS NULL OR (t.name LIKE :search OR t.email LIKE :search OR t.phone LIKE :search)) "
              + "AND t.state = :state")
  Page<Teacher> findAllByCondition(
      @Param(value = "search") String search,
      @Param(value = "state") String state,
      Pageable pageable);

  List<Teacher> findAllByState(String state);
}