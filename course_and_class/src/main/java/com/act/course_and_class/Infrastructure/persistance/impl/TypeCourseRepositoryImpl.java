package com.act.course_and_class.Infrastructure.persistance.impl;

import com.act.course_and_class.Infrastructure.persistance.jpa.TypeCourseJpaRepository;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@RequiredArgsConstructor
@Repository("TypeCourseRepository")
public class TypeCourseRepositoryImpl implements TypeCourseRepository {

  private final TypeCourseJpaRepository typeCourseJpaRepository;

  @Override
  public Page<TypeCourse> findAllByCondition(String name, PageRequest pageRequest) {
    return typeCourseJpaRepository.findAllByCondition(name, StateEnums.ACTIVE.name(), pageRequest);
  }

  @Override
  public TypeCourse findById(Integer typeId) {
    return typeCourseJpaRepository
        .findByIdAndState(typeId, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public TypeCourse findByName(String name) {
    return typeCourseJpaRepository
        .findByNameIgnoreCaseAndState(name, StateEnums.ACTIVE.name())
        .orElse(null);
  }

  @Override
  public String getDescById(Integer typeId) {
    TypeCourse type = findById(typeId);
    return type == null ? "" : type.getDesc();
  }

  @Override
  public void save(TypeCourse typeCourse) {
    typeCourseJpaRepository.save(typeCourse);
  }

  @Override
  public List<TypeCourse> findAll() {
    return typeCourseJpaRepository.findAllByState(StateEnums.ACTIVE.name());
  }
}
