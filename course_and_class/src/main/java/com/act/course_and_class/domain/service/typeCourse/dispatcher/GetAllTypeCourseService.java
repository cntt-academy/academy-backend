package com.act.course_and_class.domain.service.typeCourse.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetAllTypeCourseService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
