package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.Infrastructure.utils.RedisUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.CourseOutputDto;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.service.course.dispatcher.GetAllCourseService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllCourseService")
public class GetAllCourseServiceImpl implements GetAllCourseService {

  private final CourseRepository courseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL COURSE ========");

    List<Course> courses = courseRepository.findAll();
    log.info("<Query> => Get all course result: {}", gson.toJson(courses));

    List<CourseOutputDto> courseOutputDtos = genericMapper.mapToListCourseOutputDto(courses);

    // Lưu vào Redis
    String dataCourse;
    CourseOutputDto course;
    RedisUtils.set("TOTAL_COURSE", String.valueOf(courses.size()));
    for (int i = 1; i <= courseOutputDtos.size(); i++) {
      course = courseOutputDtos.get(i - 1);
      dataCourse = course.getId() + "," + course.getType() + "," + course.getName();
      RedisUtils.set("COURSE_" + i, dataCourse);
    }

    Map<String, Object> data =
        Map.of("courses", courseOutputDtos, "totalElements", courses.size());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách tất cả khóa học và lưu vào Redis thành công");
    response.setData(data);

    log.info("<Result API> => Getting all course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
