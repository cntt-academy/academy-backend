package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.request.register.UpdateStateRegisterRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.UpdateStateRegisterService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateStateRegisterService")
public class UpdateStateRegisterServiceImpl implements UpdateStateRegisterService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final ClassRepository classRepository;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Register register;
  private String state;

  @Override
  public ResponseEntity<?> execute(UpdateStateRegisterRequest request) {
    log.info("======== API UPDATE STATE REGISTER ========");
    log.info("Request: {}", gson.toJson(request));
    return UpdateStateRegisterService.super.execute(request);
  }

  @Override
  public void validate(UpdateStateRegisterRequest request) {
    register = registerRepository.findById(request.getRegisterId());
    log.info("<Query> => Result finding register by id: {}", register);
    if (register == null) {
      throw new BadRequestException("Không tìm thấy thông tin đăng ký muốn cập nhật");
    }

    Clazz clazz = classRepository.findById(register.getClassId());
    log.info("<Query> => Result finding class by id: {}", clazz);
    if (clazz == null) {
      throw new BadRequestException("Không tìm thấy thông tin lớp học");
    }

    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))
        && !clazz.getSupporterEmail().equalsIgnoreCase(loggedAccount.get("email"))
        && !clazz.getManagerEmail().equalsIgnoreCase(loggedAccount.get("email"))) {
      throw new BadRequestException("Bạn không có quyền cập nhật trạng thái thông tin đăng ký");
    }

    try {
      state = RegisterStateEnums.valueOf(request.getState()).name();
    } catch (Exception e) {
      throw new BadRequestException("Trạng thái cập nhật không hợp lệ");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateStateRegisterRequest request) {
    register.setState(state);
    register.setUpdatedBy(loggedAccount.get("name"));
    register.setUpdatedDate(new Date());
    registerRepository.save(register);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật trạng thái thông tin đăng ký thành công");

    log.info("<Result API> => Updating state of register successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
