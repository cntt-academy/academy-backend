package com.act.course_and_class.domain.service.typeCourse.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetDetailTypeCourseService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
