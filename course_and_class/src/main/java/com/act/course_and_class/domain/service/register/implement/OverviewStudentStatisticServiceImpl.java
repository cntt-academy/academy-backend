package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.request.register.OverviewStudentStatisticRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.StudentStatisticOutputDto;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.OverviewStudentStatisticService;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("OverviewStudentStatisticService")
public class OverviewStudentStatisticServiceImpl implements OverviewStudentStatisticService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(OverviewStudentStatisticRequest request) {
    log.info("======== API OVERALL STUDENT STATISTIC ========");
    log.info("Request: {}", request);
    return OverviewStudentStatisticService.super.execute(request);
  }

  @Override
  public void validate(OverviewStudentStatisticRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xem tổng quan thống kê học viên");
    }
  }

  @Override
  public ResponseEntity<?> process(OverviewStudentStatisticRequest request) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat year = new SimpleDateFormat("yyyy");
    SimpleDateFormat month = new SimpleDateFormat("MM");
    SimpleDateFormat day = new SimpleDateFormat("dd");

    Date fromDate = null;
    if (StringUtils.isNotBlank(request.getFromDate())) {
      try {
        fromDate = sdf.parse(request.getFromDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày bắt đầu không hợp lệ");
      }
    }

    Date toDate = null;
    if (StringUtils.isNotBlank(request.getToDate())) {
      try {
        toDate = sdf.parse(request.getToDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày kết thúc không hợp lệ");
      }
    }

    List<StudentStatisticProjection> statistics =
        registerRepository.overviewStatisticStudent(fromDate, toDate);

    List<StudentStatisticOutputDto> resultStatistic = new ArrayList<>();

    if (!CollectionUtils.isEmpty(statistics)) {
      StudentStatisticOutputDto statistic = null;
      String monthCheck = null;
      String currentMonth;

      if ((fromDate == null || toDate == null)
          || (!month.format(fromDate).equals(month.format(toDate))
          && year.format(fromDate).equals(year.format(toDate)))) {
        for (StudentStatisticProjection index : statistics) {
          currentMonth = month.format(index.getCreatedDate());

          if (monthCheck == null || !monthCheck.equals(currentMonth)) {
            if (monthCheck != null) {
              resultStatistic.add(statistic);
            }

            monthCheck = currentMonth;
            statistic = new StudentStatisticOutputDto();
            statistic.setCreatedDate("Tháng " + monthCheck);
            statistic.setTotalStudent(index.getNumberStudent());
          } else {
            statistic.setTotalStudent(statistic.getTotalStudent() + index.getNumberStudent());
          }
        }

        if (statistic != null) {
          resultStatistic.add(statistic);
        }
      } else if (!year.format(fromDate).equals(year.format(toDate))) {
        for (StudentStatisticProjection index : statistics) {
          currentMonth = month.format(index.getCreatedDate());

          if (monthCheck == null || !monthCheck.equals(currentMonth)) {
            if (monthCheck != null) {
              resultStatistic.add(statistic);
            }

            monthCheck = currentMonth;
            statistic = new StudentStatisticOutputDto();
            statistic.setCreatedDate(
                "Tháng " + monthCheck + " - " + year.format(index.getCreatedDate()));
            statistic.setTotalStudent(index.getNumberStudent());
          } else {
            statistic.setTotalStudent(statistic.getTotalStudent() + index.getNumberStudent());
          }
        }

        if (statistic != null) {
          resultStatistic.add(statistic);
        }
      } else {
        for (StudentStatisticProjection index : statistics) {
          statistic = new StudentStatisticOutputDto();
          statistic.setCreatedDate("Ngày " + day.format(index.getCreatedDate()));
          statistic.setTotalStudent(index.getNumberStudent());
          resultStatistic.add(statistic);
        }
      }
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra tổng quan thống kê học viên thành công");
    response.setData(Map.of("studentStatistics", resultStatistic));

    log.info("<Result API> => Getting list overview student statistic successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
