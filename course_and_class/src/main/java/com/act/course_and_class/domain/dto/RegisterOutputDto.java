package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.Register} entity
 */
@Data
public class RegisterOutputDto implements Serializable {

  private Integer id;
  private Integer collaboratorId;
  private String collaboratorName;
  private Integer studentId;
  private String studentName;
  private String state;
  private String createdDate;
}