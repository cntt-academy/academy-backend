package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.request.register.DetailStudentStatisticRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.StudentStatisticOutputDto;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.DetailStudentStatisticService;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("DetailStudentStatisticService")
public class DetailStudentStatisticServiceImpl implements DetailStudentStatisticService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(DetailStudentStatisticRequest request) {
    log.info("======== API DETAIL STUDENT STATISTIC ========");
    log.info("Request: {}", request);
    return DetailStudentStatisticService.super.execute(request);
  }

  @Override
  public void validate(DetailStudentStatisticRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xem chi tiết thống kê học viên");
    }
  }

  @Override
  public ResponseEntity<?> process(DetailStudentStatisticRequest request) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    Date fromDate = null;
    if (StringUtils.isNotBlank(request.getFromDate())) {
      try {
        fromDate = sdf.parse(request.getFromDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày bắt đầu không hợp lệ");
      }
    }

    Date toDate = null;
    if (StringUtils.isNotBlank(request.getToDate())) {
      try {
        toDate = sdf.parse(request.getToDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày kết thúc không hợp lệ");
      }
    }

    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<StudentStatisticProjection> statistics =
        registerRepository.detailStatisticStudent(fromDate, toDate, pageRequest);

    List<StudentStatisticProjection> statisticContents = statistics.getContent();
    List<StudentStatisticOutputDto> resultStatistic = new ArrayList<>();

    if (!CollectionUtils.isEmpty(statisticContents)) {
      StudentStatisticOutputDto statistic = null;
      Integer classId = null;

      for (StudentStatisticProjection index : statistics) {
        if (statistic == null || !Objects.equals(index.getClassId(), classId)) {
          if (statistic != null) {
            statistic.setTotalStudent(
                statistic.getNumberNewStudent()
                    + statistic.getNumberWaitStudent()
                    + statistic.getNumberPayHalfStudent()
                    + statistic.getNumberConfirmStudent());
            resultStatistic.add(statistic);
          }

          statistic = new StudentStatisticOutputDto();
          classId = index.getClassId();
          statistic.setClassId(classId);
          statistic.setClassName(index.getClassName());
          statistic.setClassState(ClassStateEnums.valueOf(index.getClassState()).getValue());
        }

        switch (RegisterStateEnums.valueOf(index.getState())) {
          case NEW:
            statistic.setNumberNewStudent(index.getNumberStudent());
            break;
          case WAIT:
            statistic.setNumberWaitStudent(index.getNumberStudent());
            break;
          case PAY_HALF:
            statistic.setNumberPayHalfStudent(index.getNumberStudent());
            break;
          case CONFIRM:
            statistic.setNumberConfirmStudent(index.getNumberStudent());
            break;
          default:
            break;
        }
      }

      if (statistic != null) {
        statistic.setTotalStudent(
            statistic.getNumberNewStudent()
                + statistic.getNumberWaitStudent()
                + statistic.getNumberPayHalfStudent()
                + statistic.getNumberConfirmStudent());
        resultStatistic.add(statistic);
      }
    }

    Map<String, Object> data =
        Map.of("studentStatistics", resultStatistic,
            "totalElements", statistics.getTotalElements(),
            "totalPages", statistics.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách chi tiết thống kê học viên thành công");
    response.setData(data);

    log.info("<Result API> => Getting list detail student statistic successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
