package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.request.clazz.GetListClassRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.ClassOutputDto;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetListClassService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListClassService")
public class GetListClassServiceImpl implements GetListClassService {

  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListClassRequest request) {
    log.info("======== API GET LIST CLASS ========");
    log.info("Request get list class: {}", gson.toJson(request));
    return GetListClassService.super.execute(request);
  }

  @Override
  public void validate(GetListClassRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListClassRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<Clazz> classes;
    if (request.getUserId() != null && request.getUserId() != 0) {
      classes = classRepository.findAllByUser(request.getUserId(),
          courseAndClassUtils.standardizedParameterQueryLike(request.getSearch()),
          courseAndClassUtils.standardizedParameterQueryEqual(request.getState()),
          courseAndClassUtils.standardizedParameterQueryEqual(request.getStateNot()),
          pageRequest);
    } else if (StringUtils.isNotBlank(request.getAdminEmail())) {
      classes = classRepository.findAllByAdmin(request.getAdminEmail(), pageRequest);
    } else if (request.getTeacherId() != null && request.getTeacherId() != 0) {
      classes = classRepository.findAllByTeacher(request.getTeacherId(), pageRequest);
    } else {
      classes = classRepository.findAllByConditions(
          courseAndClassUtils.standardizedParameterQueryLike(request.getSearch()),
          courseAndClassUtils.standardizedParameterQueryEqual(request.getState()),
          courseAndClassUtils.standardizedParameterQueryEqual(request.getHowToLearn()),
          pageRequest);
    }

    List<ClassOutputDto> classOutputDtos =
        genericMapper.mapToListClassOutputDto(classes.getContent());
    log.info("<Query> => Get list class by condition result: {}", gson.toJson(classOutputDtos));

    classOutputDtos.forEach(i -> {
      if (HowToLearnEnums.Online.name().equals(i.getHowToLearn())) {
        VideoLecture trialLesson = videoLectureRepository.findByVideoOrderAndClassId(1, i.getId());
        if (trialLesson != null) {
          i.setTrialLessonName(trialLesson.getName());
          i.setTrialLessonUrl(trialLesson.getUrl());
        }
      }
    });

    Map<String, Object> data =
        Map.of("classes", classOutputDtos,
            "totalElements", classes.getTotalElements(),
            "totalPages", classes.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách lớp học thành công");
    response.setData(data);

    log.info("<Result API> => Getting list class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
