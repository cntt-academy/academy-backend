package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.RoleEnums;
import com.act.course_and_class.application.request.teacher.GetListTeacherRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.TeacherOutputDto;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetListTeacherService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListTeacherService")
public class GetListTeacherServiceImpl implements GetListTeacherService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final TeacherRepository teacherRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListTeacherRequest request) {
    log.info("======== API GET LIST TEACHER ========");
    log.info("Request get list teacher: {}", gson.toJson(request));
    return GetListTeacherService.super.execute(request);
  }

  @Override
  public void validate(GetListTeacherRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (loggedAccount.get("role").equals(RoleEnums.User.name())) {
      throw new BadRequestException("Bạn không có quyền lấy ra danh sách giảng viên");
    }
  }

  @Override
  public ResponseEntity<?> process(GetListTeacherRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<Teacher> teachers = teacherRepository.findAllByCondition(
        courseAndClassUtils.standardizedParameterQueryLike(request.getSearch()),
        pageRequest);
    log.info("<Query> => Result getting list teacher by conditions: {}", gson.toJson(teachers));

    List<TeacherOutputDto> teacherOutputDtos =
        genericMapper.mapToListTeacherOutputDto(teachers.getContent());

    Map<String, Object> data =
        Map.of("teachers", teacherOutputDtos,
            "totalElements", teachers.getTotalElements(),
            "totalPages", teachers.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách giảng viên thành công");
    response.setData(data);

    log.info("<Result API> =>  Getting list teacher successfully: {}", gson.toJson(response));
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
