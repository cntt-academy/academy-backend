package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.service.course.dispatcher.DeleteCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.GetAllCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteCourseService")
public class DeleteCourseServiceImpl implements DeleteCourseService {


  private final GetAllCourseService getAllCourseService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final CourseRepository courseRepository;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private Course course;

  @Override
  public ResponseEntity<?> execute(Integer courseId) {
    log.info("======== API DELETE COURSE ========");
    log.info("Course Id: {}", courseId);
    return DeleteCourseService.super.execute(courseId);
  }

  @Override
  public void validate(Integer courseId) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa khóa học");
    }

    course = courseRepository.findById(courseId);
    log.info("<Query> => Result finding course by id: {}", course);
    if (course == null) {
      throw new BadRequestException("Không tìm thấy khóa học muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer courseId) {
    course.setState(StateEnums.DELETED.name());
    course.setUpdatedBy(loggedAccount.get("name"));
    course.setUpdatedDate(new Date());
    courseRepository.save(course);

    // Set lại redis danh sách khóa học
    CompletableFuture.runAsync(getAllCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa khóa học thành công");

    log.info("<Result API> => Deleting course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
