package com.act.course_and_class.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "history_watch")
public class HistoryWatch {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Column(name = "class_id", nullable = false)
  private Integer classId;

  @NotNull
  @Column(name = "video_lecture_id", nullable = false)
  private Integer videoLectureId;

  @Size(max = 50)
  @NotNull
  @Column(name = "user_email", nullable = false, length = 50)
  private String userEmail;

  @NotNull
  @Column(name = "watched_time", nullable = false)
  private Date watchedTime;

  @Override
  public String toString() {
    return "HistoryWatch{" +
        "id=" + id +
        ", classId=" + classId +
        ", video_lecture_id=" + videoLectureId +
        ", userEmail='" + userEmail + '\'' +
        ", watchedTime=" + watchedTime +
        '}';
  }
}