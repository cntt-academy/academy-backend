package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.TypeCourse;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface TypeCourseRepository {

  Page<TypeCourse> findAllByCondition(String name, PageRequest pageRequest);

  TypeCourse findById(Integer typeId);

  TypeCourse findByName(String name);

  String getDescById(Integer typeId);

  void save(TypeCourse typeCourse);

  List<TypeCourse> findAll();

}
