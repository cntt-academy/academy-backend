package com.act.course_and_class.domain.service.internal.dispatcher;

import com.act.course_and_class.application.request.register.DeleteAllRegisterRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface DeleteAllRegisterService extends
    BaseServiceRequestBody<DeleteAllRegisterRequest, ResponseEntity<?>> {

}
