package com.act.course_and_class.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "teachers")
public class Teacher {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Lob
  @Column(name = "avatar", nullable = false)
  private String avatar;

  @Size(max = 50)
  @NotNull
  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Size(max = 10)
  @NotNull
  @Column(name = "gender", nullable = false, length = 10)
  private String gender;

  @Size(max = 10)
  @NotNull
  @Column(name = "phone", nullable = false, length = 10)
  private String phone;

  @Size(max = 50)
  @NotNull
  @Column(name = "email", nullable = false, length = 50)
  private String email;

  @Size(max = 10)
  @NotNull
  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Size(max = 50)
  @NotNull
  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Size(max = 50)
  @NotNull
  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @NotNull
  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "Teacher{" +
        "id=" + id +
        ", avatar='" + avatar + '\'' +
        ", name='" + name + '\'' +
        ", gender='" + gender + '\'' +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}