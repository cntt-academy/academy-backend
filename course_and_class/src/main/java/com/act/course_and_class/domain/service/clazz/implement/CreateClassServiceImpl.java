package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.request.clazz.CreateClassRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.CreateClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetAllClassService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateClassService")
public class CreateClassServiceImpl implements CreateClassService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final GetAllClassService getAllClassService;
  private final TeacherRepository teacherRepository;
  private final CourseRepository courseRepository;
  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Map<String, Object> supporter;
  private Map<String, Object> manager;
  private String howToLearn;
  private Date dateOpening;
  private Teacher teacher;
  private Course course;

  @Override
  public ResponseEntity<?> execute(CreateClassRequest request) {
    log.info("======== API CREATE CLASS ========");
    log.info("Request create class: {}", gson.toJson(request));
    return CreateClassService.super.execute(request);
  }

  @Override
  public void validate(CreateClassRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền tạo lớp học");
    }

    Clazz clazz = classRepository.findByName(request.getName());
    log.info("<Query> => Result finding class by name: {}", clazz);
    if (clazz != null) {
      throw new BadRequestException("Tên lớp học đã được đăng ký trong hệ thống");
    }

    clazz = classRepository.findByFullName(request.getFullName());
    log.info("<Query> => Result finding class by full name: {}", clazz);
    if (clazz != null) {
      throw new BadRequestException("Tên đầy đủ lớp học đã được đăng ký trong hệ thống");
    }

    if (HowToLearnEnums.Offline.name().equalsIgnoreCase(request.getHowToLearn())) {
      howToLearn = HowToLearnEnums.Offline.name();
    } else if (HowToLearnEnums.Online.name().equalsIgnoreCase(request.getHowToLearn())) {
      howToLearn = HowToLearnEnums.Online.name();
    } else {
      throw new BadRequestException("Hình thức học không hợp lệ");
    }

    try {
      dateOpening = CourseAndClassUtils.sdf.parse(request.getDateOpening());
      if (dateOpening.before(new Date()) || dateOpening.equals(new Date())) {
        throw new BadRequestException("Ngày khai giảng không hợp lệ");
      }
    } catch (Exception e) {
      throw new BadRequestException("Ngày khai giảng không hợp lệ");
    }

    teacher = teacherRepository.findById(request.getTeacherId());
    log.info("<Query> => Result finding teacher by id: {}", teacher);
    if (teacher == null) {
      throw new BadRequestException("Không tìm thấy giảng viên");
    }

    course = courseRepository.findById(request.getCourseId());
    log.info("<Query> => Result finding course by id: {}", course);
    if (course == null) {
      throw new BadRequestException("Không tìm thấy khóa học");
    }

    manager = courseAndClassUtils.getAdminByEmail(request.getManagerEmail());
    if (manager == null) {
      throw new BadRequestException("Email quản lý lớp không hợp lệ");
    }
    supporter = courseAndClassUtils.getAdminByEmail(request.getSupporterEmail());
    if (supporter == null) {
      throw new BadRequestException("Email trợ giảng không hợp lệ");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateClassRequest request) {
    Clazz clazz = genericMapper.mapToType(request, Clazz.class);
    clazz.setHowToLearn(howToLearn);
    clazz.setDateOpening(dateOpening);
    clazz.setTeacher(teacher);
    clazz.setManager((String) manager.get("name"));
    clazz.setSupporter((String) supporter.get("name"));
    clazz.setCourse(course);
    clazz.setNumberStudent(0);
    clazz.setState(ClassStateEnums.WAIT.name());
    clazz.setCreatedBy(loggedAccount.get("name"));
    clazz.setCreatedDate(new Date());
    clazz.setUpdatedBy(loggedAccount.get("name"));
    clazz.setUpdatedDate(new Date());
    classRepository.save(clazz);

    // Set lại redis danh sách lớp học
    CompletableFuture.runAsync(getAllClassService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm lớp học thành công");

    log.info("<Result API> => Creating class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
