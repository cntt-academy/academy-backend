package com.act.course_and_class.domain.service.historyWatch.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.HistoryWatch;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.HistoryWatchRepository;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.historyWatch.dispatcher.AddHistoryWatchService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("AddHistoryWatchService")
public class AddHistoryWatchServiceImpl implements AddHistoryWatchService {

  private final VideoLectureRepository videoLectureRepository;
  private final HistoryWatchRepository historyWatchRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final Gson gson;
  private VideoLecture videoLecture;

  @Override
  public ResponseEntity<?> execute(Integer videoLectureId) {
    log.info("======== API ADD HISTORY WATCH ========");
    log.info("Video lecture id: {}", videoLectureId);
    return AddHistoryWatchService.super.execute(videoLectureId);
  }

  @Override
  public void validate(Integer videoLectureId) {
    videoLecture = videoLectureRepository.findById(videoLectureId);
    log.info("<Query> => Result finding video lecture by id: {}", videoLecture);
    if (videoLecture == null) {
      throw new BadRequestException("Bài giảng không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer videoLectureId) {
    HistoryWatch historyWatch = new HistoryWatch();
    historyWatch.setClassId(videoLecture.getClassId());
    historyWatch.setVideoLectureId(videoLecture.getId());
    historyWatch.setUserEmail(courseAndClassUtils.getAccountLoginInfo().get("email"));
    historyWatch.setWatchedTime(new Date());
    historyWatchRepository.save(historyWatch);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm lịch sử xem thành công");

    log.info("<Result API> => Adding history watched successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
