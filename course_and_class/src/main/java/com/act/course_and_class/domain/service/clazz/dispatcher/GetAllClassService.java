package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetAllClassService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
