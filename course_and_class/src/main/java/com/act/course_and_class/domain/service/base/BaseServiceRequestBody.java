package com.act.course_and_class.domain.service.base;

public interface BaseServiceRequestBody<I, O> {

  void validate(I request);

  O process(I request);

  default O execute(I request) {
    validate(request);
    return process(request);
  }

}
