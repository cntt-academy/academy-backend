package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.Infrastructure.utils.RedisUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.ClassOutputDto;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetAllClassService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllClassService")
public class GetAllClassServiceImpl implements GetAllClassService {

  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL CLASS ========");

    List<Clazz> classes = classRepository.findAll();
    log.info("<Query> => Get all class result: {}", classes);

    List<ClassOutputDto> classOutputDtos = genericMapper.mapToListClassOutputDto(classes);

    // Lưu vào Redis
    String dataClass;
    ClassOutputDto clazz;
    RedisUtils.set("TOTAL_CLASS", String.valueOf(classes.size()));
    for (int i = 1; i <= classOutputDtos.size(); i++) {
      clazz = classOutputDtos.get(i - 1);
      dataClass = clazz.getId() + "," + clazz.getName();
      RedisUtils.set("CLASS_" + i, dataClass);
    }

    Map<String, Object> data =
        Map.of("classes", classOutputDtos, "totalElements", classes.size());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách tất cả lớp học và lưu vào Redis thành công");
    response.setData(data);

    log.info("<Result API> => Getting all class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
