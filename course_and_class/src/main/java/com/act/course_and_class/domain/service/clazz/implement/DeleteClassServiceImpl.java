package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.DeleteClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetAllClassService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteClassService")
public class DeleteClassServiceImpl implements DeleteClassService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final GetAllClassService getAllClassService;
  private final ClassRepository classRepository;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private Clazz clazz;

  @Override
  public ResponseEntity<?> execute(Integer classId) {
    log.info("======== API DELETE CLASS ========");
    log.info("Class Id: {}", classId);
    return DeleteClassService.super.execute(classId);
  }

  @Override
  public void validate(Integer classId) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa lớp học");
    }

    clazz = classRepository.findById(classId);
    log.info("<Query> => Result finding class by id: {}", clazz);
    if (clazz == null) {
      throw new BadRequestException("Không tìm thấy lớp học muốn xóa");
    }
    if (ClassStateEnums.PREP.name().equals(clazz.getState())) {
      throw new BadRequestException("Không được phép xóa lớp học dự bị");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer courseId) {
    clazz.setState(ClassStateEnums.DELETED.name());
    clazz.setUpdatedBy(loggedAccount.get("name"));
    clazz.setUpdatedDate(new Date());
    classRepository.save(clazz);

    // Set lại redis danh sách lớp học
    CompletableFuture.runAsync(getAllClassService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa lớp học thành công");

    log.info("<Result API> => Deleting class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
