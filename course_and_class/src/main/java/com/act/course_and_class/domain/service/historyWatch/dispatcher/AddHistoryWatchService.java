package com.act.course_and_class.domain.service.historyWatch.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface AddHistoryWatchService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
