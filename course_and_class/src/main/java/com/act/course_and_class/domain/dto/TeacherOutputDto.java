package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.Teacher} entity
 */
@Data
public class TeacherOutputDto implements Serializable {

  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String phone;
  private String email;
  private String state;
  private String createdBy;
  private String createdDate;
}