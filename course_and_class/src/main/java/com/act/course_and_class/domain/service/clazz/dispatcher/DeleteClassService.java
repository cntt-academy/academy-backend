package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface DeleteClassService extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
