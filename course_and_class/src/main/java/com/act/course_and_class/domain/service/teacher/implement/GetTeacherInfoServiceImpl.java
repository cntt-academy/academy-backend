package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.AESUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.TeacherOutputDto;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetTeacherInfoService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetTeacherInfoService")
public class GetTeacherInfoServiceImpl implements GetTeacherInfoService {

  private final TeacherRepository teacherRepository;
  private final GenericMapper genericMapper;
  private final AESUtils aesUtils;
  private final Gson gson;
  private Teacher teacher;

  @Override
  public ResponseEntity<?> execute(String teacherId) {
    log.info("======== API GET TEACHER INFO ========");
    log.info("Teacher Id: {}", teacherId);
    return GetTeacherInfoService.super.execute(teacherId);
  }

  @Override
  public void validate(String teacherId) {
    teacher = teacherRepository.findById(Integer.valueOf(aesUtils.decrypt(teacherId)));
    log.info("<Query> => Result finding teacher by id: {}", teacher);
    if (teacher == null) {
      throw new BadRequestException("Không tìm thấy thông tin giảng viên");
    }
  }

  @Override
  public ResponseEntity<?> process(String teacherId) {
    TeacherOutputDto teacherOutputDto = genericMapper.mapToTeacherOutputDto(teacher);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin giảng viên thành công");
    response.setData(Map.of("teacher", teacherOutputDto));
    log.info("<Result API> => Getting teacher info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
