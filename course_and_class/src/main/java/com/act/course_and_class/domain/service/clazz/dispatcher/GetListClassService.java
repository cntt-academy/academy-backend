package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.application.request.clazz.GetListClassRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListClassService extends
    BaseServiceRequestBody<GetListClassRequest, ResponseEntity<?>> {

}
