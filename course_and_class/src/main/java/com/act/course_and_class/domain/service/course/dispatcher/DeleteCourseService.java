package com.act.course_and_class.domain.service.course.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface DeleteCourseService extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
