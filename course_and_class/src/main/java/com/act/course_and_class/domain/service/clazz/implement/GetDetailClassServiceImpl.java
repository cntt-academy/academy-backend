package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.ClassOutputDto;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetDetailClassService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailClassService")
public class GetDetailClassServiceImpl implements GetDetailClassService {

  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Clazz clazz;

  @Override
  public ResponseEntity<?> execute(Integer classId) {
    log.info("======== API GET DETAIL CLASS ========");
    log.info("Class Id want to get detail: {}", classId);
    return GetDetailClassService.super.execute(classId);
  }

  @Override
  public void validate(Integer classId) {
    clazz = classRepository.findById(classId);
    log.info("<Query> => Find class by id result: {}", clazz);
    if (clazz == null) {
      throw new BadRequestException("Lớp học không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer courseId) {
    ClassOutputDto classOutputDto = genericMapper.mapToClassOutputDto(clazz);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin lớp học thành công");
    response.setData(Map.of("class", classOutputDto));

    log.info("<Result API> => Getting class info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
