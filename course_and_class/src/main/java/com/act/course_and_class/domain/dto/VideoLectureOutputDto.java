package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.VideoLecture} entity
 */
@Data
public class VideoLectureOutputDto implements Serializable {

  private Integer id;
  private Integer videoOrder;
  private String name;
  private String desc;
  private String url;
  private Integer classId;
  private String state;
  private String createdBy;
  private String createdDate;
}