package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.DeleteRegisterService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteRegisterService")
public class DeleteRegisterServiceImpl implements DeleteRegisterService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final ClassRepository classRepository;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Register register;
  private Clazz clazz;

  @Override
  public ResponseEntity<?> execute(Integer registerId) {
    log.info("======== API DELETE REGISTER ========");
    log.info("Register Id: {}", registerId);
    return DeleteRegisterService.super.execute(registerId);
  }

  @Override
  public void validate(Integer registerId) {
    register = registerRepository.findById(registerId);
    log.info("<Query> => Result finding register by id: {}", register);
    if (register == null) {
      throw new BadRequestException("Không tìm thấy thông tin đăng ký muốn xóa");
    }

    clazz = classRepository.findById(register.getClassId());
    log.info("<Query> => Result finding class by id: {}", clazz);
    if (clazz == null) {
      throw new BadRequestException("Không tìm thấy thông tin lớp học");
    }

    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))
        && !clazz.getSupporterEmail().equalsIgnoreCase(loggedAccount.get("email"))
        && !clazz.getManagerEmail().equalsIgnoreCase(loggedAccount.get("email"))) {
      throw new BadRequestException("Bạn không có quyền xóa thông tin đăng ký");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer registerId) {
    register.setState(RegisterStateEnums.DELETED.name());
    register.setUpdatedBy(loggedAccount.get("name"));
    register.setUpdatedDate(new Date());
    registerRepository.save(register);

    Integer numberStudent = clazz.getNumberStudent();
    clazz.setNumberStudent(numberStudent - 1);
    clazz.setUpdatedBy(loggedAccount.get("name"));
    classRepository.save(clazz);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa thông tin đăng ký thành công");

    log.info("<Result API> => Deleting register successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
