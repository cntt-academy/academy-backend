package com.act.course_and_class.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "classes")
public class Clazz {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Lob
  @Column(name = "full_name", nullable = false)
  private String fullName;

  @Size(max = 50)
  @NotNull
  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Size(max = 10)
  @Column(name = "how_to_learn", nullable = false, length = 10)
  private String howToLearn;

  @Lob
  @Column(name = "url_class")
  private String urlClass;

  @NotNull
  @Column(name = "number_student", nullable = false)
  private Integer numberStudent;

  @Column(name = "date_opening", nullable = false)
  private Date dateOpening;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "teacher_id", nullable = false)
  private Teacher teacher;

  @Size(max = 50)
  @Column(name = "manager", nullable = false, length = 50)
  private String manager;

  @Size(max = 50)
  @Column(name = "manager_email", nullable = false, length = 50)
  private String managerEmail;

  @Size(max = 50)
  @Column(name = "supporter", nullable = false, length = 50)
  private String supporter;

  @Size(max = 50)
  @Column(name = "supporter_email", nullable = false, length = 50)
  private String supporterEmail;

  @NotNull
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "course_id", nullable = false)
  private Course course;

  @Lob
  @Column(name = "special")
  private String special;

  @Size(max = 10)
  @NotNull
  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Size(max = 50)
  @NotNull
  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Size(max = 50)
  @NotNull
  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @NotNull
  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "Clazz{" +
        "id=" + id +
        ", fullName='" + fullName + '\'' +
        ", name='" + name + '\'' +
        ", howToLearn='" + howToLearn + '\'' +
        ", numberStudent=" + numberStudent +
        ", dateOpening=" + dateOpening +
        ", teacherId=" + (teacher != null ? teacher.getId().toString() : 0) +
        ", manager='" + manager + '\'' +
        ", supporter='" + supporter + '\'' +
        ", courseId=" + course.getId() +
        ", special='" + special + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}