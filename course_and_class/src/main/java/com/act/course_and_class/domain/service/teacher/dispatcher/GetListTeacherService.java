package com.act.course_and_class.domain.service.teacher.dispatcher;

import com.act.course_and_class.application.request.teacher.GetListTeacherRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListTeacherService
    extends BaseServiceRequestBody<GetListTeacherRequest, ResponseEntity<?>> {

}
