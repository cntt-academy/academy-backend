package com.act.course_and_class.domain.service.course.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetAllCourseService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
