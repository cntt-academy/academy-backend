package com.act.course_and_class.domain.service.register.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface DeleteRegisterService extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
