package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.ProcessException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.UnirestUtils;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.request.register.RegisterRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.RegisterService;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("RegisterService")
public class RegisterServiceImpl implements RegisterService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final ClassRepository classRepository;
  private final EurekaClient eurekaClient;
  private final Gson gson;

  @Value("${path.sendEmail.userAddedIntoClass}")
  private String pathSendEmailUserAddedIntoClass;
  @Value("${path.sendEmail.userRegistered}")
  private String pathSendEmailUserRegistered;


  @Override
  public ResponseEntity<?> execute(RegisterRequest request) {
    log.info("======== API REGISTER ========");
    log.info("Request register study: {}", gson.toJson(request));
    return RegisterService.super.execute(request);
  }

  @Override
  public void validate(RegisterRequest request) {
  }

  @Override
  public ResponseEntity<?> process(RegisterRequest request) {
    // Kiểm tra học viên có tồn tại ?
    Map<String, Object> user = courseAndClassUtils.getUserByEmail(request.getUserEmail());
    if (user == null) {
      throw new ProcessException("Thông tin học viên không hợp lệ");
    }

    // Nếu truyền xuống email admin = > Kiểm tra admin có tồn tại ?
    Map<String, Object> collaborator = null;
    if (StringUtils.isNotBlank(request.getAdminEmail())) {
      collaborator = courseAndClassUtils.getAdminByEmail(request.getAdminEmail());
      if (collaborator == null) {
        throw new ProcessException("Thông tin cộng tác viên không hợp lệ");
      }
    }

    Clazz clazz = null;

    // Nếu có truyền xuống classId => Tìm kiếm thông tin lớp học
    if (request.getClassId() != null) {
      clazz = classRepository.findById(request.getClassId());
      log.info("<Query> => Result finding class by id: {}", clazz);
    }

    // Nếu không truyền xuống classId nhưng có truyền courseId và howToLearn
    if (clazz == null
        && request.getCourseId() != null
        && StringUtils.isNotBlank(request.getHowToLearn())) {

      // Kiểm tra howToLearn truyền xuống có hợp lệ ?
      String howToLearn;
      if (HowToLearnEnums.Offline.name().equalsIgnoreCase(request.getHowToLearn())) {
        howToLearn = HowToLearnEnums.Offline.name();
      } else if (HowToLearnEnums.Online.name().equalsIgnoreCase(request.getHowToLearn())) {
        howToLearn = HowToLearnEnums.Online.name();
      } else {
        throw new ProcessException("Hình thức học không hợp lệ");
      }

      // Tìm kiếm các lớp sắp khai giảng ứng với courseId và howToLearn truyền xuống
      List<Clazz> classes =
          classRepository.findAllByCourseIdAndHowToLearnAndState(
              request.getCourseId(), howToLearn, ClassStateEnums.WAIT.name());
      log.info(
          "<Query> => Result finding list class by courseId and howToLearn and state WAIT: {}",
          classes);

      // Nếu không tìm thấy lớp sắp khai giảng => Cho vào lớp dự bị
      if (CollectionUtils.isEmpty(classes)) {
        clazz =
            classRepository.findByCourseIdAndState(
                request.getCourseId(), ClassStateEnums.PREP.name());
        log.info("<Query> => Result finding list class by courseId and state PREP: {}", classes);
      } else {
        clazz = classes.get(0);
      }
    }

    // Kiểm tra lớp học có tồn tại ?
    if (clazz == null) {
      throw new ProcessException("Không tìm thấy thông tin lớp học");
    }

    // Kiểm tra đã có thông tin đăng ký của học viên chưa ?
    Register registerInfo =
        registerRepository.findByStudentIdAndClassId((Integer) user.get("id"), clazz.getId());
    log.info("<Query> => Result finding register info by studentId and classId: {}", registerInfo);
    if (registerInfo != null) {
      throw new ProcessException("Thông tin học viên đã có trong lớp học");
    }

    // Thêm thông tin đăng ký vào bảng Register
    Register register = new Register();
    register.setStudentId((Integer) user.get("id"));
    register.setStudentName((String) user.get("name"));
    register.setClassId(clazz.getId());
    register.setClassName(clazz.getName());
    register.setState(RegisterStateEnums.NEW.name());
    register.setCreatedDate(new Date());
    register.setUpdatedDate(new Date());
    if (collaborator != null) {
      register.setCollaboratorId((Integer) collaborator.get("id"));
      register.setCollaboratorName((String) collaborator.get("name"));
      register.setCreatedBy((String) collaborator.get("name"));
      register.setUpdatedBy((String) collaborator.get("name"));
    } else {
      register.setCreatedBy((String) user.get("name"));
      register.setUpdatedBy((String) user.get("name"));
    }
    registerRepository.save(register);

    // Cập nhật số lượng học sinh trong lớp
    clazz.setNumberStudent(clazz.getNumberStudent() + 1);
    clazz.setUpdatedDate(new Date());
    classRepository.save(clazz);

    if (request.getClassId() != null && collaborator != null) {
      sendEmailUserAddedIntoClass(user, collaborator, clazz);
    } else {
      sendEmailUserRegistered(user, clazz.getCourse().getName(), clazz);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Đăng ký học thành công");

    log.info("<Result API> => Registering successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  private void sendEmailUserAddedIntoClass(
      Map<String, Object> user, Map<String, Object> collaborator, Clazz clazz) {
    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailUserAddedIntoClass = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailUserAddedIntoClass;

          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

          Map<String, Object> body = new HashMap<>();
          body.put("userName", user.get("name"));
          body.put("userEmail", user.get("email"));
          body.put("className", clazz.getName());
          body.put("classManagerEmail", clazz.getManagerEmail());
          body.put("collaboratorName", collaborator.get("name"));
          body.put("collaboratorEmail", collaborator.get("email"));
          body.put("time", sdf.format(new Date()));

          UnirestUtils.postByBodyWithoutHeader(urlSendEmailUserAddedIntoClass, gson.toJson(body));
        });
  }

  private void sendEmailUserRegistered(
      Map<String, Object> user, String courseName, Clazz clazz) {
    CompletableFuture.runAsync(
        () -> {
          String urlSendEmailUserRegistered = eurekaClient
              .getNextServerFromEureka("message", false).getHomePageUrl()
              + "email" + pathSendEmailUserRegistered;

          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

          Map<String, Object> body = new HashMap<>();
          body.put("name", user.get("name"));
          body.put("email", user.get("email"));
          body.put("course", courseName);
          body.put("howToLearn", clazz.getHowToLearn());
          body.put("className", clazz.getName());
          body.put("classManagerEmail", clazz.getManagerEmail());
          body.put("time", sdf.format(new Date()));
          if (ClassStateEnums.PREP.name().equalsIgnoreCase(clazz.getState())) {
            body.put("isPrepClass", true);
          } else {
            body.put("isPrepClass", false);
          }

          UnirestUtils.postByBodyWithoutHeader(urlSendEmailUserRegistered, gson.toJson(body));
        });
  }
}
