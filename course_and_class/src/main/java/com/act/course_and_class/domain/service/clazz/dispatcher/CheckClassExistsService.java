package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.application.request.clazz.CheckClassExistsRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CheckClassExistsService
    extends BaseServiceRequestBody<CheckClassExistsRequest, ResponseEntity<?>> {

}
