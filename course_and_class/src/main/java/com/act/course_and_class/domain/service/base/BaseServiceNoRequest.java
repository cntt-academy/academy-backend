package com.act.course_and_class.domain.service.base;

public interface BaseServiceNoRequest<O> {

  O execute();
}
