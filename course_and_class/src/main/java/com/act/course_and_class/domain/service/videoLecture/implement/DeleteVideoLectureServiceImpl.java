package com.act.course_and_class.domain.service.videoLecture.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.DeleteVideoLectureService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteVideoLectureService")
public class DeleteVideoLectureServiceImpl implements DeleteVideoLectureService {

  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final Gson gson;
  private VideoLecture videoLecture;

  @Override
  public ResponseEntity<?> execute(Integer videoLectureId) {
    log.info("======== API DELETE VIDEO LECTURE ========");
    log.info("Video lecture Id : {}", videoLectureId);
    return DeleteVideoLectureService.super.execute(videoLectureId);
  }

  @Override
  public void validate(Integer videoLectureId) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa bài giảng");
    }

    videoLecture = videoLectureRepository.findById(videoLectureId);
    log.info("<Query> => Find video lecture by id result: {}", videoLecture);
    if (videoLecture == null) {
      throw new BadRequestException("Không tìm thấy bài giảng muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer videoLectureId) {
    videoLectureRepository.delete(videoLecture);

    List<VideoLecture> videoLectures =
        videoLectureRepository.findAllByClassId(videoLecture.getClassId());
    int order = 1;
    for (VideoLecture item : videoLectures) {
      item.setVideoOrder(order++);
    }
    videoLectureRepository.saveAll(videoLectures);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa bài giảng thành công");

    log.info("<Result API> => Deleting video lecture successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
