package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.AESUtils;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.DeleteTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetAllTeacherService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteTeacherService")
public class DeleteTeacherServiceImpl implements DeleteTeacherService {

  private final GetAllTeacherService getAllTeacherService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final TeacherRepository teacherRepository;
  private final AESUtils aesUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private Teacher teacher;

  @Override
  public ResponseEntity<?> execute(String teacherId) {
    log.info("======== API DELETE TEACHER ========");
    log.info("Teacher Id: {}", teacherId);
    return DeleteTeacherService.super.execute(teacherId);
  }

  @Override
  public void validate(String teacherId) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa giảng viên");
    }

    teacher = teacherRepository.findById(Integer.valueOf(aesUtils.decrypt(teacherId)));
    log.info("<Query> => Find teacher by id result: {}", teacher);
    if (teacher == null) {
      throw new BadRequestException("Không tìm thấy giảng viên muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(String teacherId) {
    teacher.setState(StateEnums.DELETED.name());
    teacher.setUpdatedBy(loggedAccount.get("name"));
    teacher.setUpdatedDate(new Date());
    teacherRepository.save(teacher);

    // Set lại redis danh sách giảng viên
    CompletableFuture.runAsync(getAllTeacherService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa giảng viên thành công");

    log.info("<Result API> => Deleting teacher successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
