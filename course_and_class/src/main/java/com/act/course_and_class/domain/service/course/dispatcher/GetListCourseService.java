package com.act.course_and_class.domain.service.course.dispatcher;

import com.act.course_and_class.application.request.course.GetListCourseRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListCourseService extends
    BaseServiceRequestBody<GetListCourseRequest, ResponseEntity<?>> {

}
