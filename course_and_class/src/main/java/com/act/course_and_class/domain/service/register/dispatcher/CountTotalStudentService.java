package com.act.course_and_class.domain.service.register.dispatcher;

import com.act.course_and_class.application.request.register.CountTotalStudentRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CountTotalStudentService extends
    BaseServiceRequestBody<CountTotalStudentRequest, ResponseEntity<?>> {

}
