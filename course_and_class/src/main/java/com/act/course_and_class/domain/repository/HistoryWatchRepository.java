package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.HistoryWatch;
import java.util.List;

public interface HistoryWatchRepository {

  List<HistoryWatch> findAllByVideoLectureIdAndUserEmail(Integer videoLectureId, String userEmail);
  List<Integer> findAllVideoIdByClassIdAndUserEmail(Integer classId, String userEmail);
  void save(HistoryWatch historyWatch);
}
