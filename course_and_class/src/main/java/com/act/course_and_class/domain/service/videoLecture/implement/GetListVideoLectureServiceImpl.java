package com.act.course_and_class.domain.service.videoLecture.implement;

import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.enums.RoleEnums;
import com.act.course_and_class.application.enums.VideoLectureEnums;
import com.act.course_and_class.application.request.videoLecture.GetListVideoLectureRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.VideoLectureOutputDto;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.HistoryWatchRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.GetListVideoLectureService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListVideoLectureService")
public class GetListVideoLectureServiceImpl implements GetListVideoLectureService {

  private final HistoryWatchRepository historyWatchRepository;
  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListVideoLectureRequest request) {
    log.info("======== API GET LIST VIDEO LECTURE ========");
    log.info("Request get list video lecture: {}", gson.toJson(request));
    return GetListVideoLectureService.super.execute(request);
  }

  @Override
  public void validate(GetListVideoLectureRequest request) {
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(GetListVideoLectureRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();

    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<VideoLecture> videoLectures = videoLectureRepository.findAllByCondition(
        request.getClassId(),
        courseAndClassUtils.standardizedParameterQueryLike(request.getName()),
        pageRequest);
    log.info("<Query> => Result getting list video lecture by conditions: {}",
        gson.toJson(videoLectures));

    CompletableFuture<List<?>> getVideoLectures = CompletableFuture.supplyAsync(() -> {
      List<VideoLectureOutputDto> videoLectureOutputDtos
          = genericMapper.mapToListVideoLectureOutputDto(videoLectures.getContent());

      if (!RoleEnums.User.name().equals(loggedAccount.get("role"))) {
        return videoLectureOutputDtos;
      }
      List<VideoLectureOutputDto> tempVideoLectureOutputDtos = new ArrayList<>();

      Map<String, Object> user = courseAndClassUtils.getUserByEmail(loggedAccount.get("email"));
      Register register =
          registerRepository.findByStudentIdAndClassId(
              (Integer) user.get("id"), request.getClassId());
      String registerState = register.getState();

      List<Integer> videoWatchedIds = historyWatchRepository
          .findAllVideoIdByClassIdAndUserEmail(request.getClassId(), loggedAccount.get("email"));

      videoLectureOutputDtos.forEach(item -> {
        if (videoWatchedIds.contains(item.getId())) {
          item.setState(VideoLectureEnums.WATCHED.getValue());
        } else {
          item.setState(VideoLectureEnums.WAIT.getValue());
        }
        if ((RegisterStateEnums.NEW.name().equals(registerState)
            || RegisterStateEnums.WAIT.name().equals(registerState))
            && item.getVideoOrder() >= 3) {
          item.setState(VideoLectureEnums.BLOCKED.getValue());
        } else if (RegisterStateEnums.PAY_HALF.name().equals(registerState)
            && item.getVideoOrder() >= 5) {
          item.setState(VideoLectureEnums.BLOCKED.getValue());
        }
        tempVideoLectureOutputDtos.add(item);
      });

      return tempVideoLectureOutputDtos;
    });

    Map<String, Object> data =
        Map.of("videoLectures", getVideoLectures.get(),
            "totalElements", videoLectures.getTotalElements(),
            "totalPages", videoLectures.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách video bài giảng thành công");
    response.setData(data);

    log.info("<Result API> =>  Getting list video lecture successfully: {}", gson.toJson(response));
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
