package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.request.clazz.CreatePrepClassRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.CreatePrepClassService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreatePrepClassService")
public class CreatePrepClassServiceImpl implements CreatePrepClassService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final CourseRepository courseRepository;
  private final ClassRepository classRepository;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Course course;

  @Override
  public ResponseEntity<?> execute(CreatePrepClassRequest request) {
    log.info("======== API CREATE PREP CLASS ========");
    log.info("Request create prep class: {}", gson.toJson(request));
    return CreatePrepClassService.super.execute(request);
  }

  @Override
  public void validate(CreatePrepClassRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền tạo lớp học dự bị");
    }

    course = courseRepository.findById(request.getCourse().getId());
    log.info("<Query> => Result finding course by id: {}", course);
    if (course == null) {
      throw new BadRequestException("Không tìm thấy khóa học");
    }
  }

  @Override
  public ResponseEntity<?> process(CreatePrepClassRequest request) {
    Clazz clazz = new Clazz();
    clazz.setFullName("Lớp dự bị " + course.getName());
    clazz.setName("Prep_course_" + course.getId());
    clazz.setNumberStudent(0);
    clazz.setCourse(course);
    clazz.setState(ClassStateEnums.PREP.name());
    clazz.setCreatedBy(loggedAccount.get("name"));
    clazz.setCreatedDate(new Date());
    clazz.setUpdatedBy(loggedAccount.get("name"));
    clazz.setUpdatedDate(new Date());
    classRepository.save(clazz);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm lớp học dự bị thành công");

    log.info("<Result API> => Creating prep class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
