package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.Teacher;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface TeacherRepository {

  Page<Teacher> findAllByCondition(String search, PageRequest pageRequest);

  Teacher findByEmail(String email);

  Teacher findById(Integer teacherId);

  void save(Teacher teacher);

  List<Teacher> findAll();
}
