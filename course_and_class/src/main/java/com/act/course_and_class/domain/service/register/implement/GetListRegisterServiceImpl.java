package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.RoleEnums;
import com.act.course_and_class.application.request.register.GetListRegisterRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.RegisterOutputDto;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.GetListRegisterService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListRegisterService")
public class GetListRegisterServiceImpl implements GetListRegisterService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListRegisterRequest request) {
    log.info("======== API GET LIST REGISTER ========");
    log.info("Request get list register: {}", gson.toJson(request));
    return GetListRegisterService.super.execute(request);
  }

  @Override
  public void validate(GetListRegisterRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (RoleEnums.User.name().equals(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền lấy ra danh sách đăng ký học");
    }
  }

  @Override
  public ResponseEntity<?> process(GetListRegisterRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<Register> registers =
        registerRepository.findAllByClassId(request.getClassId(), pageRequest);

    List<RegisterOutputDto> registerOutputDtos =
        genericMapper.mapToListRegisterOutputDto(registers.getContent());
    log.info(
        "<Query> => Get list register by class id result: {}", gson.toJson(registerOutputDtos));

    Map<String, Object> data =
        Map.of("registers", registerOutputDtos,
            "totalElements", registers.getTotalElements(),
            "totalPages", registers.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách đăng ký học thành công");
    response.setData(data);

    log.info("<Result API> => Getting list register successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
