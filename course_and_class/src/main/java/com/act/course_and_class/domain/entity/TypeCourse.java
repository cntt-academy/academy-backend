package com.act.course_and_class.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "type_courses")
public class TypeCourse {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @NotNull
  @Lob
  @Column(name = "name", nullable = false)
  private String name;

  @NotNull
  @Lob
  @Column(name = "`desc`", nullable = false)
  private String desc;

  @Size(max = 10)
  @NotNull
  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Size(max = 50)
  @NotNull
  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @NotNull
  @Column(name = "created_date", nullable = false)
  private Date createdDate;

  @Size(max = 50)
  @NotNull
  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @NotNull
  @Column(name = "updated_date", nullable = false)
  private Date updatedDate;

  @Override
  public String toString() {
    return "TypeCourse{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", desc='" + desc + '\'' +
        ", state='" + state + '\'' +
        ", createdBy='" + createdBy + '\'' +
        ", createdDate=" + createdDate +
        ", updatedBy='" + updatedBy + '\'' +
        ", updatedDate=" + updatedDate +
        '}';
  }
}