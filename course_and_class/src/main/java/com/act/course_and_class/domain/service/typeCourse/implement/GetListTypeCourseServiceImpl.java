package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.request.typeCourse.GetListTypeCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.TypeCourseOutputDto;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetListTypeCourseService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListTypeCourseService")
public class GetListTypeCourseServiceImpl implements GetListTypeCourseService {

  private final TypeCourseRepository typeCourseRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListTypeCourseRequest request) {
    log.info("======== API GET LIST TYPE COURSE ========");
    log.info("Request get list type course: {}", gson.toJson(request));
    return GetListTypeCourseService.super.execute(request);
  }

  @Override
  public void validate(GetListTypeCourseRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListTypeCourseRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<TypeCourse> typeCourses = typeCourseRepository.findAllByCondition(
        courseAndClassUtils.standardizedParameterQueryLike(request.getName()),
        pageRequest);
    log.info("<Query> => Result getting list type courses by conditions: {}", gson.toJson(typeCourses));

    List<TypeCourseOutputDto> typeCourseOutputDtos =
        genericMapper.mapToListTypeCourseOutputDto(typeCourses.getContent());

    Map<String, Object> data =
        Map.of("typeCourses", typeCourseOutputDtos,
            "totalElements", typeCourses.getTotalElements(),
            "totalPages", typeCourses.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy danh sách loại khóa học thành công");
    response.setData(data);

    log.info("<Result API> =>  Getting list type course successfully: {}", gson.toJson(response));
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
