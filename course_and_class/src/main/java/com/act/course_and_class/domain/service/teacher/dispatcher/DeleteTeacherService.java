package com.act.course_and_class.domain.service.teacher.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface DeleteTeacherService
    extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
