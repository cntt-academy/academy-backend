package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.Clazz;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface ClassRepository {

  List<Clazz> findAllByCourseIdAndHowToLearnAndState(
      Integer courseId, String howToLearn, String state);

  Page<Clazz> findAllByConditions(
      String search, String state, String howToLearn, PageRequest pageRequest);

  Page<Clazz> findAllByUser(
      Integer studentId, String search, String state, String stateNot, PageRequest pageRequest);

  Page<Clazz> findAllByTeacher(Integer teacherId, PageRequest pageRequest);

  Page<Clazz> findAllByAdmin(String adminEmail, PageRequest pageRequest);

  Clazz findByCourseIdAndState(Integer courseId, String state);

  Clazz findByFullName(String fullName);

  Clazz findById(Integer classId);

  Clazz findByName(String name);

  void saveAll(List<Clazz> classes);

  void save(Clazz clazz);

  List<Clazz> findAll();
}
