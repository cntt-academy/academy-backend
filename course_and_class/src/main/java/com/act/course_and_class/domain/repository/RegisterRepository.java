package com.act.course_and_class.domain.repository;

import com.act.course_and_class.Infrastructure.projection.StudentStatisticProjection;
import com.act.course_and_class.domain.entity.Register;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface RegisterRepository {

  List<StudentStatisticProjection> overviewStatisticStudent(Date fromDate, Date toDate);

  Page<StudentStatisticProjection> detailStatisticStudent(
      Date fromDate, Date toDate, PageRequest pageRequest);

  Page<Register> findAllByClassId(Integer classId, PageRequest pageRequest);

  Register findByStudentIdAndClassId(Integer studentId, Integer classId);

  List<Register> findAllByStudentId(Integer studentId);

  Long countTotalStudent(Date fromDate, Date toDate);

  Register findById(Integer registerId);

  void saveAll(List<Register> registers);

  void save(Register register);
}
