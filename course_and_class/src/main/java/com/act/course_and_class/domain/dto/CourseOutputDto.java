package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.Course} entity
 */
@Data
public class CourseOutputDto implements Serializable {

  private Integer id;
  private String img;
  private String name;
  private String desc;
  private String content;
  private Double price;
  private Integer totalHourStudy;
  private String type;
  private String createdBy;
  private String createdDate;
}