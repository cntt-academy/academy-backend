package com.act.course_and_class.domain.service.course.dispatcher;

import com.act.course_and_class.application.request.course.UpdateCourseRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateCourseService extends
    BaseServiceRequestBody<UpdateCourseRequest, ResponseEntity<?>> {

}
