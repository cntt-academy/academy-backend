package com.act.course_and_class.domain.service.videoLecture.dispatcher;

import com.act.course_and_class.application.request.typeCourse.UpdateTypeCourseRequest;
import com.act.course_and_class.application.request.videoLecture.UpdateVideoLectureRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateVideoLectureService
    extends BaseServiceRequestBody<UpdateVideoLectureRequest, ResponseEntity<?>> {

}
