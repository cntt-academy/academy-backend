package com.act.course_and_class.domain.service.videoLecture.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.request.videoLecture.UpdateVideoLectureRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.UpdateVideoLectureService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateVideoLectureService")
public class UpdateVideoLectureServiceImpl implements UpdateVideoLectureService {

  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private VideoLecture videoLecture;

  @Override
  public ResponseEntity<?> execute(UpdateVideoLectureRequest request) {
    log.info("======== API UPDATE VIDEO LECTURE ========");
    log.info("Request update video lecture: {}", gson.toJson(request));
    return UpdateVideoLectureService.super.execute(request);
  }

  @Override
  public void validate(UpdateVideoLectureRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa bài giảng");
    }

    videoLecture = videoLectureRepository.findById(request.getId());
    log.info("<Query> => Result finding video lecture by id: {}", videoLecture);
    if (videoLecture == null) {
      throw new BadRequestException("Không tìm thấy thông tin bài giảng muốn cập nhật");
    }

    if (StringUtils.isNotBlank(request.getName())) {
      VideoLecture videoLecture = videoLectureRepository.findByName(request.getName());
      log.info("<Query> => Result finding video lecture by name: {}", videoLecture);
      if (videoLecture != null && !Objects.equals(videoLecture.getId(), request.getId())) {
        throw new BadRequestException("Tên bài giảng đã tồn tại");
      }
    }
    if (StringUtils.isNotBlank(request.getUrl())) {
      VideoLecture videoLecture = videoLectureRepository.findByUrl(request.getUrl());
      log.info("<Query> => Result finding video lecture by url: {}", videoLecture);
      if (videoLecture != null && !Objects.equals(videoLecture.getId(), request.getId())) {
        throw new BadRequestException("Bài giảng đã tồn tại");
      }
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateVideoLectureRequest request) {
    if (StringUtils.isNotBlank(request.getName())) {
      videoLecture.setName(request.getName());
    }
    if (StringUtils.isNotBlank(request.getDesc())) {
      videoLecture.setDesc(request.getDesc());
    }
    if (StringUtils.isNotBlank(request.getUrl())) {
      videoLecture.setUrl(request.getUrl());
    }
    videoLecture.setUpdatedBy(loggedAccount.get("name"));
    videoLecture.setUpdatedDate(new Date());
    videoLectureRepository.save(videoLecture);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật bài giảng thành công");

    log.info("<Result API> => Updating video lecture successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
