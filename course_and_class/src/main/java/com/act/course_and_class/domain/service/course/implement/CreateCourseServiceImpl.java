package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.request.clazz.CreatePrepClassRequest;
import com.act.course_and_class.application.request.course.CreateCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.CreatePrepClassService;
import com.act.course_and_class.domain.service.course.dispatcher.CreateCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.GetAllCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateCourseService")
public class CreateCourseServiceImpl implements CreateCourseService {

  private final CreatePrepClassService createPrepClassService;
  private final TypeCourseRepository typeCourseRepository;
  private final GetAllCourseService getAllCourseService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final CourseRepository courseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(CreateCourseRequest request) {
    log.info("======== API CREATE COURSE ========");
    log.info("Request create course: {}", gson.toJson(request));
    return CreateCourseService.super.execute(request);
  }

  @Override
  public void validate(CreateCourseRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền tạo khóa học");
    }

    TypeCourse typeCourse = typeCourseRepository.findById(request.getTypeId());
    log.info("<Query> => Result finding type course by id: {}", typeCourse);
    if (typeCourse == null) {
      throw new BadRequestException("Không tìm thấy loại khóa học");
    }

    Course course = courseRepository.findByName(request.getName());
    log.info("<Query> => Result finding course by name: {}", course);
    if (course != null) {
      throw new BadRequestException("Đã tồn tại khóa học có tên giống với yêu cầu");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateCourseRequest request) {
    Course course = genericMapper.mapToType(request, Course.class);
    course.setState(StateEnums.ACTIVE.name());
    course.setCreatedBy(loggedAccount.get("name"));
    course.setCreatedDate(new Date());
    course.setUpdatedBy(loggedAccount.get("name"));
    course.setUpdatedDate(new Date());
    courseRepository.save(course);

    // Tạo lớp học dự bị
    createPrepClassService.execute(new CreatePrepClassRequest(course));

    // Set lại redis danh sách khóa học
    CompletableFuture.runAsync(getAllCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm khóa học mới thành công");

    log.info("<Result API> => Creating course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
