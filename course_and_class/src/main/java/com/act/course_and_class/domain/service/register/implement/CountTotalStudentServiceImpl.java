package com.act.course_and_class.domain.service.register.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.request.register.CountTotalStudentRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.register.dispatcher.CountTotalStudentService;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CountTotalStudentService")
public class CountTotalStudentServiceImpl implements CountTotalStudentService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(CountTotalStudentRequest request) {
    log.info("======== API COUNT TOTAL STUDENT ========");
    log.info("Request: {}", request);
    return CountTotalStudentService.super.execute(request);
  }

  @Override
  public void validate(CountTotalStudentRequest request) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền lấy ra tổng số học viên");
    }

  }

  @Override
  public ResponseEntity<?> process(CountTotalStudentRequest request) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    Date fromDate = null;
    if (StringUtils.isNotBlank(request.getFromDate())) {
      try {
        fromDate = sdf.parse(request.getFromDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày bắt đầu không hợp lệ");
      }
    }

    Date toDate = null;
    if (StringUtils.isNotBlank(request.getToDate())) {
      try {
        toDate = sdf.parse(request.getToDate());
      } catch (ParseException e) {
        throw new BadRequestException("Ngày kết thúc không hợp lệ");
      }
    }

    Long totalStudent =
        registerRepository.countTotalStudent(fromDate, toDate);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra tổng số học viên thành công");
    response.setData(Map.of("totalStudent", totalStudent));

    log.info("<Result API> => Counting total student successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
