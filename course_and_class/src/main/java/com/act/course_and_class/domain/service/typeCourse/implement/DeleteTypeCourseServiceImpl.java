package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.DeleteTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetAllTypeCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteTypeCourseService")
public class DeleteTypeCourseServiceImpl implements DeleteTypeCourseService {

  private final GetAllTypeCourseService getAllTypeCourseService;
  private final TypeCourseRepository typeCourseRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private TypeCourse typeCourse;

  @Override
  public ResponseEntity<?> execute(Integer typeCourseId) {
    log.info("======== API DELETE TYPE COURSE ========");
    log.info("Type course Id : {}", typeCourseId);
    return DeleteTypeCourseService.super.execute(typeCourseId);
  }

  @Override
  public void validate(Integer typeCourseId) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền xóa loại khóa học");
    }

    typeCourse = typeCourseRepository.findById(typeCourseId);
    log.info("<Query> => Find type course by id result: {}", typeCourse);
    if (typeCourse == null) {
      throw new BadRequestException("Không tìm thấy loại khóa học muốn xóa");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer typeCourseId) {
    typeCourse.setState(StateEnums.DELETED.name());
    typeCourse.setUpdatedBy(loggedAccount.get("name"));
    typeCourse.setUpdatedDate(new Date());
    typeCourseRepository.save(typeCourse);

    // Set lại redis danh sách loại khóa học
    CompletableFuture.runAsync(getAllTypeCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa loại khóa học thành công");

    log.info("<Result API> => Deleting type course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
