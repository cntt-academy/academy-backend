package com.act.course_and_class.domain.service.internal.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.request.register.DeleteAllRegisterRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.service.internal.dispatcher.DeleteAllRegisterService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteAllRegisterService")
public class DeleteAllRegisterServiceImpl implements DeleteAllRegisterService {

  private final RegisterRepository registerRepository;
  private final ClassRepository classRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(DeleteAllRegisterRequest request) {
    log.info("======== API DELETE ALL REGISTER BY STUDENT ID ========");
    log.info("Request: {}", request);
    return DeleteAllRegisterService.super.execute(request);
  }

  @Override
  public void validate(DeleteAllRegisterRequest request) {
  }

  @Override
  public ResponseEntity<?> process(DeleteAllRegisterRequest request) {
    List<Register> registers = registerRepository.findAllByStudentId(request.getStudentId());
    log.info("<Query> => Result finding all register by student id: {}", registers);
    if (registers == null) {
      throw new BadRequestException("Không tìm thấy thông tin đăng ký muốn xóa");
    }

    Clazz clazz;
    List<Clazz> classes = new ArrayList<>();
    for (Register register : registers) {
      register.setState(RegisterStateEnums.DELETED.name());
      register.setUpdatedBy(register.getUpdatedBy());
      register.setUpdatedDate(new Date());

      clazz = classRepository.findById(register.getClassId());
      if (clazz != null) {
        Integer numberStudent = clazz.getNumberStudent();
        clazz.setNumberStudent(numberStudent - 1);
        classes.add(clazz);
      }
    }

    classRepository.saveAll(classes);
    registerRepository.saveAll(registers);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Xóa thông tin đăng ký thành công");

    log.info("<Result API> => Deleting register successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
