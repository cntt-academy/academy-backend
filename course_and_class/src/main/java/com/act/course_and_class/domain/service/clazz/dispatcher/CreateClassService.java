package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.application.request.clazz.CreateClassRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateClassService extends
    BaseServiceRequestBody<CreateClassRequest, ResponseEntity<?>> {

}
