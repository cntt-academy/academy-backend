package com.act.course_and_class.domain.service.videoLecture.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.request.videoLecture.CreateVideoLectureRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.CreateVideoLectureService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateVideoLectureService")
public class CreateVideoLectureServiceImpl implements CreateVideoLectureService {

  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(CreateVideoLectureRequest request) {
    log.info("======== API CREATE VIDEO LECTURE ========");
    log.info("Request: {}", gson.toJson(request));
    return CreateVideoLectureService.super.execute(request);
  }

  @Override
  public void validate(CreateVideoLectureRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền thêm bài giảng");
    }

    VideoLecture videoLecture = videoLectureRepository.findByName(request.getName());
    log.info("<Query> => Result finding video lecture by name: {}", videoLecture);
    if (videoLecture != null) {
      throw new BadRequestException("Tên bài giảng đã tồn tại");
    }

    videoLecture = videoLectureRepository.findByUrl(request.getUrl());
    log.info("<Query> => Result finding video lecture by url: {}", videoLecture);
    if (videoLecture != null) {
      throw new BadRequestException("Bài giảng đã tồn tại");
    }

    Clazz clazz = classRepository.findById(request.getClassId());
    log.info("<Query> => Result finding class by id: {}", clazz);
    if (clazz == null || !HowToLearnEnums.Online.name().equals(clazz.getHowToLearn())) {
      throw new BadRequestException("Lớp học không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateVideoLectureRequest request) {
    VideoLecture videoLecture = genericMapper.mapToType(request, VideoLecture.class);
    videoLecture.setVideoOrder(videoLectureRepository.getOrderByClassId(request.getClassId()));
    videoLecture.setState(StateEnums.ACTIVE.name());
    videoLecture.setCreatedBy(loggedAccount.get("name"));
    videoLecture.setCreatedDate(new Date());
    videoLecture.setUpdatedBy(loggedAccount.get("name"));
    videoLecture.setUpdatedDate(new Date());
    videoLectureRepository.save(videoLecture);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm bài giảng thành công");

    log.info("<Result API> => Creating video lecture successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
