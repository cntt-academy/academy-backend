package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.exception.ProcessException;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.request.clazz.CheckClassExistsRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.CheckClassExistsService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("CheckClassExistsService")
public class CheckClassExistsServiceImpl implements CheckClassExistsService {

  private final ClassRepository classRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(CheckClassExistsRequest request) {
    log.info("======== API CHECK CLASS EXISTS ========");
    log.info("Request check: {}", gson.toJson(request));
    return CheckClassExistsService.super.execute(request);
  }

  @Override
  public void validate(CheckClassExistsRequest request) {
    boolean isCourseIdNull = request.getCourseId() == null || request.getCourseId() == 0;
    boolean isClassIdNull = request.getClassId() == null || request.getClassId() == 0;
    boolean isHowToLearnBlank = StringUtils.isBlank(request.getHowToLearn());

    if (isClassIdNull && (isCourseIdNull || isHowToLearnBlank)) {
      throw new BadRequestException("Thông tin kiểm tra không hợp lệ");
    }
  }

  @Override
  public ResponseEntity<?> process(CheckClassExistsRequest request) {
    if (request.getClassId() != null && request.getClassId() != 0) {
      Clazz clazz = classRepository.findById(request.getClassId());
      log.info("<Query> => Result finding class by Id: {}", clazz);

      if (clazz == null) {
        throw new ProcessException("Không tìm thấy thông tin lớp học");
      }
    } else {
      String howToLearn;
      if (HowToLearnEnums.Offline.name().equalsIgnoreCase(request.getHowToLearn())) {
        howToLearn = HowToLearnEnums.Offline.name();
      } else if (HowToLearnEnums.Online.name().equalsIgnoreCase(request.getHowToLearn())) {
        howToLearn = HowToLearnEnums.Online.name();
      } else {
        throw new ProcessException("Hình thức học không hợp lệ");
      }

      List<Clazz> classes =
          classRepository.findAllByCourseIdAndHowToLearnAndState(
              request.getCourseId(), howToLearn, ClassStateEnums.WAIT.name());
      if (CollectionUtils.isEmpty(classes)) {
        throw new ProcessException("Không tìm thấy thông tin lớp học");
      }
      log.info(
          "<Query> => Result finding list class by courseId and howToLearn and state WAIT: {}",
          classes);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Có tồn tại lớp học");

    log.info("<Result API> => Check class exists successfully");
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
