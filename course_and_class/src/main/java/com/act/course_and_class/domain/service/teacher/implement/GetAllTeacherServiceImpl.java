package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.utils.RedisUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetAllTeacherService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllTeacherService")
public class GetAllTeacherServiceImpl implements GetAllTeacherService {

  private final TeacherRepository teacherRepository;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL TEACHER ========");

    List<Teacher> teachers = teacherRepository.findAll();
    log.info("<Query> => Get all teacher result: {}", gson.toJson(teachers));

    // Lưu vào Redis
    String dataTeacher;
    Teacher teacher;
    RedisUtils.set("TOTAL_TEACHER", String.valueOf(teachers.size()));
    for (int i = 1; i <= teachers.size(); i++) {
      teacher = teachers.get(i - 1);
      dataTeacher = teacher.getId() + "," + teacher.getName();
      RedisUtils.set("TEACHER_" + i, dataTeacher);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách tất cả giảng viên và lưu vào Redis thành công");

    log.info("<Result API> => Getting all teachers successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
