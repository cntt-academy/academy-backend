package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.request.course.GetListCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.CourseOutputDto;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.service.course.dispatcher.GetListCourseService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetListCourseService")
public class GetListCourseServiceImpl implements GetListCourseService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final CourseRepository courseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute(GetListCourseRequest request) {
    log.info("======== API GET LIST COURSE ========");
    log.info("Request get list course: {}", gson.toJson(request));
    return GetListCourseService.super.execute(request);
  }

  @Override
  public void validate(GetListCourseRequest request) {
  }

  @Override
  public ResponseEntity<?> process(GetListCourseRequest request) {
    PageRequest pageRequest;
    int page = request.getPageRequest().getPage() - 1;
    int size = request.getPageRequest().getSize();
    String sortBy = request.getPageRequest().getSortBy();
    if (StringUtils.isNotBlank(sortBy)) {
      Sort sort =
          request.getPageRequest().isSortDesc() ?
              Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
      pageRequest = PageRequest.of(page, size, sort);
    } else {
      pageRequest = PageRequest.of(page, size);
    }

    Page<Course> courses = courseRepository.findAllByConditions(
        courseAndClassUtils.standardizedParameterQueryLike(request.getName()),
        courseAndClassUtils.standardizedParameterQueryEqual(request.getTypeId()),
        pageRequest);
    log.info("<Query> => Get list course by condition result: {}", gson.toJson(courses));

    List<CourseOutputDto> courseOutputDtos =
        genericMapper.mapToListCourseOutputDto(courses.getContent());

    Map<String, Object> data =
        Map.of("courses", courseOutputDtos,
            "totalElements", courses.getTotalElements(),
            "totalPages", courses.getTotalPages());

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách khóa học thành công");
    response.setData(data);

    log.info("<Result API> => Getting list course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
