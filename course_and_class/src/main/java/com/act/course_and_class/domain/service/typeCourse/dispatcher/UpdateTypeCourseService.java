package com.act.course_and_class.domain.service.typeCourse.dispatcher;

import com.act.course_and_class.application.request.typeCourse.UpdateTypeCourseRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateTypeCourseService
    extends BaseServiceRequestBody<UpdateTypeCourseRequest, ResponseEntity<?>> {

}
