package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.request.typeCourse.CreateTypeCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.CreateTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetAllTypeCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTypeCourseService")
public class CreateTypeCourseServiceImpl implements CreateTypeCourseService {

  private final GetAllTypeCourseService getAllTypeCourseService;
  private final TypeCourseRepository typeCourseRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Map<String, String> loggedAccount;

  @Override
  public ResponseEntity<?> execute(CreateTypeCourseRequest request) {
    log.info("======== API CREATE TYPE COURSE ========");
    log.info("Request create type course: {}", gson.toJson(request));
    return CreateTypeCourseService.super.execute(request);
  }

  @Override
  public void validate(CreateTypeCourseRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền thêm loại khóa học");
    }

    TypeCourse typeCourse = typeCourseRepository.findByName(request.getName());
    log.info("<Query> => Result finding type course by name: {}", typeCourse);
    if (typeCourse != null) {
      throw new BadRequestException("Loại khóa học đã tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateTypeCourseRequest request) {
    TypeCourse typeCourse = genericMapper.mapToType(request, TypeCourse.class);
    typeCourse.setState(StateEnums.ACTIVE.name());
    typeCourse.setCreatedBy(loggedAccount.get("name"));
    typeCourse.setCreatedDate(new Date());
    typeCourse.setUpdatedBy(loggedAccount.get("name"));
    typeCourse.setUpdatedDate(new Date());
    typeCourseRepository.save(typeCourse);

    // Set lại redis danh sách loại khóa học
    CompletableFuture.runAsync(getAllTypeCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm loại khóa học thành công");

    log.info("<Result API> => Creating type course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
