package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.VideoLecture;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface VideoLectureRepository {

  Page<VideoLecture> findAllByCondition(Integer classId, String name, PageRequest pageRequest);

  VideoLecture findByVideoOrderAndClassId(Integer videoOrder, Integer classId);

  List<VideoLecture> findAllByClassId(Integer classId);

  VideoLecture findById(Integer videoLectureId);

  Integer getOrderByClassId(Integer classId);

  VideoLecture findByName(String name);

  VideoLecture findByUrl(String url);

  void saveAll(List<VideoLecture> videos);

  void delete(VideoLecture video);

  void save(VideoLecture video);

}
