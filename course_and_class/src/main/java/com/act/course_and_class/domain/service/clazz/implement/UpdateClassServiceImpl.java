package com.act.course_and_class.domain.service.clazz.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.exception.ProcessException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.enums.ClassStateEnums;
import com.act.course_and_class.application.enums.HowToLearnEnums;
import com.act.course_and_class.application.request.clazz.UpdateClassRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.clazz.dispatcher.GetAllClassService;
import com.act.course_and_class.domain.service.clazz.dispatcher.UpdateClassService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateClassService")
public class UpdateClassServiceImpl implements UpdateClassService {

  private final CourseAndClassUtils courseAndClassUtils;
  private final GetAllClassService getAllClassService;
  private final TeacherRepository teacherRepository;
  private final CourseRepository courseRepository;
  private final ClassRepository classRepository;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Clazz clazzUpdate;

  @Override
  public ResponseEntity<?> execute(UpdateClassRequest request) {
    log.info("======== API UPDATE CLASS ========");
    log.info("Request update class: {}", gson.toJson(request));
    return UpdateClassService.super.execute(request);
  }

  @Override
  public void validate(UpdateClassRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa lớp học");
    }

    clazzUpdate = classRepository.findById(request.getId());
    log.info("<Query> => Result finding class by id: {}", clazzUpdate);
    if (clazzUpdate == null) {
      throw new BadRequestException("Không tìm thấy lớp học muốn chỉnh sửa");
    }
    if (ClassStateEnums.PREP.name().equals(clazzUpdate.getState())) {
      throw new BadRequestException("Không được phép chỉnh sửa lớp học dự bị");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateClassRequest request) {
    if (StringUtils.isNotBlank(request.getName())
        && !request.getName().equalsIgnoreCase(clazzUpdate.getName())) {
      Clazz clazz = classRepository.findByName(request.getName());
      log.info("<Query> => Result finding class by name: {}", clazz);
      if (clazz != null) {
        throw new ProcessException("Tên lớp học đã được đăng ký trong hệ thống");
      } else {
        clazzUpdate.setName(request.getName());
      }
    }

    if (StringUtils.isNotBlank(request.getFullName())
        && !request.getFullName().equalsIgnoreCase(clazzUpdate.getFullName())) {
      Clazz clazz = classRepository.findByFullName(request.getFullName());
      log.info("<Query> => Result finding class by full name: {}", clazz);
      if (clazz != null) {
        throw new ProcessException("Tên đầy đủ lớp học đã được đăng ký trong hệ thống");
      } else {
        clazzUpdate.setFullName(request.getFullName());
      }
    }

    if (StringUtils.isNotBlank(request.getHowToLearn())
        && !request.getHowToLearn().equalsIgnoreCase(clazzUpdate.getHowToLearn())) {
      if (HowToLearnEnums.Offline.name().equalsIgnoreCase(request.getHowToLearn())) {
        clazzUpdate.setHowToLearn(HowToLearnEnums.Offline.name());
      } else if (HowToLearnEnums.Online.name().equalsIgnoreCase(request.getHowToLearn())) {
        clazzUpdate.setHowToLearn(HowToLearnEnums.Online.name());
      } else {
        throw new ProcessException("Hình thức học không hợp lệ");
      }
    }

    if (StringUtils.isNotBlank(request.getUrlClass())) {
      clazzUpdate.setUrlClass(request.getUrlClass());
    }

    if (request.getNumberStudent() != null
        && !Objects.equals(request.getNumberStudent(), clazzUpdate.getNumberStudent())) {
      if (request.getNumberStudent() <= 0) {
        throw new ProcessException("Số lượng học sinh không hợp lệ");
      } else {
        clazzUpdate.setNumberStudent(request.getNumberStudent());
      }
    }

    if (StringUtils.isNotBlank(request.getDateOpening())
        && !request.getDateOpening()
        .equalsIgnoreCase(CourseAndClassUtils.sdf.format(clazzUpdate.getDateOpening()))) {
      try {
        Date dateOpening = CourseAndClassUtils.sdf.parse(request.getDateOpening());
        if (dateOpening.before(new Date()) || dateOpening.equals(new Date())) {
          throw new ProcessException("Ngày khai giảng không hợp lệ");
        } else {
          clazzUpdate.setDateOpening(dateOpening);
        }
      } catch (Exception e) {
        throw new ProcessException("Ngày khai giảng không hợp lệ");
      }
    }

    if (request.getTeacherId() != null
        && !Objects.equals(request.getTeacherId(), clazzUpdate.getTeacher().getId())) {
      Teacher teacher = teacherRepository.findById(request.getTeacherId());
      log.info("<Query> => Result finding teacher by id: {}", teacher);
      if (teacher == null) {
        throw new ProcessException("Không tìm thấy giảng viên");
      } else {
        clazzUpdate.setTeacher(teacher);
      }
    }

    if (request.getCourseId() != null
        && !Objects.equals(request.getCourseId(), clazzUpdate.getCourse().getId())) {
      Course course = courseRepository.findById(request.getCourseId());
      log.info("<Query> => Result finding course by id: {}", course);
      if (course == null) {
        throw new ProcessException("Không tìm thấy khóa học");
      } else {
        clazzUpdate.setCourse(course);
      }
    }

    if (StringUtils.isNotBlank(request.getManagerEmail())
        && !clazzUpdate.getManagerEmail().equals(request.getManagerEmail())) {
      Map<String, Object> manager = courseAndClassUtils.getAdminByEmail(request.getManagerEmail());
      if (manager == null) {
        throw new BadRequestException("Email quản lý lớp không hợp lệ");
      } else if (!clazzUpdate.getManager().equalsIgnoreCase((String) manager.get("name"))) {
        clazzUpdate.setManager((String) manager.get("name"));
        clazzUpdate.setManagerEmail(request.getManagerEmail());
      }
    }

    if (StringUtils.isNotBlank(request.getSupporterEmail())
        && !clazzUpdate.getSupporterEmail().equals(request.getSupporterEmail())) {
      Map<String, Object> supporter = courseAndClassUtils.getAdminByEmail(
          request.getSupporterEmail());
      if (supporter == null) {
        throw new BadRequestException("Email trợ giảng không hợp lệ");
      } else if (!clazzUpdate.getSupporter().equalsIgnoreCase((String) supporter.get("name"))) {
        clazzUpdate.setSupporter((String) supporter.get("name"));
        clazzUpdate.setSupporterEmail(request.getSupporterEmail());
      }
    }

    if (StringUtils.isNotBlank(request.getState())) {
      try {
        ClassStateEnums stateEnums = ClassStateEnums.valueOf(request.getState().toUpperCase());
        clazzUpdate.setState(stateEnums.name());
      } catch (Exception ex) {
        throw new BadRequestException("Trạng thái mới không hợp lệ");
      }
    }

    clazzUpdate.setUpdatedBy(loggedAccount.get("name"));
    clazzUpdate.setUpdatedDate(new Date());
    classRepository.save(clazzUpdate);

    // Set lại redis danh sách lớp học
    CompletableFuture.runAsync(getAllClassService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Chỉnh sửa lớp học thành công");

    log.info("<Result API> => Updating class successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
