package com.act.course_and_class.domain.service.videoLecture.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface DeleteVideoLectureService
    extends BaseServiceRequestParam<Integer, ResponseEntity<?>> {

}
