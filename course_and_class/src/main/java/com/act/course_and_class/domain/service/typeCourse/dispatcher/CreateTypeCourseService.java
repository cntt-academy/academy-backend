package com.act.course_and_class.domain.service.typeCourse.dispatcher;

import com.act.course_and_class.application.request.typeCourse.CreateTypeCourseRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateTypeCourseService
    extends BaseServiceRequestBody<CreateTypeCourseRequest, ResponseEntity<?>> {

}
