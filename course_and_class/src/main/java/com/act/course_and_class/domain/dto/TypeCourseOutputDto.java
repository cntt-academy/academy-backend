package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.TypeCourse} entity
 */
@Data
public class TypeCourseOutputDto implements Serializable {

  private Integer id;
  private String name;
  private String desc;
  private String createdBy;
  private String createdDate;
}