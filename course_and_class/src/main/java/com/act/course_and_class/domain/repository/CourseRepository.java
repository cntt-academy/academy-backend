package com.act.course_and_class.domain.repository;

import com.act.course_and_class.domain.entity.Course;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CourseRepository {

  Page<Course> findAllByConditions(String name, Integer typeId, PageRequest pageRequest);

  Course findById(Integer courseId);

  Course findByName(String name);

  void save(Course course);

  List<Course> findAll();

}
