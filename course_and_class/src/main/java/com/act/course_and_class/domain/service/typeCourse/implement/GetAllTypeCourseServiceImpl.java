package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.Infrastructure.utils.RedisUtils;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.TypeCourseOutputDto;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetAllTypeCourseService;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetAllTypeCourseService")
public class GetAllTypeCourseServiceImpl implements GetAllTypeCourseService {

  private final TypeCourseRepository typeCourseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  @Override
  public ResponseEntity<?> execute() {
    log.info("======== API GET ALL TYPE COURSE ========");
    List<TypeCourse> typeCourses = typeCourseRepository.findAll();
    log.info("<Query> => Find all type course result: {}", gson.toJson(typeCourses));

    // Lưu vào Redis
    String dataTypeCourse;
    TypeCourseOutputDto typeCourse;
    List<TypeCourseOutputDto> typeCourseOutputDtos =
        genericMapper.mapToListOfType(typeCourses, TypeCourseOutputDto.class);
    RedisUtils.set("TOTAL_TYPE_COURSE", String.valueOf(typeCourseOutputDtos.size()));
    for (int i = 1; i <= typeCourseOutputDtos.size(); i++) {
      typeCourse = typeCourseOutputDtos.get(i - 1);
      dataTypeCourse = typeCourse.getId() + "," + typeCourse.getName();
      RedisUtils.set("TYPE_COURSE_" + i, dataTypeCourse);
    }

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra danh sách loại khóa học và lưu vào redis thành công");
    response.setData(Map.of("typeCourses", typeCourseOutputDtos));

    log.info("<Result API> => Getting list type course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
