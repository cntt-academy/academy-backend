package com.act.course_and_class.domain.service.course.dispatcher;

import com.act.course_and_class.application.request.course.CreateCourseRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface CreateCourseService extends
    BaseServiceRequestBody<CreateCourseRequest, ResponseEntity<?>> {

}
