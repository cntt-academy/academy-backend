package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.request.teacher.UpdateTeacherRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetAllTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.UpdateTeacherService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateTeacherService")
public class UpdateTeacherServiceImpl implements UpdateTeacherService {

  private final GetAllTeacherService getAllTeacherService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final TeacherRepository teacherRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private Teacher teacher;

  @Override
  public ResponseEntity<?> execute(UpdateTeacherRequest request) {
    log.info("======== API UPDATE TEACHER ========");
    log.info("Request update teacher: {}", gson.toJson(request));
    return UpdateTeacherService.super.execute(request);
  }

  @Override
  public void validate(UpdateTeacherRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa thông tin giảng viên");
    }

    teacher = teacherRepository.findById(request.getId());
    log.info("<Query> => Result finding teacher by id: {}", teacher);
    if (teacher == null) {
      throw new BadRequestException("Không tìm thấy thông tin giảng viên muốn cập nhật");
    }

    if (StringUtils.isNotBlank(request.getGender())) {
      teacher.setGender(courseAndClassUtils.validateAndStandardizedGender(request.getGender()));
    }

    if (StringUtils.isNotBlank(request.getEmail())) {
      Teacher teacher = teacherRepository.findByEmail(request.getEmail());
      log.info("<Query> => Result finding teacher by email: {}", teacher);
      if (teacher != null && !Objects.equals(teacher.getId(), request.getId())) {
        throw new BadRequestException("Email giảng viên đã được đăng ký trong hệ thống");
      }
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateTeacherRequest request) {
    Teacher dataUpdate = genericMapper.mapToType(request, Teacher.class);
    dataUpdate.setGender(null);
    genericMapper.copyNonNullProperties(dataUpdate, teacher);
    teacher.setUpdatedBy(loggedAccount.get("name"));
    teacher.setUpdatedDate(new Date());
    teacherRepository.save(teacher);

    // Set lại redis danh sách giảng viên
    CompletableFuture.runAsync(getAllTeacherService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật thông tin giảng viên thành công");

    log.info("<Result API> => Updating teacher successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
