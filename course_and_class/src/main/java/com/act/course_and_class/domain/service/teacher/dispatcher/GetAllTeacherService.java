package com.act.course_and_class.domain.service.teacher.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceNoRequest;
import org.springframework.http.ResponseEntity;

public interface GetAllTeacherService extends BaseServiceNoRequest<ResponseEntity<?>> {

}
