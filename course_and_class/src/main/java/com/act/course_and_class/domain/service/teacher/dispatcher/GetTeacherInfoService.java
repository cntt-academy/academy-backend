package com.act.course_and_class.domain.service.teacher.dispatcher;

import com.act.course_and_class.domain.service.base.BaseServiceRequestParam;
import org.springframework.http.ResponseEntity;

public interface GetTeacherInfoService
    extends BaseServiceRequestParam<String, ResponseEntity<?>> {

}
