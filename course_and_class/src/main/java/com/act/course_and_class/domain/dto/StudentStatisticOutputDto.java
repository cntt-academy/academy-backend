package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class StudentStatisticOutputDto implements Serializable {
  private Integer classId;
  private String className;
  private String classState;
  private String createdDate;
  private Integer totalStudent = 0;
  private Integer numberNewStudent = 0;
  private Integer numberWaitStudent = 0;
  private Integer numberPayHalfStudent = 0;
  private Integer numberConfirmStudent = 0;
}
