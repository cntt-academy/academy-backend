package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.CourseOutputDto;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.service.course.dispatcher.GetDetailCourseService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailCourseService")
public class GetDetailCourseServiceImpl implements GetDetailCourseService {

  private final CourseRepository courseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Course course;

  @Override
  public ResponseEntity<?> execute(Integer courseId) {
    log.info("======== API GET DETAIL COURSE ========");
    log.info("Course Id: {}", courseId);
    return GetDetailCourseService.super.execute(courseId);
  }

  @Override
  public void validate(Integer courseId) {
    course = courseRepository.findById(courseId);
    log.info("<Query> => Find course by id result: {}", course);
    if (course == null) {
      throw new BadRequestException("Khóa học không tồn tại");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer courseId) {
    CourseOutputDto courseOutputDto = genericMapper.mapToCourseOutputDto(course);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra thông tin khóa học thành công");
    response.setData(Map.of("course", courseOutputDto));

    log.info("<Result API> => Getting course info successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
