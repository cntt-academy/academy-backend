package com.act.course_and_class.domain.service.clazz.dispatcher;

import com.act.course_and_class.application.request.clazz.UpdateClassRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface UpdateClassService extends
    BaseServiceRequestBody<UpdateClassRequest, ResponseEntity<?>> {

}
