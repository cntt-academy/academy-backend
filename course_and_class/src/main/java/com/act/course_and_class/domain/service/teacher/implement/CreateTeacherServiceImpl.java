package com.act.course_and_class.domain.service.teacher.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.enums.StateEnums;
import com.act.course_and_class.application.request.teacher.CreateTeacherRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Teacher;
import com.act.course_and_class.domain.repository.TeacherRepository;
import com.act.course_and_class.domain.service.teacher.dispatcher.CreateTeacherService;
import com.act.course_and_class.domain.service.teacher.dispatcher.GetAllTeacherService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTeacherService")
public class CreateTeacherServiceImpl implements CreateTeacherService {

  private final GetAllTeacherService getAllTeacherService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final TeacherRepository teacherRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private String gender;

  @Override
  public ResponseEntity<?> execute(CreateTeacherRequest request) {
    log.info("======== API CREATE TEACHER ========");
    log.info("Request create teacher: {}", gson.toJson(request));
    return CreateTeacherService.super.execute(request);
  }

  @Override
  public void validate(CreateTeacherRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền thêm giảng viên");
    }

    Pattern pattern = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    Matcher matcher = pattern.matcher(request.getEmail());
    if (!matcher.matches()) {
      throw new BadRequestException("Email không hợp lệ");
    }

    gender = courseAndClassUtils.validateAndStandardizedGender(request.getGender());

    Teacher teacher = teacherRepository.findByEmail(request.getEmail());
    log.info("<Query> => Result finding teacher by email: {}", teacher);
    if (teacher != null) {
      throw new BadRequestException("Email giảng viên đã được đăng ký trong hệ thống");
    }
  }

  @Override
  public ResponseEntity<?> process(CreateTeacherRequest request) {
    Teacher teacher = genericMapper.mapToType(request, Teacher.class);
    teacher.setGender(gender);
    teacher.setState(StateEnums.ACTIVE.name());
    teacher.setCreatedBy(loggedAccount.get("name"));
    teacher.setCreatedDate(new Date());
    teacher.setUpdatedBy(loggedAccount.get("name"));
    teacher.setUpdatedDate(new Date());
    teacherRepository.save(teacher);

    // Set lại redis danh sách giảng viên
    CompletableFuture.runAsync(getAllTeacherService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Thêm thông tin giảng viên thành công");

    log.info("<Result API> => Creating teacher successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
