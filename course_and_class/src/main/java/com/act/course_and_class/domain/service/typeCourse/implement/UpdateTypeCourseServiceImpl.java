package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.application.request.typeCourse.UpdateTypeCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetAllTypeCourseService;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.UpdateTypeCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateTypeCourseService")
public class UpdateTypeCourseServiceImpl implements UpdateTypeCourseService {

  private final GetAllTypeCourseService getAllTypeCourseService;
  private final TypeCourseRepository typeCourseRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final Gson gson;
  private Map<String, String> loggedAccount;
  private TypeCourse typeCourse;

  @Override
  public ResponseEntity<?> execute(UpdateTypeCourseRequest request) {
    log.info("======== API UPDATE TYPE COURSE ========");
    log.info("Request update type course: {}", gson.toJson(request));
    return UpdateTypeCourseService.super.execute(request);
  }

  @Override
  public void validate(UpdateTypeCourseRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền chỉnh sửa loại khóa học");
    }

    typeCourse = typeCourseRepository.findById(request.getId());
    log.info("<Query> => Result finding type course by id: {}", typeCourse);
    if (typeCourse == null) {
      throw new BadRequestException("Không tìm thấy thông tin loại khóa học muốn cập nhật");
    }

    if (StringUtils.isNotBlank(request.getName())) {
      TypeCourse typeCourse = typeCourseRepository.findByName(request.getName());
      log.info("<Query> => Result finding type course by name: {}", typeCourse);
      if (typeCourse != null && !Objects.equals(typeCourse.getId(), request.getId())) {
        throw new BadRequestException("Tên loại khóa học đã tồn tại");
      }
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateTypeCourseRequest request) {
    if (StringUtils.isNotBlank(request.getName())) {
      typeCourse.setName(request.getName());
    }
    if (StringUtils.isNotBlank(request.getDesc())) {
      typeCourse.setDesc(request.getDesc());
    }
    typeCourse.setUpdatedBy(loggedAccount.get("name"));
    typeCourse.setUpdatedDate(new Date());
    typeCourseRepository.save(typeCourse);

    // Set lại redis danh sách loại khóa học
    CompletableFuture.runAsync(getAllTypeCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật loại khóa học thành công");

    log.info("<Result API> => Updating type course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
