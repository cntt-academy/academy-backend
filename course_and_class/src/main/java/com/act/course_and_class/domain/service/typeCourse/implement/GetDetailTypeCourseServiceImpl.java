package com.act.course_and_class.domain.service.typeCourse.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.TypeCourseOutputDto;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.typeCourse.dispatcher.GetDetailTypeCourseService;
import com.google.gson.Gson;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailTypeCourseService")
public class GetDetailTypeCourseServiceImpl implements GetDetailTypeCourseService {

  private final TypeCourseRepository typeCourseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;
  private TypeCourse typeCourse;

  @Override
  public ResponseEntity<?> execute(Integer typeCourseId) {
    log.info("======== API GET DETAIL TYPE COURSE ========");
    log.info("Type course Id: {}", typeCourseId);
    return GetDetailTypeCourseService.super.execute(typeCourseId);
  }

  @Override
  public void validate(Integer typeCourseId) {
    typeCourse = typeCourseRepository.findById(typeCourseId);
    log.info("<Query> => Result finding type course by id: {}", typeCourse);
    if (typeCourse == null) {
      throw new BadRequestException("Không tìm thấy loại khóa học");
    }
  }

  @Override
  public ResponseEntity<?> process(Integer typeCourseId) {
    TypeCourseOutputDto typeCourseOutputDto = genericMapper.mapToTypeCourseOutputDto(typeCourse);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra loại khóa học thành công");
    response.setData(Map.of("typeCourse", typeCourseOutputDto));
    log.info("<Result API> => Getting type course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
