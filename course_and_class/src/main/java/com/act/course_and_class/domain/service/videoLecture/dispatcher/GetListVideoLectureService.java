package com.act.course_and_class.domain.service.videoLecture.dispatcher;

import com.act.course_and_class.application.request.videoLecture.GetListVideoLectureRequest;
import com.act.course_and_class.domain.service.base.BaseServiceRequestBody;
import org.springframework.http.ResponseEntity;

public interface GetListVideoLectureService
    extends BaseServiceRequestBody<GetListVideoLectureRequest, ResponseEntity<?>> {

}
