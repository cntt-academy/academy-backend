package com.act.course_and_class.domain.service.course.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.application.request.course.UpdateCourseRequest;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.entity.Course;
import com.act.course_and_class.domain.entity.TypeCourse;
import com.act.course_and_class.domain.repository.CourseRepository;
import com.act.course_and_class.domain.repository.TypeCourseRepository;
import com.act.course_and_class.domain.service.course.dispatcher.GetAllCourseService;
import com.act.course_and_class.domain.service.course.dispatcher.UpdateCourseService;
import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateCourseService")
public class UpdateCourseServiceImpl implements UpdateCourseService {

  private final TypeCourseRepository typeCourseRepository;
  private final GetAllCourseService getAllCourseService;
  private final CourseAndClassUtils courseAndClassUtils;
  private final CourseRepository courseRepository;
  private final GenericMapper genericMapper;
  private final Gson gson;

  private Map<String, String> loggedAccount;
  private Course course;

  @Override
  public ResponseEntity<?> execute(UpdateCourseRequest request) {
    log.info("======== API UPDATE COURSE ========");
    log.info("Request update course: {}", gson.toJson(request));
    return UpdateCourseService.super.execute(request);
  }

  @Override
  public void validate(UpdateCourseRequest request) {
    loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    if (!courseAndClassUtils.isOwnerOrManager(loggedAccount.get("role"))) {
      throw new BadRequestException("Bạn không có quyền cập nhật thông tin khóa học");
    }

    if (request.getTypeId() != null && request.getTypeId() != 0) {
      TypeCourse typeCourse = typeCourseRepository.findById(request.getTypeId());
      log.info("<Query> => Result finding type course by id: {}", typeCourse);
      if (typeCourse == null) {
        throw new BadRequestException("Không tìm thấy loại khóa học");
      }
    }

    course = courseRepository.findByName(request.getName());
    log.info("<Query> => Result finding course by name: {}", course);
    if (course != null && !Objects.equals(course.getId(), request.getId())) {
      throw new BadRequestException("Đã tồn tại khóa học có tên giống với yêu cầu");
    }

    course = courseRepository.findById(request.getId());
    log.info("<Query> => Result finding course by id: {}", course);
    if (course == null) {
      throw new BadRequestException("Không tìm thấy khóa học muốn cập nhật");
    }
  }

  @Override
  public ResponseEntity<?> process(UpdateCourseRequest request) {
    Course dataUpdate = genericMapper.mapToType(request, Course.class);
    genericMapper.copyNonNullProperties(dataUpdate, course);
    course.setUpdatedBy(loggedAccount.get("name"));
    course.setUpdatedDate(new Date());
    courseRepository.save(course);

    // Set lại redis danh sách khóa học
    CompletableFuture.runAsync(getAllCourseService::execute);

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Cập nhật thông tin khóa học thành công");

    log.info("<Result API> => Updating course successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
