package com.act.course_and_class.domain.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * A DTO for the {@link com.act.course_and_class.domain.entity.Clazz} entity
 */
@Data
public class ClassOutputDto implements Serializable {

  private Integer id;
  private String avatar;
  private String fullName;
  private String name;
  private String howToLearn;
  private String urlClass;
  private Integer numberStudent;
  private String dateOpening;
  private String teacher;
  private String manager;
  private String managerEmail;
  private String supporter;
  private String supporterEmail;
  private Integer courseId;
  private String course;
  private String special;
  private String trialLessonName;
  private String trialLessonUrl;
  private String state;
  private String createdBy;
  private String createdDate;
}