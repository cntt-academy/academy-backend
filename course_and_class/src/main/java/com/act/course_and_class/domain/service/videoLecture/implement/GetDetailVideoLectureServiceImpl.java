package com.act.course_and_class.domain.service.videoLecture.implement;

import com.act.course_and_class.Infrastructure.exception.BadRequestException;
import com.act.course_and_class.Infrastructure.exception.ProcessException;
import com.act.course_and_class.Infrastructure.utils.CourseAndClassUtils;
import com.act.course_and_class.Infrastructure.utils.GenericMapper;
import com.act.course_and_class.Infrastructure.utils.UnirestUtils;
import com.act.course_and_class.application.enums.RegisterStateEnums;
import com.act.course_and_class.application.enums.RoleEnums;
import com.act.course_and_class.application.response.JsonResponseBase;
import com.act.course_and_class.domain.dto.VideoLectureOutputDto;
import com.act.course_and_class.domain.entity.Clazz;
import com.act.course_and_class.domain.entity.HistoryWatch;
import com.act.course_and_class.domain.entity.Register;
import com.act.course_and_class.domain.entity.VideoLecture;
import com.act.course_and_class.domain.repository.ClassRepository;
import com.act.course_and_class.domain.repository.HistoryWatchRepository;
import com.act.course_and_class.domain.repository.RegisterRepository;
import com.act.course_and_class.domain.repository.VideoLectureRepository;
import com.act.course_and_class.domain.service.videoLecture.dispatcher.GetDetailVideoLectureService;
import com.google.gson.Gson;
import com.netflix.discovery.EurekaClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailVideoLectureService")
public class GetDetailVideoLectureServiceImpl implements GetDetailVideoLectureService {

  private final HistoryWatchRepository historyWatchRepository;
  private final VideoLectureRepository videoLectureRepository;
  private final CourseAndClassUtils courseAndClassUtils;
  private final RegisterRepository registerRepository;
  private final ClassRepository classRepository;
  private final GenericMapper genericMapper;
  private final EurekaClient eurekaClient;
  private final Gson gson;

  @Value("${path.sendEmail.encouragePayTuition}")
  private String pathSendEmailEncouragePayTuition;
  private VideoLecture videoLecture;
  private Map<String, Object> user;

  @Override
  public ResponseEntity<?> execute(Integer videoLectureId) {
    log.info("======== API GET DETAIL VIDEO LECTURE ========");
    log.info("Video lecture Id: {}", videoLectureId);
    return GetDetailVideoLectureService.super.execute(videoLectureId);
  }

  @Override
  public void validate(Integer videoLectureId) {
    videoLecture = videoLectureRepository.findById(videoLectureId);
    log.info("<Query> => Result finding video lecture by id: {}", videoLecture);
    if (videoLecture == null) {
      throw new BadRequestException("Không tìm thấy video bài giảng");
    }
  }

  @Override
  @SneakyThrows
  public ResponseEntity<?> process(Integer videoLectureId) {
    Map<String, String> loggedAccount = courseAndClassUtils.getAccountLoginInfo();
    user = courseAndClassUtils.getUserByEmail(loggedAccount.get("email"));

    if (RoleEnums.User.name().equals(loggedAccount.get("role"))) {
      CompletableFuture<Boolean> getConfirm = CompletableFuture.supplyAsync(() -> {
        Register register =
            registerRepository.findByStudentIdAndClassId(
                (Integer) user.get("id"), videoLecture.getClassId());
        return RegisterStateEnums.CONFIRM.name().equals(register.getState());
      });

      if (!getConfirm.get() && videoLecture.getVideoOrder() >= 3) {
        throw new ProcessException(
            "Xin hãy liên hệ với Quản trị viên để được hướng dẫn xem tiếp những video tiếp theo");
      }

      if (videoLecture.getVideoOrder() > 1) {
        VideoLecture preVideoLecture =
            videoLectureRepository.findByVideoOrderAndClassId(
                videoLecture.getVideoOrder() - 1, videoLecture.getClassId());
        List<HistoryWatch> historyWatches = historyWatchRepository.findAllByVideoLectureIdAndUserEmail(
            preVideoLecture.getId(),
            (String) user.get("email"));
        if (CollectionUtils.isEmpty(historyWatches)) {
          throw new ProcessException(
              "Bạn chưa xem bài giảng trước <br> Xin hãy xem bài giảng trước rồi mới xem bài giảng này");
        }
      }
    }

    VideoLectureOutputDto videoLectureOutputDto =
        genericMapper.mapToVideoLectureOutputDto(videoLecture);

    CompletableFuture.runAsync(
        () -> {
          List<HistoryWatch> historyWatches =
              historyWatchRepository.findAllByVideoLectureIdAndUserEmail(
                  videoLectureId, (String) user.get("email"));
          if (historyWatches.size() <= 1) {
            Clazz clazz = classRepository.findById(videoLecture.getClassId());

            String urlSendEmailUserAddedIntoClass = eurekaClient
                .getNextServerFromEureka("message", false).getHomePageUrl()
                + "email" + pathSendEmailEncouragePayTuition;

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

            Map<String, Object> body = new HashMap<>();
            body.put("classManagerEmail", clazz.getManagerEmail());
            body.put("className", clazz.getName());
            body.put("studentName", user.get("name"));
            body.put("studentEmail", user.get("email"));
            body.put("studentPhone", user.get("phone"));
            body.put("time", sdf.format(new Date()));

            UnirestUtils.postByBodyWithoutHeader(urlSendEmailUserAddedIntoClass, gson.toJson(body));
          }
        });

    JsonResponseBase<Map<?, ?>> response = new JsonResponseBase<>();
    response.setStatus(HttpStatus.OK.value());
    response.setMessage("Lấy ra video bài giảng thành công");
    response.setData(Map.of("videoLecture", videoLectureOutputDto));
    log.info("<Result API> => Getting video lecture successfully");
    log.info(gson.toJson(response));

    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
